package com.hinos.rewardsample.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status
import com.hinos.rewardsample.base.BaseBroadcastReceiver
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SMSReceiver @Inject constructor() : BaseBroadcastReceiver()
{
    private var mListener : OnSMSListener? = null

    override fun onReceive(context: Context, intent: Intent)
    {
        super.onReceive(context, intent)

        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action)
        {
            val extras = intent.extras
            val status = extras?.get(SmsRetriever.EXTRA_STATUS) as Status?
            status?.let {
                when(it.statusCode) {
                    CommonStatusCodes.SUCCESS -> {
                        val message = extras?.get(SmsRetriever.EXTRA_SMS_MESSAGE) as String?
                        message?.let { auth ->
                            mListener?.onSMSListener(extractProofKey(auth))
                        }
                    }

                    else -> return
                }
            }
        }
    }

    fun setOnSMSListener(listener : OnSMSListener)
    {
        mListener = listener
    }

    fun registerReceiver(context: Context)
    {
        val filter = IntentFilter().apply {
            addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        }
        context.registerReceiver(this, filter)
    }

    fun unregisterReceiver(context: Context)
    {
        context.unregisterReceiver(this)
    }

    private fun extractProofKey(smsMsg : String) : String
    {
        val data = smsMsg.replace("[Web발신]", "")
        val start = data.indexOf("[")
        val end = data.indexOf("]")
        val proofKey = data.substring(start+1, end)
        return proofKey
    }

    interface OnSMSListener {
        fun onSMSListener(proofKey : String)
    }

}