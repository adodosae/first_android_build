package com.hinos.rewardsample.receiver

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.hinos.rewardsample.base.BaseBroadcastReceiver
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class LogoutReceiver @Inject constructor() : BaseBroadcastReceiver()
{

    private var mLogoutListener : OnLogoutListener? = null

    companion object
    {
        private const val ACTION_LOGOUT = ".ACTION_LOGOUT"

        fun send(context: Context)
        {
            val i = Intent(context.packageName + ACTION_LOGOUT)
            context.sendBroadcast(i)
        }
    }

    override fun onReceive(context: Context, intent: Intent)
    {
        super.onReceive(context, intent)
        if (intent.action == context.packageName + ACTION_LOGOUT)
        {
            mLogoutListener?.onLogoutListener()
        }
    }
    fun setOnLogoutListener(listener : OnLogoutListener)
    {
        mLogoutListener = listener
    }
    fun registerReceiver(context: Context)
    {
        val filter = IntentFilter().apply {
            addAction(context.packageName + ACTION_LOGOUT)
        }
        context.registerReceiver(this, filter)
    }
    fun unregisterReceiver(context: Context)
    {
        context.unregisterReceiver(this)
    }
    interface OnLogoutListener
    {
        fun onLogoutListener()
    }
}