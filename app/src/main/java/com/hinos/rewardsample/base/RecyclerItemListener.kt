package com.hinos.rewardsample.base

import com.hinos.rewardsample.data.dto.item.ItemOfferWall

interface RecyclerItemListener
{
    fun <T> onItemSelected(item : T)
}