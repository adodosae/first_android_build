package com.hinos.rewardsample.base

interface BaseDialog
{
    companion object
    {
        const val RES_OK = 1
        const val RES_CANCEL = 2
        const val RES_REVIEW = 3
        const val RES_FAIL = 4
    }

    interface OnDialogResult {
        fun onDialogResult(nResult: Int, data : Any)
    }
}