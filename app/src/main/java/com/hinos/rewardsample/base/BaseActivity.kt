package com.hinos.rewardsample.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.hinos.rewardsample.R
import com.hinos.rewardsample.receiver.LogoutReceiver
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.dialog.GeneralDialog
import com.hinos.rewardsample.ui.dialog.GeneralSheet
import com.hinos.rewardsample.ui.login.LoginActivity
import com.hinos.rewardsample.ui.splash.SplashActivity
import com.hinos.rewardsample.util.ErrorCase
import kotlinx.coroutines.Job
import javax.inject.Inject
import javax.inject.Scope

abstract class BaseActivity <T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity()
{
    lateinit var mBinding: T
    abstract val mViewModel : V

    abstract val mLayoutResID : Int

    @Inject
    lateinit var mLogOutReceiver : LogoutReceiver

    @Inject
    lateinit var mSheet : GeneralSheet

    @Inject
    lateinit var mDialog : GeneralDialog

    private var mToast : Toast? = null

    private var mSessionScope : Job? = null

    abstract fun observeViewModel()
    abstract fun initListener()

    fun openActivity(i : Intent)
    {
        startActivity(i)
    }

    fun <T> openActivity(className : Class<T>)
    {
        val i = Intent(this, className)
        startActivity(i)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView<T>(this, mLayoutResID).apply {
            lifecycleOwner = this@BaseActivity
        }
        initListener()
        observeViewModel()
        registerReceiver()
    }

    override fun onStop() {
        super.onStop()
        mSessionScope?.let { it.cancel() }
    }

    override fun onDestroy()
    {
        super.onDestroy()
        mLogOutReceiver.unregisterReceiver(this)
    }

    private fun registerReceiver()
    {
        mLogOutReceiver.run {
            registerReceiver(this@BaseActivity)
            setOnLogoutListener(object : LogoutReceiver.OnLogoutListener
            {
                override fun onLogoutListener()
                {
                    if (!this@BaseActivity.isFinishing || !isDestroyed)
                    {
                        closeActivity()
                    }
                }
            })
        }
    }

    fun checkUserSession()
    {
        mViewModel.run {
            mSessionScope = requestSessionUser()
            sessionLiveData.observe(this@BaseActivity) {
                showToast(it.errorCode ?: 0)
                LogoutReceiver.send(this@BaseActivity)
                openActivity(Intent(this@BaseActivity, SplashActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
            }
        }
    }


    override fun onBackPressed()
    {
        closeActivity()
    }

    open fun showToast(message : String, length : Int = Toast.LENGTH_SHORT) {
        showToast(this.applicationContext, message, length)
    }

    open fun showToast(errorCode: Int, length : Int = Toast.LENGTH_SHORT)
    {
        val errorMsgRes : Int = ErrorCase.errorHashtable[errorCode] ?: R.string.error_etc
        val errorMsg = if (errorMsgRes == R.string.error_etc) {
            getString(errorMsgRes) + " Code[$errorCode]"
        } else {
            getString(errorMsgRes)
        }
        showToast(errorMsg, length)
    }

    open fun showToast(context : Context, message : String, length : Int = Toast.LENGTH_SHORT) {
        mToast?.cancel()
        mToast = Toast.makeText(context, message, length).apply { show() }
    }

    open fun getErrorString(errorCode: Int) : String {
        val errorMsg : Int = ErrorCase.errorHashtable[errorCode] ?: R.string.error_etc
        return getString(errorMsg)
    }


    open fun closeActivity()
    {
        finish()
    }
}