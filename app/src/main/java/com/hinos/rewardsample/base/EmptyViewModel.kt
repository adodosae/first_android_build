package com.hinos.rewardsample.base

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.data.DataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EmptyViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    val 개인정보처리방침 : MutableLiveData<String> = MutableLiveData<String>()
    val 서비스이용약관 : MutableLiveData<String> = MutableLiveData<String>()

    init {
        viewModelScope.launch {
            개인정보처리방침.value = mDataRepository.getCachePrivacy()
            서비스이용약관.value = mDataRepository.getCacheTermSofService()
        }
    }
}