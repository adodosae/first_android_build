package com.hinos.rewardsample.base

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.hinos.rewardsample.MyApp
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.request.SessionRequest
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Scope

abstract class BaseViewModel(protected val app : Application, protected val mDataRepository: DataRepository) : AndroidViewModel(app)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    protected val  privateSessionLiveData = MutableLiveData<Resource<ResCode>>()
    val sessionLiveData : LiveData<Resource<ResCode>> get() = privateSessionLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    protected val  protectedUserPointResponseLiveData = MutableLiveData<Resource<PurchaseResponse>>()
    val userPointResponseLiveData: LiveData<Resource<PurchaseResponse>> get() = protectedUserPointResponseLiveData

//    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
//    protected val  protectedUserPointLiveData = MutableLiveData<String>()
//    val userPointLiveData: LiveData<String> get() = protectedUserPointLiveData

    init
    {
        getCachePoint()
    }

    fun requestSessionUser() : Job
    {
        return CoroutineScope(Dispatchers.Main + Job()).launch {

            if (mDataRepository.getCacheLoginKey().isEmpty()) // 회원가입 하기전에는 세션을 확인하지 않는다.
                return@launch

            val request = SessionRequest.toRequest(app, mDataRepository.getCacheUserKey(), mDataRepository.getCacheLoginKey())
            mDataRepository.requestSession(request).collect {
                if (it.errorCode == ErrorCase.SERVER_ERROR_SESSION_EXPIRED)
                {
                    mDataRepository.clearAllUserCache()
                    privateSessionLiveData.value = it
                }
            }
        }
    }

    fun fetchUserPoint()
    {
        viewModelScope.launch {
            protectedUserPointResponseLiveData.value = Resource.Loading()
            val request = BaseRequest.toRequest(app, mDataRepository.getCacheUserKey())
            mDataRepository.fetchUserPoint(request).collect {
                protectedUserPointResponseLiveData.value = it
                val userPoint = it.data?.user_point
                if (!userPoint.isNullOrEmpty())
                    getCommonPointLiveData().value = userPoint.toInt()
            }
        }
    }

    fun getCachePoint() : String
    {
        return (getMyApp().mPointLiveData.value ?: 0).toString()
    }

    fun getMyApp() : MyApp = app as MyApp

    fun getCommonPointLiveData() : MutableLiveData<Int> = getMyApp().mPointLiveData
}