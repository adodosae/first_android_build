package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class PurchaseResponse(

    @Field("res_code")
    val res_code : String,

    @Field("res_msg")
    val res_msg : String,

    @Field("user_point")
    val user_point : String
)
