package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class SessionRequest private constructor(
        @Field("app_key")
        val app_key : String,

        @Field("user_key")
        val user_key : String,

        @Field("version")
        val version : String,

        @Field("android_id")
        val android_id : String,

        @Field("login_key")
        val login_key : String
)
{
        companion object
        {
                fun toRequest(context: Context, user_key: String, login_key: String) : SessionRequest
                {
                        return SessionRequest(
                                AppConfig.APP_KEY,
                                user_key,
                                Common.getAppVersion(context),
                                Common.getAndroidId(context),
                                login_key
                        )
                }
        }
}
