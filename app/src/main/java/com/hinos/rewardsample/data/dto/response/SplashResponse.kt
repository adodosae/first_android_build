package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class SplashResponse(
    @Field("res_code")
    val res_code : String,

    @Field("res_msg")
    val res_msg : String,

    @Field("email")
    val email : String,

    @Field("refund")
    val refund : String
)
