package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class AppSettingResponse(

        @Field("res_code")
        val res_code : String,

        @Field("res_msg")
        val res_msg : String,

        @Field("min_ver")
        val min_ver : String,

        @Field("app_ver")
        val app_ver : String,

        @Field("pop_ver")
        val pop_ver : String,

        @Field("market_url")
        val market_url : String,

        @Field("ad_skip")
        val ad_skip : String,

        @Field("invite")
        val invite : String,

        @Field("kakaohelp") // 카카오 도움말
        val kakaohelp : String,

        @Field("refundhelp") // 환급신청 도움말
        val refundhelp : String,

        @Field("privacy") // 개인정보처리방침 url
        val privacy : String,

        @Field("termsofservice") // 서비스이용약관 url
        val termsofservice : String
)