package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class CategoryResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("nkey")
    val nkey : String,

    @SerializedName("list")
    val categoryList : List<Data>
) {
    @Parcelize
    data class Data(
        @SerializedName("goods_id")
        val goods_id : String,

        @SerializedName("goods_name")
        val goods_name : String,

        @SerializedName("goods_thumb")
        val goods_thumb : String,

        @SerializedName("goods_price")
        val goods_price : String,

        @SerializedName("brand_name")
        val brand_name : String
    ) : Parcelable
}