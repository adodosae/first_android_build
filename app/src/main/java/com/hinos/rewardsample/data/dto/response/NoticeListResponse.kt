package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class NoticeListResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("list")
    val boardList : List<Data>
) {
    @Parcelize
    data class Data(
        @SerializedName("seq")
        val seq : String,

        @SerializedName("no")
        val no : String,

        @SerializedName("title")
        val title : String,

        @SerializedName("bd_name")
        val bd_name : String,

        @SerializedName("create_date")
        val create_date : String
    ) : Parcelable
}