package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class ChangePwdRequest private constructor(
        @Field("app_key")
        val app_key : String,

        @Field("user_key")
        val user_key : String,

        @Field("android_id")
        val android_id : String,

        @Field("version")
        val version : String,

        @Field("email")
        val email : String,

        @Field("pwd")
        val pwd : String,

        @Field("new_pwd1")
        val new_pwd1 : String,

        @Field("new_pwd2")
        val new_pwd2 : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, email : String, pwd: String, new_pwd1: String, new_pwd2: String) : ChangePwdRequest
        {
            return ChangePwdRequest(
                    app_key = AppConfig.APP_KEY,
                    user_key = user_key,
                    android_id = Common.getAndroidId(context),
                    version = Common.getAppVersion(context),
                    email = email,
                    pwd = pwd,
                    new_pwd1 = new_pwd1,
                    new_pwd2 = new_pwd2
            )
        }
    }
}