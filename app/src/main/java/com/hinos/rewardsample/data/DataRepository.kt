package com.hinos.rewardsample.data

import com.hinos.rewardsample.data.dto.request.*
import com.hinos.rewardsample.data.dto.response.*
import com.hinos.rewardsample.data.local.LocalData
import com.hinos.rewardsample.data.remote.RemoteData
import com.hinos.rewardsample.data.remote.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class DataRepository @Inject constructor(
        private val mRemoteRepository : RemoteData,
        private val mLocalRepository : LocalData,
        private val mIoDispatcher : CoroutineContext) : DataRepositorySource
{
    override suspend fun requestSplash(splashRequest: SplashRequest): Flow<Resource<SplashResponse>> {
        return flow {
            emit(mRemoteRepository.requestSplash(splashRequest = splashRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestAppSetting(request: AppSettingRequest): Flow<Resource<AppSettingResponse>> {
        return flow {
            emit(mRemoteRepository.requestAppSetting(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun doLogin(loginRequest: LoginRequest): Flow<Resource<LoginResponse>> {
        return flow {
            emit(mRemoteRepository.doLogin(loginRequest = loginRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestChangeHumanState(request: BaseRequest): Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.requestChangeHumanState(request = request))
        }.flowOn(mIoDispatcher)
    }



    override suspend fun directHeartBeat(request: BaseRequest): Resource<ResCode> {
        return mRemoteRepository.doHeartBeat(request = request)
    }

    override suspend fun fetchUserPoint(request: BaseRequest): Flow<Resource<PurchaseResponse>> {
        return flow {
            emit(mRemoteRepository.fetchUserPoint(request = request))
        }.flowOn(mIoDispatcher)
    }



    override suspend fun requestSession(request: SessionRequest): Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.requestSession(request = request))
        }.flowOn(mIoDispatcher)
    }


    override suspend fun requestPhoneAuth(phoneAuthRequest: PhoneAuthRequest): Flow<Resource<PhoneAuthResponse>> {
        return flow {
            emit(mRemoteRepository.requestPhoneAuth(phoneAuthRequest = phoneAuthRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestOverlap(overlapRequest: OverlapRequest): Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.requestOverlap(request = overlapRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun findEmail(findEmailRequest: FindEmailRequest): Flow<Resource<FindEmailResponse>> {
        return flow {
            emit(mRemoteRepository.findEmail(request = findEmailRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun findPwd(findPwdRequest: FindPwdRequest): Flow<Resource<FindPwdResponse>> {
        return flow {
            emit(mRemoteRepository.findPwd(request = findPwdRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun checkSession(sessionRequest: SessionRequest): Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.checkSession(sessionRequest = sessionRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun signInUser(signInRequest: SignInRequest): Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.signInUser(signInRequest = signInRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun signOutUser(signOutRequest : SignOutRequest) : Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.signOutUser(signOutRequest = signOutRequest))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestChangePassword(request : ChangePwdRequest) : Flow<Resource<ResCode>> {
        return flow {
            emit(mRemoteRepository.requestChangePassword(request = request))
        }.flowOn(mIoDispatcher)
    }


    override suspend fun requestPurchaseHistory(request : BaseRequest) : Flow<Resource<PurchaseHistoryResponse>> {
        return flow {
            emit(mRemoteRepository.requestPurchaseHistory(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestPointHistory(request : BaseRequest) : Flow<Resource<PointHistoryResponse>> {
        return flow {
            emit(mRemoteRepository.requestPointHistory(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestRefundHistory(request : BaseRequest) : Flow<Resource<RefundHistoryResponse>> {
        return flow {
            emit(mRemoteRepository.requestRefundHistory(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestStore(request: BaseRequest): Flow<Resource<StoreResponse>> {
        return flow {
            emit(mRemoteRepository.requestStore(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestCategory(request: CategoryRequest): Flow<Resource<CategoryResponse>> {
        return flow {
            emit(mRemoteRepository.requestCategory(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestCategoryDetail(request: CategoryDetailRequest): Flow<Resource<CategoryDetailResponse>> {
        return flow {
            emit(mRemoteRepository.requestCategoryDetail(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestCategorySearch(request: CategorySearchRequest): Flow<Resource<CategorySearchResponse>> {
        return flow {
            emit(mRemoteRepository.requestCategorySearch(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun purchaseProduct(request: CategoryDetailRequest): Flow<Resource<PurchaseResponse>> {
        return flow {
            emit(mRemoteRepository.purchaseProduct(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestPurchaseDetail(request: PurchaseDetailRequest): Flow<Resource<PurchaseDetailResponse>> {
        return flow {
            emit(mRemoteRepository.requestPurchaseDetail(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestPurchaseImoge(request : ImogeRequest) : Flow<Resource<PurchaseResponse>>
    {
        return flow {
            emit(mRemoteRepository.requestPurchaseImoge(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestRefund(request : RefundRequest) : Flow<Resource<PurchaseResponse>>
    {
        return flow {
            emit(mRemoteRepository.requestRefund(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun cancelRefund(request : RefundCancelRequest) : Flow<Resource<PurchaseResponse>>
    {
        return flow {
            emit(mRemoteRepository.cancelRefund(request = request))
        }.flowOn(mIoDispatcher)
    }




    override suspend fun requestBoard(request : NoticeListRequest) : Flow<Resource<NoticeListResponse>>
    {
        return flow {
            emit(mRemoteRepository.requestBoard(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestQnaHistory(request : BaseRequest) : Flow<Resource<QnaListResponse>>
    {
        return flow {
            emit(mRemoteRepository.requestQnaHistory(request = request))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestWriteInquiry(param : Map<String, RequestBody>, img_url_1 : MultipartBody.Part?, img_url_2 : MultipartBody.Part?, img_url_3 : MultipartBody.Part?) : Flow<Resource<ResCode>>
    {
        return flow {
            emit(mRemoteRepository.requestWriteInquiry(param, img_url_1, img_url_2, img_url_3))
        }.flowOn(mIoDispatcher)
    }

    override suspend fun requestNoticeDetail(request : NoticeReadRequest) : Flow<Resource<NoticeReadResponse>>
    {
        return flow {
            emit(mRemoteRepository.requestNoticeDetail(request = request))
        }.flowOn(mIoDispatcher)
    }











// local

    override suspend fun cacheUserKey(userKey: String) {
        mLocalRepository.cacheUserKey(userKey)
    }

    override suspend fun getCacheUserKey() : String {
        return mLocalRepository.getCacheUserKey()
    }

    override suspend fun cacheLoginKey(loginKey: String) {
        mLocalRepository.cacheLoginKey(loginKey)
    }

    override suspend fun getCacheLoginKey() : String {
        return mLocalRepository.getCacheLoginKey()
    }

    override suspend fun cacheNickname(name: String) {
        mLocalRepository.cacheNickname(name)
    }

    override suspend fun getCacheNickname() : String {
        return mLocalRepository.getCacheNickname()
    }

    override suspend fun cacheEmail(email: String) {
        mLocalRepository.cacheEmail(email)
    }
    override suspend fun getCacheEmail() : String {
        return mLocalRepository.getCacheEmail()
    }


//    override suspend fun cachePoint(point: String) {
//        mLocalRepository.cachePoint(point)
//    }
//
//    override suspend fun getCachePoint(): String {
//        return mLocalRepository.getCachePoint()
//    }

    override suspend fun cacheRefund(refund: String) {
        mLocalRepository.cacheRefund(refund)
    }

    override suspend fun getCacheRefund(): String {
        return mLocalRepository.getCacheRefund()
    }

    override suspend fun cacheUpdateKey(uKey: String) {
        mLocalRepository.cacheUpdateKey(uKey)
    }

    override suspend fun getCacheUpdateKey(): String {
        return mLocalRepository.getCacheUpdateKey()
    }

    override suspend fun clearAllUserCache() {
        mLocalRepository.cacheUpdateKey("")
        mLocalRepository.cacheEmail("")
        mLocalRepository.cacheUserKey("")
        mLocalRepository.cacheLoginKey("")
    }

    override suspend fun cacheReviewPopupTime(millions: Long) {
        mLocalRepository.cacheReviewLastPopupTime(millions)
    }

    override suspend fun getCacheReviewPopupTime() : Long {
        return mLocalRepository.getCacheReviewLastPopupTime()
    }


    override suspend fun cacheAppVer(value: String) {
        mLocalRepository.cacheAppVer(value)
    }

    override suspend fun getAppVer(): String {
        return mLocalRepository.getAppVer()
    }

    override suspend fun cacheMinVer(value: String) {
        mLocalRepository.cacheMinVer(value)
    }

    override suspend fun getCacheMinVer(): String {
                return mLocalRepository.getCacheMinVer()
    }

    override suspend fun cachePopVer(value: String) {
        mLocalRepository.cachePopVer(value)
    }


    override suspend fun getCachePopVer(): String {
                return mLocalRepository.getCachePopVer()
    }

    override suspend fun cacheMarketUrl(value: String) {
        mLocalRepository.cacheMarketUrl(value)
    }

    override suspend fun getCacheMarketUrl(): String {
                return mLocalRepository.getCacheMarketUrl()
    }

    override suspend fun cacheAdSkip(value: String) {
        mLocalRepository.cacheAdSkip(value)
    }

    override suspend fun getCacheAdSkip(): String {
                return mLocalRepository.getCacheAdSkip()
    }

    override suspend fun cacheInviteUrl(value: String) {
        mLocalRepository.cacheInviteUrl(value)
    }

    override suspend fun getCacheInviteUrl(): String {
        return mLocalRepository.getCacheInviteUrl()
    }

    override suspend fun cacheKakaoHelp(value: String) {
        mLocalRepository.cacheKakaoHelp(value)
    }

    override suspend fun getCacheKakaoHelp(): String {
        return mLocalRepository.getCacheKakaoHelp()
    }

    override suspend fun cacheRefundHelp(value: String) {
        mLocalRepository.cacheRefundHelp(value)
    }

    override suspend fun getCacheRefundHelp(): String {
        return mLocalRepository.getCacheRefundHelp()
    }

    override suspend fun cachePrivacy(value: String) {
        mLocalRepository.cachePrivacy(value)
    }

    override suspend fun getCachePrivacy(): String {
        return mLocalRepository.getCachePrivacy()
    }

    override suspend fun cacheTermSofService(value: String) {
        mLocalRepository.cacheTermSofService(value)
    }

    override suspend fun getCacheTermSofService(): String {
        return mLocalRepository.getCacheTermSofService()
    }
}