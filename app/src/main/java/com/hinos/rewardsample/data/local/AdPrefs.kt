package com.hinos.rewardsample.data.local

import android.app.Activity
import android.content.Context



/**
 * Created by apulsa on 2016-11-13.
 */

object AdPrefs
{
    private val AD_CONFIG = "CONFIG"

    fun readDenyUnityPermissionCount(context: Context?): Int {
        return readIntValue(context!!, AD_CONFIG, "DenyUnityPermissionCount", 0)
    }

    fun writeDenyUnityPermissionCount(context: Context?, `val`: Int) {
        writeIntValue(context!!, AD_CONFIG, "DenyUnityPermissionCount", `val`)
    }

    fun writeIntValue(context: Context, strName: String,
                      strKey: String, nValue: Int) {
        val pref = context.getSharedPreferences(strName,
            Activity.MODE_PRIVATE)

        val ed = pref.edit()

        ed.putInt(strKey, nValue)
        ed.commit()
    }

    fun readIntValue(context: Context, strName: String,
                     strKey: String, nDefValue: Int): Int {
        val pref = context.getSharedPreferences(strName,
            Activity.MODE_PRIVATE)

        return pref.getInt(strKey, nDefValue)
    }

}
