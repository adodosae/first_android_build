package com.hinos.rewardsample.data.dto.response

import com.google.gson.annotations.SerializedName

data class CategoryDetailResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("goods_id")
    val goods_id : String,

    @SerializedName("goods_name")
    val goods_name : String,

    @SerializedName("goods_thumb")
    val goods_thumb : String,

    @SerializedName("goods_price")
    val goods_price : String,

    @SerializedName("brand_name")
    val brand_name : String,

    @SerializedName("goods_content")
    val goods_content : String

)
