package com.hinos.rewardsample.data.remote.service

import com.hinos.rewardsample.data.dto.response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface BoardService {

    @POST("/wapi/board/notice_list.php")
    suspend fun requestBoard(@Body request : Any) : Response<NoticeListResponse>

    @POST("/wapi/board/get_qna.php")
    suspend fun requestQnaHistory(@Body request : Any) : Response<QnaListResponse>

    @POST("/wapi/board/get_notice.php")
    suspend fun requestNoticeDetail(@Body request : Any) : Response<NoticeReadResponse>

//    @Multipart
//    @POST ("/wapi/board/set_qna.php")//photo_upload.php
//    fun requestWriteInquiry(
//        @PartMap params : Map<String, @JvmSuppressWildcards RequestBody>,
//        @Part img_url_1:              MultipartBody.Part?,
//        @Part img_url_2:              MultipartBody.Part?,
//        @Part img_url_3:              MultipartBody.Part?
//
////        @Part("app_key")        app_key: RequestBody,
////        @Part("user_key")       user_key: RequestBody,
////        @Part("version")        version: RequestBody,
////        @Part("android_id")     android_id: RequestBody,
////        @Part("bd_name")        bd_name: RequestBody,
////        @Part("lang")           lang: RequestBody,
////        @Part("country")        country: RequestBody,
////        @Part("os_ver")         os_ver: RequestBody,
////        @Part("model")          model: RequestBody,
////        @Part("man")            man: RequestBody,
////        @Part("email")          email: RequestBody,
////        @Part("title")          title: RequestBody,
////        @Part("contents")       contents: RequestBody,
////        @Part img_url_1:              MultipartBody.Part?,
////        @Part img_url_2:              MultipartBody.Part?,
////        @Part img_url_3:              MultipartBody.Part?
//    ): Response<ResCode>
//    // 사진



    @Multipart
    @POST ("/wapi/board/set_qna.php")//photo_upload.php
    fun requestWriteInquiry2(
//        @PartMap params : Map<String, @JvmSuppressWildcards RequestBody>,
//        @Part img_url_1:              MultipartBody.Part?,
//        @Part img_url_2:              MultipartBody.Part?,
//        @Part img_url_3:              MultipartBody.Part?

        @Part("app_key")        app_key: RequestBody?,
        @Part("user_key")       user_key: RequestBody?,
        @Part("version")        version: RequestBody?,
        @Part("android_id")     android_id: RequestBody?,
        @Part("bd_name")        bd_name: RequestBody?,
        @Part("lang")           lang: RequestBody?,
        @Part("country")        country: RequestBody?,
        @Part("os_ver")         os_ver: RequestBody?,
        @Part("model")          model: RequestBody?,
        @Part("man")            man: RequestBody?,
        @Part("email")          email: RequestBody?,
        @Part("title")          title: RequestBody?,
        @Part("contents")       contents: RequestBody?,
        @Part img_url_1:              MultipartBody.Part?,
        @Part img_url_2:              MultipartBody.Part?,
        @Part img_url_3:              MultipartBody.Part?
    ): Call<ResCode>



    @Multipart
    @POST ("/wapi/board/set_qna.php")//photo_upload.php
    fun requestWriteInquiry3(
//        @PartMap params : Map<String, @JvmSuppressWildcards RequestBody>,
//        @Part img_url_1:              MultipartBody.Part?,
//        @Part img_url_2:              MultipartBody.Part?,
//        @Part img_url_3:              MultipartBody.Part?

        @Part("app_key")        app_key: RequestBody?,
        @Part("user_key")       user_key: RequestBody?,
        @Part("version")        version: RequestBody?,
        @Part("android_id")     android_id: RequestBody?,
        @Part("bd_name")        bd_name: RequestBody?,
        @Part("lang")           lang: RequestBody?,
        @Part("country")        country: RequestBody?,
        @Part("os_ver")         os_ver: RequestBody?,
        @Part("model")          model: RequestBody?,
        @Part("man")            man: RequestBody?,
        @Part("email")          email: RequestBody?,
        @Part("title")          title: RequestBody?,
        @Part("contents")       contents: RequestBody?,
        @Part img_url_1:              MultipartBody.Part?,
        @Part img_url_2:              MultipartBody.Part?,
        @Part img_url_3:              MultipartBody.Part?
    ): Call<ResCode>
}