package com.hinos.rewardsample.data.dto.response

import com.google.gson.annotations.SerializedName

data class FindEmailResponse(
        @SerializedName("res_code")
        val res_code : String,

        @SerializedName("res_msg")
        val res_msg : String,

        @SerializedName("list")
        val emailList : List<Data>
) {
        data class Data(
                @SerializedName("email")
                val email : String
        )
}