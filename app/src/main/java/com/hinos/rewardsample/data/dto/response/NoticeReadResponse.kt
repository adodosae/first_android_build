package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class NoticeReadResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("bd_name")
    val bd_name : String,

    @SerializedName("no")
    val no : String,

    @SerializedName("wr_date")
    val wr_date : String,

    @SerializedName("name")
    val name : String,

    @SerializedName("title")
    val title : String,

    @SerializedName("contents")
    val contents : String
)