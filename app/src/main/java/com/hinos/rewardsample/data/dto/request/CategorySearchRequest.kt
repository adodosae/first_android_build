package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class CategorySearchRequest private constructor(
    @Field("app_key")
    val app_key : String,

    @Field("user_key")
    val user_key : String,

    @Field("version")
    val version : String,

    @Field("android_id")
    val android_id : String,

    @Field("ukey")
    val ukey : String,

    @Field("search")
    val search : String,

    @Field("page_cnt")
    val page_cnt : String,

    @Field("list_cnt")
    val list_cnt : String

) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, search: String, ukey: String, page_cnt: String, list_cnt: String) : CategorySearchRequest
        {
            return CategorySearchRequest(
                app_key = AppConfig.APP_KEY,
                user_key = user_key,
                version = Common.getAppVersion(context),
                android_id = Common.getAndroidId(context),
                ukey = ukey,
                search = search,
                page_cnt = page_cnt,
                list_cnt = list_cnt
            )
        }
    }
}
