package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class BaseRequest(
    @Field("app_key")
    val app_key : String,

    @Field("user_key")
    val user_key : String,

    @Field("android_id")
    val android_id : String,

    @Field("version")
    val version : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String) : BaseRequest
        {
            return BaseRequest(
                app_key = AppConfig.APP_KEY,
                user_key = user_key,
                android_id = Common.getAndroidId(context),
                version = Common.getAppVersion(context)
            )
        }
    }
}