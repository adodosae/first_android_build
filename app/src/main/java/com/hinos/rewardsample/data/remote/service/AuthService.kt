package com.hinos.rewardsample.data.remote.service

import com.hinos.rewardsample.data.dto.response.PhoneAuthResponse
import com.hinos.rewardsample.data.dto.response.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

//    @POST("wapi/login/login.php")
//    suspend fun <T> doLogin(@Body loginRequest : T) : Response<LoginResponse>


    @POST("wapi/member/certification_phone.php")
    suspend fun requestPhoneAuth(@Body request : Any) : Response<PhoneAuthResponse>

    @POST("wapi/member/check_overlap.php")
    suspend fun requestOverlap(@Body request: Any) : Response<ResCode>

    @POST("wapi/member/sign_user.php") //회원가입
    suspend fun signInUser(@Body signInRequest : Any) : Response<ResCode>

    @POST("wapi/member/secession_user.php") //회원 탈퇴
    suspend fun signOutUser(@Body signOutRequest: Any) : Response<ResCode>

    @POST("wapi/member/find_id.php") // 이메일 찾기
    suspend fun findEmail(@Body request : Any) : Response<FindEmailResponse>

    @POST("wapi/member/find_pwd.php") // 패스워드 찾기
    suspend fun findPwd(@Body request : Any) : Response<FindPwdResponse>

    @POST("wapi/member/change_pwd.php") // 패스워드 찾기
    suspend fun requestChangePassword(@Body request : Any) : Response<ResCode>
}