package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class PurchaseDetailResponse(
    @Field("res_code")
    val res_code : String,

    @Field("res_msg")
    val res_msg : String,

    @Field("product_id")
    val product_id : String,

    @Field("goods_img")
    val goods_img : String,

    @Field("goods_name")
    val goods_name : String,

    @Field("goods_content")
    val goods_content : String,

    @Field("use_date")
    val use_date : String,

    @Field("pincode")
    val pincode : String,

    @Field("pincode_img")
    val pincode_img : String,

    @Field("create_date")
    val create_date : String?,

    @Field("send_type")
    val send_type : String,

    @Field("run_state")
    val run_state : String,

    @Field("goods_state")
    val goods_state : String,

    @Field("send_phone_num")
    val send_phone_num : String,

    @Field("buy_point")
    val buy_point : String,

    @Field("buy_choco")
    val buy_choco : String,

    @Field("buy_url")
    val buy_url : String,

    @Field("payment_memo")
    val payment_memo : String?,

    @Field("send_date")
    val send_date : String?,

    @Field("refund_amount")
    val refund_amount : String,

    @Field("bank_name")
    val bank_name : String,

    @Field("account_num")
    val account_num : String,

    @Field("account_name")
    val account_name : String,

    @Field("payments_date")
    val payments_date : String,

    @Field("refund_memo")
    val refund_memo : String,

    @Field("detail_type")
    val detail_type : String

)
