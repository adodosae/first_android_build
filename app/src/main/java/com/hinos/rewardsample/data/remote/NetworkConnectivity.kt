package com.hinos.rewardsample.data.remote


interface NetworkConnectivity {
    fun isConnected(): Boolean
}