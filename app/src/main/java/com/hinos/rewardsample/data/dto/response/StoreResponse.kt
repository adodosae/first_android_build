package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class StoreResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("list")
    val categoryList : List<Data>
)
{
    @Parcelize
    data class Data(
        @SerializedName("cate_id")
        val cate_id : String,
        @SerializedName("cate_name")
        val cate_name : String,
        @SerializedName("cate_thumb")
        val cate_thumb : String,
        @SerializedName("cate_type")
        val cate_type : String
    ) : Parcelable
}
