package com.hinos.rewardsample.data.local;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;


import com.hinos.rewardsample.util.Encrypt;

import java.util.HashSet;

public class AppPrefs {
	public static final String CONFIG_SETUP = "CONFIG";
	public static boolean writeBooleanValue(Context context,
			String strKey, boolean val) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		SharedPreferences.Editor ed = pref.edit();

		ed.putBoolean(strKey, val);
		return ed.commit();
	}

	public static boolean readBooleanValue(Context context,
			String strKey, boolean val) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		return pref.getBoolean(strKey, val);
	}

	public static boolean writeIntValue(Context context,
			String strKey, int nValue) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		SharedPreferences.Editor ed = pref.edit();

		ed.putInt(strKey, nValue);
		return ed.commit();
	}

	public static int readIntValue(Context context,
			String strKey, int nDefValue) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		return pref.getInt(strKey, nDefValue);
	}

	public static boolean writeLongValue(Context context,
			String strKey, long val) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		SharedPreferences.Editor ed = pref.edit();

		ed.putLong(strKey, val);
		return ed.commit();
	}

	public static long readLongValue(Context context,
			String strKey, long val) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		return pref.getLong(strKey, val);
	}

	public static boolean writeStringValue(Context context,
			String strKey, String strValue) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);

		SharedPreferences.Editor ed = pref.edit();

		ed.putString(strKey, Encrypt.INSTANCE.encodeText(strValue));
		return ed.commit();
	}

	public static String readStringValue(Context context,
			String strKey, String strDefValue) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);
		String getValue = pref.getString(strKey, strDefValue);
		return Encrypt.INSTANCE.decodeText(getValue);
	}

	public static boolean writeSetValue(Context context, String strKey, HashSet<String> set)
	{
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);
		SharedPreferences.Editor ed = pref.edit();
		ed.putStringSet(strKey, set);
		return ed.commit();
	}

	public static HashSet<String> readSetValue(Context context,
											   String strKey) {
		SharedPreferences pref = context.getSharedPreferences(CONFIG_SETUP,
				Activity.MODE_PRIVATE);
		return (HashSet<String>) pref.getStringSet(strKey, new HashSet<String>());
	}




























}
