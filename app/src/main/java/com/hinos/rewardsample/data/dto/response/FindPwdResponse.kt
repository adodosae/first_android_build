package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class FindPwdResponse(
        @Field("res_code")
        val res_code : String,

        @Field("res_msg")
        val res_msg : String,

        @Field("find_url")
        val find_url : String
)