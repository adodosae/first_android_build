package com.hinos.rewardsample.data.remote.service

import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ApplicationService
{
    @POST("wapi/application/buy_emoticon.php")
    suspend fun requestPurchaseImoge(@Body splashRequest : Any) : Response<PurchaseResponse>

    @POST("wapi/application/refund_apply.php")
    suspend fun requestRefund(@Body request : Any) : Response<PurchaseResponse>


    @POST("wapi/application/refund_cancel.php")
    suspend fun cancelRefund(@Body request : Any) : Response<PurchaseResponse>
}