package com.hinos.rewardsample.data

import com.hinos.rewardsample.data.dto.request.*
import com.hinos.rewardsample.data.dto.response.*
import com.hinos.rewardsample.data.local.AppPrefs
import com.hinos.rewardsample.data.remote.Resource
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface DataRepositorySource {
    suspend fun requestSplash(splashRequest: SplashRequest) : Flow<Resource<SplashResponse>>
    suspend fun requestAppSetting(request: AppSettingRequest): Flow<Resource<AppSettingResponse>>
    suspend fun doLogin(loginRequest: LoginRequest): Flow<Resource<LoginResponse>>
    suspend fun requestChangeHumanState(request: BaseRequest): Flow<Resource<ResCode>>
    suspend fun directHeartBeat(request: BaseRequest): Resource<ResCode>
    suspend fun fetchUserPoint(request: BaseRequest): Flow<Resource<PurchaseResponse>>
    suspend fun requestSession(request: SessionRequest): Flow<Resource<ResCode>>
    suspend fun requestPhoneAuth(phoneAuthRequest: PhoneAuthRequest) : Flow<Resource<PhoneAuthResponse>>
    suspend fun requestOverlap(overlapRequest: OverlapRequest) : Flow<Resource<ResCode>> // 중복확인
    suspend fun findEmail(findEmailRequest: FindEmailRequest) : Flow<Resource<FindEmailResponse>>
    suspend fun findPwd(findPwdRequest: FindPwdRequest) : Flow<Resource<FindPwdResponse>>


    suspend fun checkSession(sessionRequest: SessionRequest) : Flow<Resource<ResCode>>
    suspend fun signInUser(signInRequest: SignInRequest) : Flow<Resource<ResCode>>
    suspend fun signOutUser(signOutRequest : SignOutRequest) : Flow<Resource<ResCode>>
    suspend fun requestChangePassword(request : ChangePwdRequest) : Flow<Resource<ResCode>>

    suspend fun requestPurchaseHistory(request: BaseRequest) : Flow<Resource<PurchaseHistoryResponse>>
    suspend fun requestPointHistory(request : BaseRequest) : Flow<Resource<PointHistoryResponse>>
    suspend fun requestRefundHistory(request : BaseRequest) : Flow<Resource<RefundHistoryResponse>>

    suspend fun requestStore(request: BaseRequest) : Flow<Resource<StoreResponse>>
    suspend fun requestCategory(request: CategoryRequest) : Flow<Resource<CategoryResponse>>
    suspend fun requestCategoryDetail(request: CategoryDetailRequest) : Flow<Resource<CategoryDetailResponse>>
    suspend fun requestCategorySearch(request: CategorySearchRequest): Flow<Resource<CategorySearchResponse>>
    suspend fun purchaseProduct(request: CategoryDetailRequest) : Flow<Resource<PurchaseResponse>>
    suspend fun requestPurchaseDetail(request: PurchaseDetailRequest): Flow<Resource<PurchaseDetailResponse>>
    suspend fun requestPurchaseImoge(request : ImogeRequest) : Flow<Resource<PurchaseResponse>>
    suspend fun requestRefund(request : RefundRequest) : Flow<Resource<PurchaseResponse>>
    suspend fun cancelRefund(request : RefundCancelRequest) : Flow<Resource<PurchaseResponse>>


    suspend fun requestBoard(request : NoticeListRequest) : Flow<Resource<NoticeListResponse>>
    suspend fun requestQnaHistory(request : BaseRequest) : Flow<Resource<QnaListResponse>>
    suspend fun requestWriteInquiry(param : Map<String, RequestBody>, img_url_1 : MultipartBody.Part?, img_url_2 : MultipartBody.Part?, img_url_3 : MultipartBody.Part?) : Flow<Resource<ResCode>>
    suspend fun requestNoticeDetail(request : NoticeReadRequest) : Flow<Resource<NoticeReadResponse>>








    suspend fun cacheUserKey(userKey : String)
    suspend fun getCacheUserKey() : String

    suspend fun cacheLoginKey(loginKey : String)
    suspend fun getCacheLoginKey() : String

    suspend fun cacheEmail(email: String)
    suspend fun getCacheEmail() : String

    suspend fun cacheNickname(name : String)
    suspend fun getCacheNickname() : String

//    suspend fun cachePoint(point : String)
//    suspend fun getCachePoint() : String

    suspend fun cacheRefund(refund : String)
    suspend fun getCacheRefund() : String

    suspend fun cacheUpdateKey(uKey : String)
    suspend fun getCacheUpdateKey() : String


    suspend fun cacheReviewPopupTime(millions: Long)

    suspend fun getCacheReviewPopupTime() : Long

    suspend fun clearAllUserCache()




    suspend fun cacheAppVer(value : String)

    suspend fun getAppVer() : String

    suspend fun cacheMinVer(value : String)

    suspend fun getCacheMinVer() : String

    suspend fun cachePopVer(value : String)

    suspend fun getCachePopVer() : String

    suspend fun cacheMarketUrl(value : String)

    suspend fun getCacheMarketUrl() : String

    suspend fun cacheAdSkip(value : String)

    suspend fun getCacheAdSkip() : String

    suspend fun cacheInviteUrl(value: String)

    suspend fun getCacheInviteUrl(): String


    suspend fun cacheKakaoHelp(value : String)

    suspend fun getCacheKakaoHelp(): String

    suspend fun cacheRefundHelp(value: String)

    suspend fun getCacheRefundHelp(): String

    suspend fun cachePrivacy(value: String)

    suspend fun getCachePrivacy(): String

    suspend fun cacheTermSofService(value: String)

    suspend fun getCacheTermSofService(): String
}
