package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common

data class SignOutRequest(
    val app_key : String,
    val user_key : String,
    val version : String,
    val email : String,
    val pwd : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, email: String, pwd : String) : SignOutRequest
        {
            return SignOutRequest(
                app_key = AppConfig.APP_KEY,
                user_key = user_key,
                version = Common.getAppVersion(context),
                email = email,
                pwd = pwd
            )
        }
    }
}
