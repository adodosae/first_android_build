package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class LoginRequest(

    @Field("app_key")
    val app_key : String,

    @Field("user_key")
    val user_key : String,

    @Field("version")
    val version : String,

    @Field("android_id")
    val android_id : String,

    @Field("login_key")
    val login_key : String,

    @Field("email")
    val email : String,

    @Field("pwd")
    val pwd : String,

    @Field("phone_num")
    val phone_num : String,

    @Field("model")
    val model : String,

    @Field("os_ver")
    val os_ver : String,

    @Field("lang")
    val lang : String,

    @Field("country")
    val country : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, login_key: String, email: String, pwd : String, phone_num: String) : LoginRequest
        {
            return LoginRequest(
                    app_key = AppConfig.APP_KEY,
                    user_key = user_key,
                    login_key = login_key,
                    android_id = Common.getAndroidId(context),
                    version = Common.getAppVersion(context),
                    model = Common.getPhoneModel(),
                    os_ver = Common.getAndroidVersionCode(),
                    lang = Common.getLanguageCode(context),
                    country = Common.getCountryCode(context),
                    email = email,
                    pwd = pwd,
                    phone_num = phone_num
            )
        }
    }
}