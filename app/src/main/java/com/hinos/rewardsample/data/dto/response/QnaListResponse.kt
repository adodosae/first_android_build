package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class QnaListResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("list")
    val qnaList : List<Data>
) {
    @Parcelize
    data class Data(

        @SerializedName("no")
        val no : String,

        @SerializedName("title")
        val title : String,

        @SerializedName("wr_time")
        val wr_time : String,

        @SerializedName("bd_name")
        val bd_name : String,

        @SerializedName("contents")
        val contents : String,

        @SerializedName("reply_contents")
        val reply_contents : String,

        @SerializedName("reply") // 답변유무 0:N, 1:Y
        val reply : String,

        @SerializedName("img_url1")
        val img_url1 : String,

        @SerializedName("img_url2")
        val img_url2 : String,

        @SerializedName("img_url3")
        val img_url3 : String
    ) : Parcelable

}