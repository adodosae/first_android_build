package com.hinos.rewardsample.data.remote

import com.hinos.rewardsample.data.dto.request.PhoneAuthRequest
import com.hinos.rewardsample.data.dto.response.PhoneAuthResponse
import com.hinos.rewardsample.data.dto.request.LoginRequest
import com.hinos.rewardsample.data.dto.request.OverlapRequest
import com.hinos.rewardsample.data.dto.request.SessionRequest
import com.hinos.rewardsample.data.dto.request.SignInRequest
import com.hinos.rewardsample.data.dto.response.LoginResponse
import com.hinos.rewardsample.data.dto.response.ResCode

interface RemoteDataSource
{
    suspend fun doLogin(loginRequest : LoginRequest) : Resource<LoginResponse>

    suspend fun requestPhoneAuth(phoneAuthRequest: PhoneAuthRequest): Resource<PhoneAuthResponse>

    suspend fun requestOverlap(requestOverlap : OverlapRequest) : Resource<ResCode>

    suspend fun checkSession(sessionRequest : SessionRequest) : Resource<ResCode>
    
    suspend fun signInUser(signInRequest : SignInRequest) : Resource<ResCode>
}