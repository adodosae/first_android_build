package com.hinos.rewardsample.data.remote.service

import com.hinos.rewardsample.data.dto.response.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginService
{
    @POST("wapi/login/splash.php")
    suspend fun requestSplash(@Body request : Any) : Response<SplashResponse>

    @POST("wapi/login/set_update.php")
    suspend fun requestAppSetting(@Body request : Any) : Response<AppSettingResponse>

    @POST("wapi/login/check_session.php")
    suspend fun requestSession(@Body request : Any) : Response<ResCode>

    @POST("wapi/login/login.php")
    suspend fun doLogin(@Body request : Any) : Response<LoginResponse>

    @POST("wapi/login/dormant_login.php")
    suspend fun requestChangeHumanState(@Body request : Any) : Response<ResCode>

    @POST("wapi/login/heartbeat.php")
    suspend fun doHeartBeat(@Body request : Any) : Response<ResCode>


    @POST("wapi/login/login.php")
    suspend fun checkSession(@Body request : Any) : Response<ResCode>

    @POST("wapi/login/refresh_point.php")
    suspend fun fetchUserPoint(@Body request : Any) : Response<PurchaseResponse>
}