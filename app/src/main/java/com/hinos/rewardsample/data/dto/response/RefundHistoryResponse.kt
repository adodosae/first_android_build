package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class RefundHistoryResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("list")
    val historyItems : ArrayList<Data>
) {
    @Parcelize
    data class Data(
        @SerializedName("seq")
        val seq : String,

        @SerializedName("bank_name")
        val bank_name : String,

        @SerializedName("refund_amount")
        val refund_price : String,

        @SerializedName("refund_date")
        val refund_date : String
    ) : Parcelable
}
