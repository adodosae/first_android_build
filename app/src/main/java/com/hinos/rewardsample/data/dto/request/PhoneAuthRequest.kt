package com.hinos.rewardsample.data.dto.request

import retrofit2.http.Field

data class PhoneAuthRequest(
    @Field("app_key")
    val app_key : String,

    @Field("version")
    val version : String,

    @Field("android_id")
    val android_id : String,

    @Field("phone_num")
    val phone_num : String,

    @Field("hash_key")
    val hash_key : String,

    @Field("proof_key") // 인증번호
    val proof_key : String
)
