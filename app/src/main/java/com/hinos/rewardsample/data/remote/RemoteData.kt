package com.hinos.rewardsample.data.remote

import android.util.Log
import com.hinos.rewardsample.data.dto.request.PhoneAuthRequest
import com.hinos.rewardsample.data.dto.response.PhoneAuthResponse
import com.hinos.rewardsample.data.dto.request.*
import com.hinos.rewardsample.data.dto.response.*
import com.hinos.rewardsample.data.remote.service.*
import com.hinos.rewardsample.util.ErrorCase
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class RemoteData @Inject constructor(private val mServiceGenerator : ServiceGenerator, private val mNetworkConnectivity : NetworkConnectivity)
{
    suspend fun requestSplash(splashRequest: SplashRequest): Resource<SplashResponse> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(splashRequest, service::requestSplash)) {
            is SplashResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestAppSetting(request: AppSettingRequest): Resource<AppSettingResponse> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(request, service::requestAppSetting)) {
            is AppSettingResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestSession(request: SessionRequest): Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(request, service::requestSession)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun doLogin(loginRequest: LoginRequest): Resource<LoginResponse> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(loginRequest, service::doLogin)) {
            is LoginResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                 else
                     Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestChangeHumanState(request: BaseRequest): Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(request, service::requestChangeHumanState)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }


    suspend fun doHeartBeat(request: BaseRequest): Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(request, service::doHeartBeat)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }



    suspend fun fetchUserPoint(request: BaseRequest): Resource<PurchaseResponse> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(request, service::fetchUserPoint)) {
            is PurchaseResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun checkSession(sessionRequest: SessionRequest): Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(LoginService::class.java)

        return when (val response = processCallWithParam(sessionRequest, service::checkSession)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }

    suspend fun signInUser(signInRequest: SignInRequest): Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)

        return when (val response = processCallWithParam(signInRequest, service::signInUser)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }

    suspend fun signOutUser(signOutRequest: SignOutRequest) : Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)
        return when (val response = processCallWithParam(signOutRequest, service::signOutUser)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }

    suspend fun requestChangePassword(request: ChangePwdRequest) : Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)
        return when (val response = processCallWithParam(request, service::requestChangePassword)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }





    suspend fun findPwd(request : FindPwdRequest): Resource<FindPwdResponse> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)

        return when (val response = processCallWithParam(request, service::findPwd)) {
            is FindPwdResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun findEmail(request : FindEmailRequest): Resource<FindEmailResponse> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)

        return when (val response = processCallWithParam(request, service::findEmail)) {
            is FindEmailResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestPhoneAuth(phoneAuthRequest: PhoneAuthRequest): Resource<PhoneAuthResponse> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)

        return when (val response = processCallWithParam(phoneAuthRequest, service::requestPhoneAuth)) {
            is PhoneAuthResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }

    suspend fun requestOverlap(request : OverlapRequest): Resource<ResCode> {
        val service = mServiceGenerator.createEncryptService(AuthService::class.java)

        return when (val response = processCallWithParam(request, service::requestOverlap)) {
            is ResCode -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> {
                Resource.DataError(errorCode = response as Int)
            }
        }
    }

    suspend fun requestPurchaseHistory(request : BaseRequest) : Resource<PurchaseHistoryResponse> {
        val service = mServiceGenerator.createEncryptService(HistoryService::class.java)

        return when (val response = processCallWithParam(request, service::requestPurchaseHistory)) {
            is PurchaseHistoryResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestPointHistory(request : BaseRequest) : Resource<PointHistoryResponse> {
        val service = mServiceGenerator.createEncryptService(HistoryService::class.java)

        return when (val response = processCallWithParam(request, service::requestPointHistory)) {
            is PointHistoryResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestRefundHistory(request : BaseRequest) : Resource<RefundHistoryResponse> {
        val service = mServiceGenerator.createEncryptService(HistoryService::class.java)

        return when (val response = processCallWithParam(request, service::requestRefundHistory)) {
            is RefundHistoryResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestPurchaseDetail(request : PurchaseDetailRequest) : Resource<PurchaseDetailResponse> {
        val service = mServiceGenerator.createEncryptService(HistoryService::class.java)

        return when (val response = processCallWithParam(request, service::requestPurchaseDetail)) {
            is PurchaseDetailResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestPurchaseImoge(request : ImogeRequest) : Resource<PurchaseResponse> {
        val service = mServiceGenerator.createEncryptService(ApplicationService::class.java)

        return when (val response = processCallWithParam(request, service::requestPurchaseImoge)) {
            is PurchaseResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }


    suspend fun requestRefund(request : RefundRequest) : Resource<PurchaseResponse> {
        val service = mServiceGenerator.createEncryptService(ApplicationService::class.java)

        return when (val response = processCallWithParam(request, service::requestRefund)) {
            is PurchaseResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun cancelRefund(request : RefundCancelRequest) : Resource<PurchaseResponse> {
        val service = mServiceGenerator.createEncryptService(ApplicationService::class.java)

        return when (val response = processCallWithParam(request, service::cancelRefund)) {
            is PurchaseResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }





    suspend fun requestStore(request : BaseRequest) : Resource<StoreResponse> {
        val service = mServiceGenerator.createEncryptService(GiftShowService::class.java)

        return when (val response = processCallWithParam(request, service::requestStore)) {
            is StoreResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestCategory(request : CategoryRequest) : Resource<CategoryResponse> {
        val service = mServiceGenerator.createEncryptService(GiftShowService::class.java)

        return when (val response = processCallWithParam(request, service::requestCategory)) {
            is CategoryResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestCategoryDetail(request : CategoryDetailRequest) : Resource<CategoryDetailResponse> {
        val service = mServiceGenerator.createEncryptService(GiftShowService::class.java)

        return when (val response = processCallWithParam(request, service::requestCategoryDetail)) {
            is CategoryDetailResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestCategorySearch(request : CategorySearchRequest) : Resource<CategorySearchResponse> {
        val service = mServiceGenerator.createEncryptService(GiftShowService::class.java)

        return when (val response = processCallWithParam(request, service::requestCategorySearch)) {
            is CategorySearchResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun purchaseProduct(request : CategoryDetailRequest) : Resource<PurchaseResponse> {
        val service = mServiceGenerator.createEncryptService(GiftShowService::class.java)

        return when (val response = processCallWithParam(request, service::purchaseProduct)) {
            is PurchaseResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestBoard(request : NoticeListRequest) : Resource<NoticeListResponse> {
        val service = mServiceGenerator.createEncryptService(BoardService::class.java)

        return when (val response = processCallWithParam(request, service::requestBoard)) {
            is NoticeListResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestQnaHistory(request : BaseRequest) : Resource<QnaListResponse> {
        val service = mServiceGenerator.createEncryptService(BoardService::class.java)

        return when (val response = processCallWithParam(request, service::requestQnaHistory)) {
            is QnaListResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestNoticeDetail(request : NoticeReadRequest) : Resource<NoticeReadResponse> {
        val service = mServiceGenerator.createEncryptService(BoardService::class.java)

        return when (val response = processCallWithParam(request, service::requestNoticeDetail)) {
            is NoticeReadResponse -> {
                if (response.res_code != "0")
                    Resource.DataError(response.res_code.toInt())
                else
                    Resource.Success(data = response)
            }
            else -> Resource.DataError(errorCode = response as Int)
        }
    }

    suspend fun requestWriteInquiry(param : Map<String, RequestBody>, img_url_1 : MultipartBody.Part?, img_url_2 : MultipartBody.Part?, img_url_3 : MultipartBody.Part?) : Resource<ResCode>
    {
        val service = mServiceGenerator.createService2(BoardService::class.java)
//        val a = service.requestWriteInquiry2(
//            param["app_key"],
//            param["user_key"],
//            param["version"],
//            param["android_id"],
//            param["bd_name"],
//            param["lang"],
//            param["country"],
//            param["os_ver"],
//            param["model"],
//            param["man"],
//            param["email"],
//            param["title"],
//            param["contents"],
//            img_url_1,
//            img_url_2,
//            img_url_3
//        ).enqueue(object : Callback<ResCode> {
//            override fun onResponse(call: Call<ResCode>, response: Response<ResCode>)
//            {
//                val s = response
//            }
//
//            override fun onFailure(call: Call<ResCode>, t: Throwable)
//            {
//                val s = call
//                val s2 = t
//            }
//        })

        val response = service.requestWriteInquiry3(
            param["app_key"],
            param["user_key"],
            param["version"],
            param["android_id"],
            param["bd_name"],
            param["lang"],
            param["country"],
            param["os_ver"],
            param["model"],
            param["man"],
            param["email"],
            param["title"],
            param["contents"],
            img_url_1,
            img_url_2,
            img_url_3
        ).execute()

        return if (response.isSuccessful)
            Resource.Success(response.body()!!)
         else
            Resource.DataError(response.code())
    }

    private suspend fun processCallWithParam(request : Any, responseCall: suspend (Any) -> Response<*>): Any?
    {
        if (!mNetworkConnectivity.isConnected()) {
            return ErrorCase.SERVER_NETWORK_ERROR
        }

        return try {
            val response = responseCall.invoke(request)
            val responseCode = response.code()
            if (response.isSuccessful) {
                response.body()
            } else {
                responseCode
            }
        } catch (e: IOException) {
            Log.e("processCall", "processCall: ${e.message}" )
        }
    }

    private suspend fun processCallWithEmptyParam(responseCall: suspend () -> Response<*>): Any? // Request Param 없을때
    {
        if (!mNetworkConnectivity.isConnected()) {
            return ErrorCase.SERVER_NETWORK_ERROR
        }

        return try {
            val response = responseCall.invoke()
            val responseCode = response.code()
            if (response.isSuccessful) {
                response.body()
            } else {
                responseCode
            }
        } catch (e: IOException) {
            Log.e("processCall", "processCall: ${e.message}" )
        }
    }

    private suspend fun processCallWithParam(param : Map<String, RequestBody>,
                                             img_url_1 : MultipartBody.Part?,
                                             img_url_2 : MultipartBody.Part?,
                                             img_url_3 : MultipartBody.Part?,
                                             responseCall: suspend (param : Map<String, RequestBody>, img_url_1 : MultipartBody.Part?, img_url_2 : MultipartBody.Part?, img_url_3 : MultipartBody.Part?) -> Response<*>): Any?
    {
        if (!mNetworkConnectivity.isConnected()) {
            return ErrorCase.SERVER_NETWORK_ERROR
        }

        return try {
            val response = responseCall.invoke(param, img_url_1, img_url_2, img_url_3)
            val responseCode = response.code()
            if (response.isSuccessful) {
                response.body()
            } else {
                responseCode
            }
        } catch (e: IOException) {
            Log.e("processCall", "processCall: ${e.message}" )
        }
    }
}
