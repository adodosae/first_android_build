package com.hinos.rewardsample.data.dto.item

data class ItemOfferWall(
    val no : String,
    val title : String,
    val type : OfferWallType
)

enum class OfferWallType
{
    NAS,
    ADPOPCORN,
    TNK
}