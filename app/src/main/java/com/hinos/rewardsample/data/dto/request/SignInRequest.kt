package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class SignInRequest constructor(
        @Field("app_key")
        val app_key : String,

        @Field("version")
        val version : String,

        @Field("android_id")
        val android_id : String,

        @Field("model")
        val model : String,

        @Field("os_ver")
        val os_ver : String,

        @Field("lang")
        val lang : String,

        @Field("country")
        val country : String,

        @Field("email")
        val email : String,

        @Field("pwd")
        val pwd : String,

        @Field("nickname")
        val nickname : String,

        @Field("phone_num")
        val phone_num : String,

        @Field("nomination")
        val nomination : String
) {
        companion object
        {
                fun toRequest(context : Context,  email: String, pwd: String, nickname: String, recommend: String) : SignInRequest
                {
                        return SignInRequest(
                                app_key = AppConfig.APP_KEY,
                                version = Common.getAppVersion(context),
                                android_id = Common.getAndroidId(context),
                                model = Common.getPhoneModel(),
                                os_ver = Common.getAndroidVersionCode(),
                                lang = Common.getLanguageCode(context),
                                country = Common.getCountryCode(context),
                                email = email,
                                pwd = pwd,
                                nickname = nickname,
                                phone_num = Common.getDevicePhoneNumber(context),
                                nomination = recommend
                        )
                }
        }
}