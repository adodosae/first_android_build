package com.hinos.rewardsample.data.remote.service

import com.hinos.rewardsample.data.dto.response.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface GiftShowService {

    @POST("/wapi/giftshow/giftshow_category.php")
    suspend fun requestStore(@Body request : Any) : Response<StoreResponse>

    @POST("/wapi/giftshow/giftshow_list.php")
    suspend fun requestCategory(@Body request : Any) : Response<CategoryResponse>

    @POST("/wapi/giftshow/giftshow_detail.php")
    suspend fun requestCategoryDetail(@Body request : Any) : Response<CategoryDetailResponse>

    @POST("/wapi/giftshow/search_giftshow.php")
    suspend fun requestCategorySearch(@Body request : Any) : Response<CategorySearchResponse>

    @POST("/wapi/giftshow/buy_giftshow.php")
    suspend fun purchaseProduct(@Body request : Any) : Response<PurchaseResponse>
}