package com.hinos.rewardsample.data.remote

import android.util.Log
import com.google.gson.Gson
import com.hinos.rewardsample.BuildConfig
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.Encrypt
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import okio.Buffer
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ServiceGenerator @Inject constructor()
{
    companion object
    {
        private const val TIMEOUT_READ = 30
        private const val CONTENT_TYPE = "Content-Type"
        private const val CONTENT_TYPE_VALUE = "application/json"
        private const val TIME_OUT_CONNECT = 30
    }


    private val mOkHttpClient : OkHttpClient.Builder = OkHttpClient.Builder()
    private val mRetrofit : Retrofit

    private val mHeaderInterceptor = Interceptor { chain ->
        val original = chain.request()

        val request = original.newBuilder()
            .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
            .method(original.method, original.body)
            .build()

        chain.proceed(request)
    }

    private val mLoggerInterceptor : HttpLoggingInterceptor
        get() {
            val loggingInterceptor = HttpLoggingInterceptor {
                Log.i("Logger Intercept", Encrypt.decodeText(it))
            }
            if (BuildConfig.DEBUG)
            {
                loggingInterceptor.apply { level = HttpLoggingInterceptor.Level.BODY }
            }
            return loggingInterceptor
        }

    private val mEncryptRequestInterceptor = Interceptor { chain ->
        val original = chain.request()
        val buffer = Buffer()
        original.body?.writeTo(buffer)
        val newBodyString = Encrypt.encodeText(buffer.readUtf8())
        val request = original.newBuilder()
                .post(newBodyString.toRequestBody("text/html; charset=utf-8".toMediaTypeOrNull()))
                .build()
        chain.proceed(request)

//        val original = chain.request()
//        val buffer = Buffer()
//        original.body?.writeTo(buffer)
//        val orgString = buffer.readUtf8()
//        val orgBodyString = Encrypt.encodeText(orgString)
//
//        val requestBody = MultipartBody.Builder()
//                .addFormDataPart("data", orgBodyString)
//                .build()
//
//
////        val jsonObject = JSONObject()
////        jsonObject.put("data", orgBodyString)
//        val request = original.newBuilder()
//                .url(original.url)
//                .post(requestBody)
//                .build()
//
//        chain.proceed(request)
    }

    private val mEncryptResponseInterceptor = Interceptor { chain ->
        val response = chain.proceed(chain.request())
        val orgString = response.body?.string()
        var dataJsonString = Encrypt.decodeText(orgString)
        if (dataJsonString.isEmpty()) {
            dataJsonString = "{\"res_code\" : \"9001\", \"res_message\" : \"json data is empty\"}"
        }
//        val dataJsonString = getDesJSONString(response.body?.string(), "data")

        return@Interceptor response.newBuilder()
                .body(dataJsonString.toResponseBody())
                .build()
    }

    private fun getDesJSONString(jsonString: String?, dataKey : String) : String
    {
        var result : String
        try {
            val jsonObject = JSONObject(jsonString ?: "")
            result = Encrypt.decodeText(jsonObject.getString(dataKey))
        }catch (e : Exception)
        {
            result = "{\"res_code\" : \"9001\", \"res_message\" : \"json data is empty\"}"
        }
        return result
    }

    init {
        mOkHttpClient.run {
//            addInterceptor(mHeaderInterceptor)
            addInterceptor(mEncryptResponseInterceptor)
            addInterceptor(mEncryptRequestInterceptor)
            addInterceptor(mLoggerInterceptor)
            connectTimeout(TIME_OUT_CONNECT.toLong(), TimeUnit.SECONDS)
            readTimeout(TIMEOUT_READ.toLong(), TimeUnit.SECONDS)

            val client = mOkHttpClient.build()
            mRetrofit = Retrofit.Builder()
                .baseUrl(AppConfig.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    private val mRetrofit2 = Retrofit.Builder()
        .baseUrl(AppConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .build()


    fun <S> createEncryptService(serviceClass: Class<S>) : S {
        return mRetrofit.create(serviceClass)
    }

    fun <S> createService2(serviceClass: Class<S>) : S {
        return mRetrofit2.create(serviceClass)
    }
}
