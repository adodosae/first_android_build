package com.hinos.rewardsample.data.local

import android.content.Context

object JobPrefs
{
    fun readLastHeartbeatCheckTime(context: Context): Long
    {
        return AppPrefs.readLongValue(context, "LastHeartbeatCheckTime", 0)
    }
    fun writeLastHeartbeatCheckTime(context: Context, `val`: Long)
    {
        AppPrefs.writeLongValue(context, "LastHeartbeatCheckTime", `val`)
    }
    fun readLastHeartbeatCheckResult(context: Context): Boolean {
        return AppPrefs.readBooleanValue(context, "LastHeartbeatCheckResult", false)
    }
    fun writeLastHeartbeatCheckResult(context: Context, `val`: Boolean) {
        AppPrefs.writeBooleanValue(context, "LastHeartbeatCheckResult", `val`)
    }
    fun readLastHeartbeatSuccessTime(context: Context): Long {
        return AppPrefs.readLongValue(context, "LastHeartbeatSuccessTime", 0)
    }
    fun writeLastHeartbeatSuccessTime(context: Context, `val`: Long) {
        AppPrefs.writeLongValue(context, "LastHeartbeatSuccessTime", `val`)
    }
}