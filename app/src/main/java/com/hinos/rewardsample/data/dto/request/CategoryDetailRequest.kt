package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class CategoryDetailRequest private constructor(
    @Field("app_key")
    val app_key : String,

    @Field("user_key")
    val user_key : String,

    @Field("android_id")
    val android_id : String,

    @Field("version")
    val version : String,

    @Field("goods_id")
    val goods_id : String,

    @Field("ukey")
    val ukey : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, goods_id: String, ukey: String) : CategoryDetailRequest
        {
            return CategoryDetailRequest(AppConfig.APP_KEY, user_key, Common.getAndroidId(context), Common.getAppVersion(context), goods_id, ukey)
        }
    }
}
