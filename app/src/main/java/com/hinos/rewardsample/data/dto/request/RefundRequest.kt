package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class RefundRequest(
    @Field("app_key")
    val app_key : String,

    @Field("user_key")
    val user_key : String,

    @Field("android_id")
    val android_id : String,

    @Field("version")
    val version : String,

    @Field("phone_num")
    val phone_num : String,

    @Field("email")
    val email : String,

    @Field("nickname")
    val nickname : String,

    @Field("refund_amount")
    val refund_amount : String,

    @Field("bank_name")
    val bank_name : String,

    @Field("account_num")
    val account_num : String,

    @Field("account_name")
    val account_name : String,
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, email: String, nickname: String, refund_amount: String, bank_name: String, account_num: String, account_name: String) : RefundRequest
        {
            return RefundRequest(
                app_key = AppConfig.APP_KEY,
                user_key = user_key,
                android_id = Common.getAndroidId(context),
                version = Common.getAppVersion(context),
                phone_num = Common.getDevicePhoneNumber(context),
                email = email,
                nickname = nickname,
                refund_amount = refund_amount,
                bank_name = bank_name,
                account_num = account_num,
                account_name = account_name
            )
        }
    }
}
