package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class CategorySearchResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("nkey")
    var nkey : String,

    @SerializedName("list")
    val categorySearchList : ArrayList<Data>,

    var fromPaging : Boolean = false
) {
    @Parcelize
    data class Data(
        @SerializedName("goods_code")
        val goods_code : String,

        @SerializedName("goods_no")
        val goods_no : String,

        @SerializedName("goods_name")
        val goods_name : String,

        @SerializedName("brand_code")
        val brand_code : String,

        @SerializedName("brand_name")
        val brand_name : String,

        @SerializedName("goods_img")
        val goods_img : String,

        @SerializedName("goods_price")
        val goods_price : String
    ) : Parcelable
}