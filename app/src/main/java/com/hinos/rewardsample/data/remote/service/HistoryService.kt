package com.hinos.rewardsample.data.remote.service

import com.hinos.rewardsample.data.dto.response.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface HistoryService {

    @POST("wapi/history/purchase_history.php")
    suspend fun requestPurchaseHistory(@Body request : Any) : Response<PurchaseHistoryResponse>

    @POST("wapi/history/point_history.php")
    suspend fun requestPointHistory(@Body request : Any) : Response<PointHistoryResponse>

    @POST("wapi/history/purchase_history_detail.php")
    suspend fun requestPurchaseDetail(@Body request : Any) : Response<PurchaseDetailResponse>

    @POST("wapi/history/refund_history.php")
    suspend fun requestRefundHistory(@Body request : Any) : Response<RefundHistoryResponse>

}