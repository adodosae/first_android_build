package com.hinos.rewardsample.data.dto.request

import retrofit2.http.Field

data class OverlapRequest(
        @Field("app_key")
        val app_key : String,

        @Field("user_key")
        val user_key : String,

        @Field("version")
        val version : String,

        @Field("overlap_type")
        val overlap_type : String,

        @Field("overlap_val")
        val overlap_val : String
)
