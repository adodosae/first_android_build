package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class LoginResponse(

        @Field("res_code")
        val res_code : String,

        @Field("res_msg")
        val res_msg : String,

        @Field("nickname")
        val nickname : String,

        @Field("login_key")
        val login_key : String,

        @Field("user_key")
        val user_key : String,

        @Field("point")
        val point : String,

        @Field("dormant_type") // 1. 정상, 2. 휴면계정
        val dormant_type : String,

        @Field("use_refund")
        val use_refund : String,

        @Field("ukey")
        val ukey : String
)