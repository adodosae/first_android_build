package com.hinos.rewardsample.data.dto.response

import retrofit2.http.Field

data class PhoneAuthResponse(
    @Field("res_code")
    val res_code : String,

    @Field("res_msg")
    val res_msg : String,

    @Field("valid_time")
    val valid_time : String
)
