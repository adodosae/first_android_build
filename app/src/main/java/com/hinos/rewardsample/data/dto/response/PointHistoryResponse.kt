package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class PointHistoryResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("point")
    val point : String,

    @SerializedName("list")
    val historyItems : ArrayList<Data>
) {
    @Parcelize
    data class Data(
        @SerializedName("category")
        val category : String,

        @SerializedName("product_name")
        val product_name : String,

        @SerializedName("use_point")
        val use_point : String,

        @SerializedName("point_type")
        val point_type : String,

        @SerializedName("point_date")
        val point_date : String
    ) : Parcelable
}