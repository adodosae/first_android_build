package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class ImogeRequest(
    @Field("app_key")
    val app_key : String,

    @Field("user_key")
    val user_key : String,

    @Field("android_id")
    val android_id : String,

    @Field("version")
    val version : String,

    @Field("email")
    val email : String,

    @Field("nickname")
    val nickname : String,

    @Field("buy_point")
    val buy_point : String,

    @Field("buy_choco")
    val buy_choco : String,

    @Field("buy_url")
    val buy_url : String,

    @Field("send_phone_num")
    val send_phone_num : String,

    @Field("send_type")
    val send_type : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, email : String, nickname: String, buy_point: String, buy_choco: String, buy_url: String, send_phone_num : String, send_type: String) : ImogeRequest
        {
            return ImogeRequest(
                app_key = AppConfig.APP_KEY,
                user_key = user_key,
                android_id = Common.getAndroidId(context),
                version = Common.getAppVersion(context),
                email = email,
                nickname = nickname,
                buy_point = buy_point,
                buy_choco = buy_choco,
                buy_url = buy_url,
                send_phone_num = send_phone_num,
                send_type = send_type
            )
        }
    }
}