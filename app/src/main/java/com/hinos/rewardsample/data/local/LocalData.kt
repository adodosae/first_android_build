package com.hinos.rewardsample.data.local

import android.content.Context
import com.google.gson.Gson
import javax.inject.Inject

class LocalData @Inject constructor(val mContext : Context)
{
    fun cacheUserKey(userKey : String) = AppPrefs.writeStringValue(mContext, "cacheUserKey", userKey)

    fun getCacheUserKey() : String = AppPrefs.readStringValue(mContext, "cacheUserKey", "")

    fun cacheLoginKey(userKey : String) = AppPrefs.writeStringValue(mContext, "cacheLoginKey", userKey)

    fun getCacheLoginKey() : String = AppPrefs.readStringValue(mContext, "cacheLoginKey", "")

    fun cacheEmail(email : String) = AppPrefs.writeStringValue(mContext, "cacheEmail", email)

    fun getCacheEmail() : String = AppPrefs.readStringValue(mContext, "cacheEmail", "")

    fun cacheNickname(name : String) = AppPrefs.writeStringValue(mContext, "cacheNickname", name)

    fun getCacheNickname() : String = AppPrefs.readStringValue(mContext, "cacheNickname", "")

//
//    fun cachePoint(point: String) = AppPrefs.writeStringValue(mContext, "cachePoint", point)
//
//    fun getCachePoint(): String = AppPrefs.readStringValue(mContext, "cachePoint", "")

    fun cacheRefund(refund: String) = AppPrefs.writeStringValue(mContext, "cacheRefund", refund) // 1:사용, 2:미사용

    fun getCacheRefund(): String = AppPrefs.readStringValue(mContext, "cacheRefund", "")

    fun cacheUpdateKey(uKey: String) = AppPrefs.writeStringValue(mContext, "cacheUpdateKey", uKey)

    fun getCacheUpdateKey(): String = AppPrefs.readStringValue(mContext, "cacheUpdateKey", "")


    fun cacheReviewLastPopupTime(millions: Long) = AppPrefs.writeLongValue(mContext, "LastPopupTime", millions)

    fun getCacheReviewLastPopupTime(): Long = AppPrefs.readLongValue(mContext, "LastPopupTime", 0L)




    fun cacheAppVer(value : String) = AppPrefs.writeStringValue(mContext, "cacheAppVer", value)

    fun getAppVer() : String = AppPrefs.readStringValue(mContext, "cacheAppVer", "")

    fun cacheMinVer(value : String) = AppPrefs.writeStringValue(mContext, "MinVer", value)

    fun getCacheMinVer() : String = AppPrefs.readStringValue(mContext, "MinVer", "")

    fun cachePopVer(value : String) = AppPrefs.writeStringValue(mContext, "PopVer", value)

    fun getCachePopVer() : String = AppPrefs.readStringValue(mContext, "PopVer", "")

    fun cacheMarketUrl(value : String) = AppPrefs.writeStringValue(mContext, "MarketUrl", value)

    fun getCacheMarketUrl() : String = AppPrefs.readStringValue(mContext, "MarketUrl", "")

    fun cacheAdSkip(value : String) = AppPrefs.writeStringValue(mContext, "AdSkip", value)

    fun getCacheAdSkip() : String = AppPrefs.readStringValue(mContext, "AdSkip", "")

    fun cacheInviteUrl(value : String) = AppPrefs.writeStringValue(mContext, "InviteUrl", value)

    fun getCacheInviteUrl() : String = AppPrefs.readStringValue(mContext, "InviteUrl", "")




    fun cacheKakaoHelp(value : String) = AppPrefs.writeStringValue(mContext, "KakaoHelp", value)

    fun getCacheKakaoHelp(): String = AppPrefs.readStringValue(mContext, "KakaoHelp", "")

    fun cacheRefundHelp(value: String) = AppPrefs.writeStringValue(mContext, "RefundHelp", value)

    fun getCacheRefundHelp(): String = AppPrefs.readStringValue(mContext, "RefundHelp", "")

    fun cachePrivacy(value: String) = AppPrefs.writeStringValue(mContext, "Privacy", value)

    fun getCachePrivacy(): String = AppPrefs.readStringValue(mContext, "Privacy", "")

    fun cacheTermSofService(value: String) = AppPrefs.writeStringValue(mContext, "TermSofService", value)

    fun getCacheTermSofService(): String = AppPrefs.readStringValue(mContext, "TermSofService", "")

}