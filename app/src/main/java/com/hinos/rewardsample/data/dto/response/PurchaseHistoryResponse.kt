package com.hinos.rewardsample.data.dto.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

data class PurchaseHistoryResponse(
    @SerializedName("res_code")
    val res_code : String,

    @SerializedName("res_msg")
    val res_msg : String,

    @SerializedName("list")
    val historyItems : ArrayList<Data>
) {
    @Parcelize
    data class Data(
        @SerializedName("seq")
        val seq : String,

        @SerializedName("purchase_type")
        val purchase_type : String,

        @SerializedName("product_thumb")
        val product_thumb : String,

        @SerializedName("category")
        val category : String,

        @SerializedName("goods_state")
        val goods_state : String,

        @SerializedName("product_name")
        val product_name : String,

        @SerializedName("buy_date")
        val buy_date : String
    ) : Parcelable
}
