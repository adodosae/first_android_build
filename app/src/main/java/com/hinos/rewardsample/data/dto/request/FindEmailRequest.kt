package com.hinos.rewardsample.data.dto.request

import android.content.Context
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import retrofit2.http.Field

data class FindEmailRequest(
        @Field("app_key")
        val app_key : String,

        @Field("user_key")
        val user_key : String,

        @Field("version")
        val version : String,

        @Field("android_id")
        val android_id : String,

        @Field("phone_num")
        val phone_num : String
) {
    companion object
    {
        fun toRequest(context : Context, user_key: String, phone_num: String) : FindEmailRequest
        {
            return FindEmailRequest(
                    app_key = AppConfig.APP_KEY,
                    user_key = user_key,
                    android_id = Common.getAndroidId(context),
                    version = Common.getAppVersion(context),
                    phone_num = phone_num
            )
        }
    }
}