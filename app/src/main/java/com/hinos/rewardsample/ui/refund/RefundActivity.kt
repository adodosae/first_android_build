package com.hinos.rewardsample.ui.refund

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityRefundBinding
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.refund.history.RefundHistoryActivity
import com.hinos.rewardsample.ui.views.CornerTextView
import com.hinos.rewardsample.ui.web.TermsWebActivity
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RefundActivity : BaseActivity<ActivityRefundBinding, RefundViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_refund
    override val mViewModel: RefundViewModel by viewModels()

    private val mRefundPointTv : MutableList<CornerTextView> = mutableListOf()




    override fun observeViewModel()
    {
        mViewModel.run {
//            userPointResponseLiveData.observe(this@RefundActivity, ::handleUserPointLiveData)
            selectCashPosition.observe(this@RefundActivity, ::handleSelectCashPosition)
            remainPointLiveData.observe(this@RefundActivity, ::handleRemainPointLiveData)
            refundActiveLiveData.observe(this@RefundActivity, ::handleNextActiveLiveData)
            refundResponseLiveData.observe(this@RefundActivity, ::handleRefundResponseLiveData)
            getCommonPointLiveData().observe(this@RefundActivity, ::handleUserPointLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            tvRefundHistory.setOnClickListener { openActivity(RefundHistoryActivity::class.java) }
            ivBack.setOnClickListener { closeActivity() }
            tvNext.setOnClickListener { openBottomSheet(BottomSheet.REQUEST_REFUND_POINT) }

            etBank.addTextChangedListener {
                tvBankHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidBank(it.toString())) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }

            etAccount.addTextChangedListener {
                tvAccountHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidAccount(it.toString())) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }

            etAccountHolder.addTextChangedListener {
                tvAccountHolderHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidAccountName(it.toString())) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }

            ivHelp.setOnClickListener {
                openTermsActivity(getString(R.string.request_refund_help), mViewModel.refundHelpUrlLiveData.value ?: "")
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initVewSetting()
        mViewModel.fetchUserPoint()
    }

    private fun initVewSetting()
    {
        mBinding.run {
            tvWarningContents.text = Common.toListDotText(this@RefundActivity, String.format(getString(R.string.warning_refund_contents_text), getString(R.string.app_name)))
            mRefundPointTv.run {
                add(tvPoint1)
                add(tvPoint2)
                add(tvPoint3)
                add(tvPoint4)

                forEachIndexed { index, cornerTextView ->
                    cornerTextView.text = getCashTxt(mViewModel.mRefundPoint[index])
                    cornerTextView.setOnClickListener(mClickListener)
                }
            }
        }

    }
    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.run {
            setOnDialogResult(object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    dismiss()
                    if (nResult == BaseDialog.RES_OK)
                    {
                        mBinding.run {
                            mViewModel.requestRefund(
                                bank_name = etBank.text.toString(),
                                account_num = etAccount.text.toString(),
                                account_name = etAccountHolder.text.toString()
                            )
                        }
                    }
                }
            })
            showDialog(sheet, params)
        }
    }

    private val mClickListener : View.OnClickListener = View.OnClickListener {
        mRefundPointTv.forEachIndexed { index, cornerTextView ->
            if (it == cornerTextView)
            {
                cornerTextView.setTextColor(Color.WHITE)
                cornerTextView.setBackgroundColor(Color.parseColor("#4582f9"))
                cornerTextView.setStrokeColor(Color.parseColor("#4582f9"))
                selectCash(index)
            } else {
                cornerTextView.setBackgroundColor(Color.TRANSPARENT)
                cornerTextView.setTextColor(Color.parseColor("#ced4da"))
                cornerTextView.setStrokeColor(Color.parseColor("#ced4da"))
            }
            mBinding.tvHyphen.visibility = View.INVISIBLE
        }
    }

    private fun openTermsActivity(title : String, url : String)
    {
        val i = Intent(this, TermsWebActivity::class.java).apply {
            putExtra(TermsWebActivity.EXTRA_TERMS_TITLE, title)
            putExtra(TermsWebActivity.EXTRA_TERMS_URL, url)
        }
        startActivity(i)
    }

    private fun selectCash(position : Int)
    {
        mViewModel.selectCash(position)
    }

    private fun checkNextBtnState()
    {
        mBinding.run {
            mViewModel.isWrapValue(etBank.text.toString(), etAccount.text.toString(), etAccountHolder.text.toString())
        }
    }

    private fun getCashTxt(cash : Int) : String
    {
        return (cash / 10000).toString() + getString(R.string.cash_text)
    }

    private fun handleUserPointLiveData(userPoint : Int)
    {
        mBinding.run {
            tvPoint.text = Common.toPointFormat(userPoint)
        }
    }

    private fun handleSelectCashPosition(selectPos : Int)
    {
        mBinding.run {
            val point = if(selectPos == -1) 0 else mViewModel.mRefundPoint[selectPos]
            tvRefundPoint.text = Common.toPointFormat(point)
        }
    }

    private fun handleRemainPointLiveData(remainPoint : Int)
    {
        mBinding.run {
            tvRemainPoint.text = Common.toPointFormat(remainPoint)
            checkNextBtnState()
        }
    }

    private fun handleNextActiveLiveData(isActive : Boolean)
    {
        mBinding.tvNext.run {
            isClickable = isActive
            setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
        }
    }

    private fun handleRefundResponseLiveData(status : Resource<PurchaseResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    showToast(getString(R.string.refund_completed_text))
                    closeActivity()
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }
}