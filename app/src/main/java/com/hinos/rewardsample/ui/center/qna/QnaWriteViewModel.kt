package com.hinos.rewardsample.ui.center.qna

import android.app.Application
import android.os.Build
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.ui.center.adapter.GalleryAdapter
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import javax.inject.Inject


@HiltViewModel
class QnaWriteViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val typeLiveDataPrivate = MutableLiveData(0)
    val typeLiveData: LiveData<Int> get() = typeLiveDataPrivate

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  nextBtnPrivate = MutableLiveData<Boolean>(false)
    val nextBtnState : LiveData<Boolean> get() = nextBtnPrivate


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateQnaWriteResponse = MutableLiveData<Resource<ResCode>>()
    val qnaWriteResponse : LiveData<Resource<ResCode>> get() = privateQnaWriteResponse


    fun requestWriteInquiry(title : String, contents : String, galleryList : GalleryAdapter.GalleryArray)
    {
        val boardType = Common.getBoardType(typeLiveDataPrivate.value ?: 0)

        if (title.isEmpty())
            return

        if (boardType.isEmpty())
            return

        if (contents.isEmpty())
            return

        privateQnaWriteResponse.value = Resource.Loading()

        viewModelScope.launch {
            val params = HashMap<String, RequestBody>().apply {
                put("app_key", Common.getRequestParam(AppConfig.APP_KEY))
                put("user_key", Common.getRequestParam(mDataRepository.getCacheUserKey()))
                put("version", Common.getRequestParam(Common.getAppVersion(app)))
                put("android_id", Common.getRequestParam(Common.getAndroidId(app)))
                put("bd_name", Common.getRequestParam(boardType))
                put("lang", Common.getRequestParam(Common.getLanguageCode(app)))
                put("country", Common.getRequestParam(Common.getCountryCode(app)))
                put("os_ver", Common.getRequestParam(Build.VERSION.SDK_INT.toString()))
                put("model", Common.getRequestParam(Common.getPhoneModel()))
                put("man", Common.getRequestParam(Build.MANUFACTURER))
                put("email", Common.getRequestParam(mDataRepository.getCacheEmail()))
                put("title", Common.getRequestParam(title))
                put("contents", Common.getRequestParam(contents))
            }




            var bSizeLimitFail = false

            var img_url_1 : MultipartBody.Part? = null
            var img_url_2 : MultipartBody.Part? = null
            var img_url_3 : MultipartBody.Part? = null

            for (vo in galleryList)
            {
                if (vo.bTemp) continue
                val bitmap = Common.getBitmapFromStorage(app, vo.strUri) ?: continue
                val resizedBitmap = Common.resizeXwBitmap(bitmap, 1920)
                resizedBitmap ?: continue

                val f = Common.getBitmapToFile(app, resizedBitmap, Common.getRandomString(10))
                if (Common.isLimitSize(f, 4))
                {
                    if (f == null)
                    {
//                        Log.e(TAG, "uploadPicture Error")
                    }
                    else
                    {
                        val body = f.asRequestBody("image/*".toMediaTypeOrNull())
                        if (img_url_1 == null)
                            img_url_1 = MultipartBody.Part.createFormData("img_url_1", f.name, body)
                         else if(img_url_2 == null)
                            img_url_2 = MultipartBody.Part.createFormData("img_url_2", f.name, body)
                        else if(img_url_3 == null)
                            img_url_3 = MultipartBody.Part.createFormData("img_url_3", f.name, body)
                    }
                }
                else
                {
                    bSizeLimitFail = true
                }
            }

            if (bSizeLimitFail)
            {
                privateQnaWriteResponse.value = Resource.DataError(ErrorCase.QNA_FILE_SIZE_LIMIT)
                return@launch
            }
//
//            val retrofit = Retrofit.Builder()
//                .baseUrl(AppConfig.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create(Gson()))
//                .build()
//
//
//            val retrofitService: BoardService = retrofit.create(BoardService::class.java)
//            retrofitService.requestWriteInquiry2(
//                params["app_key"],
//                params["user_key"],
//                params["version"],
//                params["android_id"],
//                params["bd_name"],
//                params["lang"],
//                params["country"],
//                params["os_ver"],
//                params["model"],
//                params["man"],
//                params["email"],
//                params["title"],
//                params["contents"],
//                img_url_1,
//                img_url_2,
//                img_url_3
//            ).enqueue(object : Callback<ResCode> {
//                override fun onResponse(call: Call<ResCode>, response: Response<ResCode>)
//                {
//                    val s = response
//                }
//
//                override fun onFailure(call: Call<ResCode>, t: Throwable)
//                {
//                    val s = call
//                    val s2 = t
//                }
//            })


            mDataRepository.requestWriteInquiry(
                params,
                img_url_1,
                img_url_2,
                img_url_3
            ).collect {
                Common.deleteChildFileFromDir(Common.getGalleryFileDir(app))
                privateQnaWriteResponse.value = it
            }
        }





//        @PartMap Map<String, RequestBody> params


    }


    fun selectInquiryType(type : Int)
    {
        if (Common.mHashInquiry.containsKey(type))
        {
            typeLiveDataPrivate.value = type
        }
    }

    fun typeToString(type : Int) : String
    {
        return Common.mHashInquiry[type] ?: ""
    }


    fun isWrapValue(title : String, contents : String)
    {
        nextBtnPrivate.value = isValidValue(Common.getBoardType(typeLiveDataPrivate.value ?: 0), title, contents) == 0
    }

    fun isValidValue(bd_name : String, title : String, contents : String) : Int
    {

        if (title.isEmpty())
            return -1

        if (bd_name.isEmpty())
            return -1

        if (contents.isEmpty())
            return -1
        return 0
    }
}
