package com.hinos.rewardsample.ui.purchase.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.databinding.ItemPurchaseHistoryBinding
import com.hinos.rewardsample.ui.purchase.history.PurchaseHistoryViewModel

class PurchaseHistoryAdapter(private val purchaseHistoryViewModel : PurchaseHistoryViewModel) : RecyclerView.Adapter<PurchaseHistoryAdapter.PurchaseHistoryHolder>()
{
    private var mItems : MutableList<PurchaseHistoryResponse.Data> = mutableListOf()

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            purchaseHistoryViewModel.openProductHistoryDetail(item as PurchaseHistoryResponse.Data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PurchaseHistoryHolder {
        val itemBinding = ItemPurchaseHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PurchaseHistoryHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: PurchaseHistoryHolder, position: Int) {
        holder.bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int = mItems.size

    fun setNewData(newItems : List<PurchaseHistoryResponse.Data>)
    {
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

    class PurchaseHistoryHolder(private val itemBinding : ItemPurchaseHistoryBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : PurchaseHistoryResponse.Data, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                root.setOnClickListener { recyclerItemListener.onItemSelected(item) }
                tvTitle.text = item.product_name
                tvSubTitle.text = item.category
                tvDate.text = item.buy_date.split(" ")[0].replace("-",".")
                if (item.goods_state == "1")
                    tvState.visibility = View.VISIBLE
                 else
                    tvState.visibility = View.INVISIBLE

                Glide.with(ivIcon).load(item.product_thumb).centerCrop().into(ivIcon) // TODO
            }
        }
    }
}