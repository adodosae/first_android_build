package com.hinos.rewardsample.ui.center.qna

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.data.dto.response.QnaListResponse
import com.hinos.rewardsample.databinding.ActivityZoomBinding
import com.hinos.rewardsample.ui.fragment.ZoomFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ZoomActivity : AppCompatActivity()
{
    companion object
    {
        const val EXTRA_QNA_VO = "EXTRA_QNA_VO"
        const val EXTRA_QNA_POSITION = "EXTRA_QNA_POSITION"

        fun openActivity(act : Activity, qVo : QnaListResponse.Data?, position: Int)
        {
            val i = Intent(act, ZoomActivity::class.java)
            i.putExtra(EXTRA_QNA_VO, qVo)
            i.putExtra(EXTRA_QNA_POSITION, position)

            act.startActivity(i)
        }
    }

    private var m_qVo : QnaListResponse.Data? = null
    private var m_nPosition : Int = 0
    private val m_arrUrl : MutableList<String> = mutableListOf()
    private val m_arrPhotoFragment : MutableList<ZoomFragment> = mutableListOf()
    private var m_adtPager : ZoomPagerAdapter? = null

    private lateinit var m_binding : ActivityZoomBinding


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        m_binding = DataBindingUtil.setContentView(this, R.layout.activity_zoom)
        m_qVo = intent.getParcelableExtra(EXTRA_QNA_VO)
        m_nPosition = intent.getIntExtra(EXTRA_QNA_POSITION, 0)
        m_qVo ?: closeActivity()

        m_arrPhotoFragment.clear()
        m_qVo?.run {
            if (!img_url1.isNullOrEmpty())
            {
                m_arrUrl.add(img_url1)
                m_arrPhotoFragment.add(ZoomFragment.newInstance(img_url1))
            }
            if (!img_url2.isNullOrEmpty())
            {
                m_arrUrl.add(img_url2)
                m_arrPhotoFragment.add(ZoomFragment.newInstance(img_url2))
            }
            if (!img_url3.isNullOrEmpty())
            {
                m_arrUrl.add(img_url3)
                m_arrPhotoFragment.add(ZoomFragment.newInstance(img_url3))
            }
        }
        initViewSetting()
    }

    private fun initViewSetting()
    {
        m_binding.run {
            vLlBack.setOnClickListener(m_hOnClickListener)
            vTvTitle.text = m_qVo?.title
            m_adtPager = ZoomPagerAdapter(this@ZoomActivity.supportFragmentManager)
            vVpPager.offscreenPageLimit = 3
            vVpPager.adapter = m_adtPager
            vVpPager.addOnPageChangeListener(m_hOnViewPagerChangeListener)
            vVpPager.setCurrentItem(m_nPosition, false)
            vTvPhotoGuide.text = "${m_nPosition+1}/${m_arrPhotoFragment.size}"
        }
    }

    private fun showAppBar()
    {
        m_binding.let {
            val visible = it.vLlActionBar.visibility
            if(visible == View.GONE)
            {
                it.vLlActionBar.visibility = View.VISIBLE
            } else if (visible == View.VISIBLE)
            {
                it.vLlActionBar.visibility = View.GONE
            }
        }
    }

    private fun closeActivity()
    {
        finish()
    }

    private val m_hOnClickListener = View.OnClickListener {
        when(it.id)
        {
            R.id.v_llBack ->
            {
                closeActivity()
            }
        }
    }

    private val m_hOnViewPagerChangeListener = object : ViewPager.OnPageChangeListener
    {
        override fun onPageScrollStateChanged(state: Int)
        {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int)
        {

        }

        override fun onPageSelected(position: Int)
        {
            m_nPosition = position
            m_binding.vTvPhotoGuide.text = "${m_nPosition+1}/${m_arrPhotoFragment.size}"
        }
    }

    inner class ZoomPagerAdapter(fm : FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
    {
        override fun getItem(position: Int): Fragment
        {
            return m_arrPhotoFragment[position]
        }

        override fun getCount(): Int
        {
            return m_arrPhotoFragment.size
        }
    }
}