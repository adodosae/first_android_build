package com.hinos.rewardsample.ui.center.qna.history

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.response.NoticeListResponse
import com.hinos.rewardsample.data.dto.response.QnaListResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class QnaHistoryViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    companion object
    {
        const val EXTRA_QNA_DETAIL : String = "EXTRA_QNA_DETAIL"
    }

    val clickEvtQnaDetail: SingleLiveEvent<QnaListResponse.Data> = SingleLiveEvent()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateQnaListLiveData = MutableLiveData<Resource<QnaListResponse>>()
    val qnaListLiveData : LiveData<Resource<QnaListResponse>> get() = privateQnaListLiveData

    fun requestQnaHistory()
    {
        viewModelScope.launch {
            val request = BaseRequest.toRequest(
                app,
                mDataRepository.getCacheUserKey()
            )
            mDataRepository.requestQnaHistory(request).collect {
                privateQnaListLiveData.value = it
            }
        }
    }

    fun openQnaDetail(data : QnaListResponse.Data)
    {
        viewModelScope.launch {
            clickEvtQnaDetail.value = data
        }
    }
}