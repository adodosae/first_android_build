package com.hinos.rewardsample.ui.splash

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.firebase.FirebaseApp
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.AppSettingResponse
import com.hinos.rewardsample.data.dto.response.LoginResponse
import com.hinos.rewardsample.data.dto.response.SplashResponse
import com.hinos.rewardsample.data.local.AdPrefs
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivitySplashBinding
import com.hinos.rewardsample.ui.center.qna.history.QnaHistoryActivity
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.dialog.DialogSheet
import com.hinos.rewardsample.ui.login.LoginActivity
import com.hinos.rewardsample.ui.main.MainActivity
import com.hinos.rewardsample.util.*
import com.rdev.adfactory.AdsMobile
import com.rdev.adfactory.etc.EnumAdsState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>()
{
    companion object
    {
        private const val REQUEST_PERMISSION = 101
        private const val REQUEST_NEED_ADS_PERMISSION_CODE = 1255
    }


    override val mLayoutResID: Int = R.layout.activity_splash
    override val mViewModel: SplashViewModel by viewModels()


    override fun initListener()
    {

    }

    override fun observeViewModel()
    {
        mViewModel.run {
            appSettingResponseLiveData.observe(this@SplashActivity, ::handleSettingResult)
            splashResponseLiveData.observe(this@SplashActivity, ::handleSplashResult)
            loginLiveData.observe(this@SplashActivity, ::handleLoginResult)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) !=
            PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) !=
            PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT <= 29)
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE), REQUEST_PERMISSION)
             else
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_NUMBERS), REQUEST_PERMISSION)

            return
        }

        initApp()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode)
        {
            REQUEST_PERMISSION ->
            {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    initApp()
                } else {
                    showToast(getString(R.string.must_has_permission_message))
                    closeActivity()
                }
            }

            REQUEST_NEED_ADS_PERMISSION_CODE ->
            {
                var agreeSize = 0
                grantResults.forEach {
                    if (it == PackageManager.PERMISSION_GRANTED)
                        agreeSize++
                }

                if (agreeSize == grantResults.size)
                {
                    AdPrefs.writeDenyUnityPermissionCount(this, 0)
                } else {
                    AdPrefs.writeDenyUnityPermissionCount(this, AdPrefs.readDenyUnityPermissionCount(this) + 1)
                    showToast(getString(R.string.must_has_permission_message))
                    closeActivity()
                }

                requestAppSetting()
            }
        }
    }

    private fun initApp()
    {
        EmulatorDetector.with(this).detect { isEmulator ->
            runOnUiThread {
                if (isEmulator || !Common.isProperUser(this)) {
                    showToast(getString(R.string.project_app_message))
                    closeActivity()
                    return@runOnUiThread
                }

                Common.makeGalleryDir(this)
                FirebaseApp.initializeApp(this)
                ComJob.checkJobSchedule(this)
                initAds()
            }
        }

//        val mJobService = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
//        mJobService.cancelAll()
//        mJobService.schedule(ComJob.getJobInfo(applicationContext))
    }



    private fun requestAppSetting()
    {
        mViewModel.requestAppSetting()
    }

    private fun initAds()
    {
        AdsMobile.initialized(
            AdsMobile.SettingBuilder(this) // TODO
                .addHomeUrl(AppConfig.URL_XW_ADS)
                .addAppKey("TEST2")
                .addUserKey(AppConfig.ADS_TEMP_USER_KEY)
                .addGoogleAdsId(AppConfig.ADS_TEMP_USER_KEY)
                .addAppVersion(Common.getAppVersion(this).toInt())
                .addAdLibVersion(1)
                .addFilterUnity(AdPrefs.readDenyUnityPermissionCount(this) >= 2)
                .setInitializedCallback(object : AdsMobile.OnAdsMobileInitialized {
                    override fun onAdsMobileAdsInitializedListener(adsState: EnumAdsState, needPermission: Set<String>?)
                    {
                        if (adsState == EnumAdsState.SUCCESS) {
                            val requestPermissions = mutableListOf<String>()

                            needPermission?.forEach { need ->
                                if (ContextCompat.checkSelfPermission(this@SplashActivity, need) != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions.add(need)
                                }
                            }

                            if (!requestPermissions.isNullOrEmpty()) {
                                requestPermission(requestPermissions.toTypedArray())
                                return
                            }
                        }
                        requestAppSetting()
                    }
                }))
        mViewModel.startTimer(2 * 1000)
    }

    private fun requestPermission(requestPermissions: Array<String>)
    {
        ActivityCompat.requestPermissions(this, requestPermissions, REQUEST_NEED_ADS_PERMISSION_CODE)
    }

    private fun handleSplashResult(status : Resource<SplashResponse>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { _ ->
//                    if (!response.email.isNullOrEmpty())
//                    {
//                        openBottomSheet(response.email)
//                    } else {
//                        openActivity(LoginActivity::class.java)
//                        closeActivity()
//                    }

                    openActivity(Intent(this@SplashActivity, LoginActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
                    closeActivity()
                }
                is Resource.DataError ->
                {
                    openActivity(Intent(this@SplashActivity, LoginActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
                    closeActivity()
                }
            }
        }
    }

    private fun handleSettingResult(status : Resource<AppSettingResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { response ->
                    if(Common.getAppVersion(this@SplashActivity) < response.min_ver)
                    {
                        loaderView.visibility = View.GONE
                        // 강제 업데이트 ㄱㄱ

                        openGeneralDialog(DialogSheet.DIALOG_MUST_UPDATE, object : BaseDialog.OnDialogResult
                        {
                            override fun onDialogResult(nResult: Int, data: Any)
                            {
                                mDialog.dismiss()
                                if (nResult == BaseDialog.RES_CANCEL)
                                {
                                    closeActivity()
                                } else if (nResult == BaseDialog.RES_OK) {
                                    Common.openUrl(this@SplashActivity, response.market_url)
                                    closeActivity()
                                }
                            }
                        })

                        return@run
                    }

                    if (Common.getAppVersion(this@SplashActivity) < response.app_ver)
                    {
                        loaderView.visibility = View.GONE
                        // 권유 업데이트 ㄱㄱ
                        openGeneralDialog(DialogSheet.DIALOG_SHOULD_UPDATE, object : BaseDialog.OnDialogResult
                        {
                            override fun onDialogResult(nResult: Int, data: Any)
                            {
                                mDialog.dismiss()
                                if (nResult == BaseDialog.RES_CANCEL)
                                {
                                    mViewModel.requestSplash()
                                } else if (nResult == BaseDialog.RES_OK) {
                                    Common.openUrl(this@SplashActivity, response.market_url)
                                }
                            }
                        })
                        return@run
                    }
                    mViewModel.requestSplash()
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleLoginResult(status : Resource<LoginResponse>) // 자동로그인
    {
        mBinding.run {
            when(status) {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    if (it.dormant_type == "1") { // 정상
                        openActivity(Intent(this@SplashActivity, MainActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
                    } else {
                        openActivity(Intent(this@SplashActivity, LoginActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
                    }
                    closeActivity()
                }
                is Resource.DataError ->
                {
                    val errorCode = status.errorCode ?: 0
                    loaderView.visibility = View.GONE
                    if (errorCode == ErrorCase.SERVER_ERROR_ID_AND_PASSWORD_INCORRECT || errorCode == ErrorCase.SERVER_ERROR_EXIST_PHONE_NUMBER)
                    {
                        status.errorCode?.let { showToast(it)}
                    } else {
                        openActivity(Intent(this@SplashActivity, LoginActivity::class.java).apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) })
                    }
                    closeActivity()
                }
            }
        }

//        showToast(status.toString())
    }
    
//    private fun openBottomSheet(email : String)
//    {
//        mSheet.setOnDialogResult(object : BaseDialog.OnDialogResult
//        {
//            override fun onDialogResult(nResult: Int, data: Any)
//            {
//                mSheet.dismiss()
//                val i = Intent(this@SplashActivity, LoginActivity::class.java)
//                if (nResult == BaseDialog.RES_OK)
//                {
//                    i.putExtra(LoginActivity.EXTRA_EMAIL, email)
//                }
//                openActivity(i)
//                closeActivity()
//            }
//        })
//
////        mSheet.showDialog(BottomSheet.AUTO_LOGIN, HashMap<String, Any>().apply { put("email", email) })
//    }

    private fun openGeneralDialog(sheet: DialogSheet, callback : BaseDialog.OnDialogResult)
    {
        mDialog.run {
            if (isShowing)
            {
                dismiss()
                return
            }

            setOnDialogResult(callback)
            showDialog(sheet)
        }
    }

    private fun navigateToMainScreen()
    {
        CoroutineScope(Dispatchers.IO).launch {

        }
        Handler(Looper.getMainLooper()).postDelayed({
            val nextActivity = Intent(this, LoginActivity::class.java)
            openActivity(nextActivity)
            closeActivity()
        }, 1000)
    }
}