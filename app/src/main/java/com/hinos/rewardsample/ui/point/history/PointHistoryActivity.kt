package com.hinos.rewardsample.ui.point.history

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.PointHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityPointHistoryBinding
import com.hinos.rewardsample.ui.point.adapter.PointHistoryAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PointHistoryActivity : BaseActivity<ActivityPointHistoryBinding, PointHistoryViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_point_history
    override val mViewModel: PointHistoryViewModel by viewModels()

    private val mAdtPointHistory : PointHistoryAdapter by lazy { PointHistoryAdapter(mViewModel) }

    override fun observeViewModel()
    {
        mViewModel.run {
            pointHistoryLiveData.observe(this@PointHistoryActivity, ::handlePointHistoryLiveData)
            getCommonPointLiveData().observe(this@PointHistoryActivity) {
                mAdtPointHistory.setNewData(it.toString())
            }
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    private fun initRecyclerView()
    {
        mBinding.recyclerView.run {
            adapter = mAdtPointHistory
            layoutManager = LinearLayoutManager(this@PointHistoryActivity)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        mViewModel.fetchUserPoint()
        initRecyclerView()
    }

    private fun handlePointHistoryLiveData(status : Resource<PointHistoryResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    mAdtPointHistory.setNewData(it.point, it.historyItems)
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }
}