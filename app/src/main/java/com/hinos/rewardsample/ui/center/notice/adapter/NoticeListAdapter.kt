package com.hinos.rewardsample.ui.center.notice.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.response.NoticeListResponse

import com.hinos.rewardsample.databinding.ItemNoticeListBinding
import com.hinos.rewardsample.ui.center.notice.NoticeViewModel

class NoticeListAdapter(private val noticeViewModel: NoticeViewModel) : RecyclerView.Adapter<NoticeListAdapter.NoticeListViewHolder>()
{
    private var mItems : MutableList<NoticeListResponse.Data> = mutableListOf()

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            noticeViewModel.openNoticeDetail(item as NoticeListResponse.Data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoticeListViewHolder
    {
        val itemBinding = ItemNoticeListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NoticeListViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: NoticeListViewHolder, position: Int)
    {
        holder.bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int
    {
        return mItems.size
    }

    fun setNewData(newItems : List<NoticeListResponse.Data>)
    {
        mItems.clear()
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class NoticeListViewHolder(private val itemBinding : ItemNoticeListBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : NoticeListResponse.Data, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                root.setOnClickListener { recyclerItemListener.onItemSelected(item) }
                tvTitle.text = item.title
                tvDate.text = item.create_date
            }
        }
    }
}