package com.hinos.rewardsample.ui.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import androidx.appcompat.widget.AppCompatTextView
import com.hinos.rewardsample.R

class CornerTextView : AppCompatTextView
{
    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    ) {
        initView(attrs)
    }

    @Dimension
    var mCornerRadius : Float = 0f
    @Dimension
    var mStrokeWidth : Int = 0
    @ColorInt
    var mStrokeColor : Int = Color.parseColor("#00ffffff")

    private fun initView(attrs: AttributeSet?) {
        setAttrShape(attrs)
    }

    private fun setAttrShape(attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.HinosButton)

        val shape = background?.let {
            return@let if (it is ColorDrawable)
            {
                GradientDrawable().apply {
                    setColor(it.color)
                }
            }
            else {
                GradientDrawable()
            }
        } ?: run {
            GradientDrawable(
                    GradientDrawable.Orientation.TOP_BOTTOM,
                    getGradientColors(typedArray)
            )
        }

        mStrokeWidth = typedArray.getDimension(R.styleable.HinosButton_strokeWidth, 0f).toInt()
        mStrokeColor = typedArray.getColor(R.styleable.HinosButton_strokeColor, Color.parseColor("#00ffffff"))
        mCornerRadius = typedArray.getDimension(R.styleable.HinosButton_cornerRadius, 0f)
        shape.setStroke(mStrokeWidth, mStrokeColor)
        shape.cornerRadius = mCornerRadius
        background = shape
    }

    private fun getGradientColors(typedArray: TypedArray): IntArray {
        @ColorInt val gradientTopColor = typedArray.getColor(
                R.styleable.HinosButton_gradientTopColor,
                Color.parseColor("#00ffffff")
        )
        @ColorInt val gradientMiddleColor = typedArray.getColor(
                R.styleable.HinosButton_gradientMiddleColor,
                Color.parseColor("#00ffffff")
        )
        @ColorInt val gradientBottomColor = typedArray.getColor(
                R.styleable.HinosButton_gradientBottomColor,
                Color.parseColor("#00ffffff")
        )
        return intArrayOf(
                gradientTopColor, gradientMiddleColor, gradientBottomColor
        )
    }


    override fun setBackgroundColor(color: Int) {
        if (background is GradientDrawable)
        {
            (background as GradientDrawable).setColor(color)
        }
    }

    fun setStrokeColor(@ColorInt parseColor: Int)
    {
        if (background is GradientDrawable)
        {
            val background = background as GradientDrawable
            background.setStroke(mStrokeWidth, parseColor)
            this.invalidateDrawable(background)
        }
    }

    fun setGradientBackgroundColor(@ColorInt topColor : Int, @ColorInt middleColor : Int, @ColorInt bottomColor : Int)
    {
        val gradientDrawable = GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                intArrayOf(topColor, middleColor, bottomColor)
        )

        gradientDrawable.setStroke(mStrokeWidth, mStrokeColor)
        gradientDrawable.cornerRadius = mCornerRadius

        background = gradientDrawable
    }
}