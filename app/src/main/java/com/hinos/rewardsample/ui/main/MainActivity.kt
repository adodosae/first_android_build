package com.hinos.rewardsample.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.ui.center.CenterActivity
import com.hinos.rewardsample.ui.center.notice.NoticeListActivity
import com.hinos.rewardsample.ui.dialog.DialogSheet
import com.hinos.rewardsample.ui.imoge.ImogeActivity
import com.hinos.rewardsample.ui.offerwall.OfferWallActivity
import com.hinos.rewardsample.ui.point.history.PointHistoryActivity
import com.hinos.rewardsample.ui.purchase.history.PurchaseHistoryActivity
import com.hinos.rewardsample.ui.refund.RefundActivity
import com.hinos.rewardsample.ui.store.StoreActivity
import com.hinos.rewardsample.ui.user_info.UserInfoActivity
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import com.rdev.adfactory.AdsMgr
import com.rdev.adfactory.AdsMobile
import com.rdev.adfactory.Builder
import com.rdev.adfactory.listener.BannerAdsListener
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainWrapperBinding, MainViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_main_wrapper
    override val mViewModel: MainViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            backPressFlagLiveData.observe(this@MainActivity, ::handleOpenDialogFlag)
//            userPointResponseLiveData.observe(this@MainActivity, ::handleUserPoint)
            userNameLiveData.observe(this@MainActivity, ::handleUserName)
            getCommonPointLiveData().observe(this@MainActivity) {
                mBinding.layoutMain.run {
                    tvPoint.text = Common.toPointFormat(it)
                }
            }

            refundUseLiveData.observe(this@MainActivity, ::handleRefundUseLiveData)
        }
    }

    override fun initListener() {
        mBinding.layoutMain.run {
            llPoint.setOnClickListener {
                openActivity(OfferWallActivity::class.java)
            }
            llCenter.setOnClickListener {
                openActivity(CenterActivity::class.java)
            }
            llFAQ.setOnClickListener {
                openNoticeListActivity(NoticeListActivity.LIST_FAQ_TYPE)
            }
            llNotice.setOnClickListener {
                openNoticeListActivity(NoticeListActivity.LIST_NOTICE_TYPE)
            }
            ivDrawer.setOnClickListener {
                openDrawer()
            }
            llRefund.setOnClickListener {
                openActivity(RefundActivity::class.java)
            }
            llStore.setOnClickListener {
                openActivity(StoreActivity::class.java)
            }
            llImoge.setOnClickListener {
                openActivity(ImogeActivity::class.java)
            }
            ivPoint.setOnClickListener {
                openActivity(PointHistoryActivity::class.java)
            }
            tvInvite.setOnClickListener {
                val inviteUrl = mViewModel.inviteUrlLiveData.value
                inviteUrl ?: return@setOnClickListener
                Common.openSystemSharedActivity(this@MainActivity, inviteUrl, getString(R.string.app_name), getString(R.string.kakao_msg))
//                Common.openUrl(this@MainActivity, mViewModel.inviteUrlLiveData.value!!)
            }
        }

        mBinding.run {
            tvUserInfo.setOnClickListener {
                openActivity(UserInfoActivity::class.java)
                closeDrawer()
            }
            tvPurchaseHistory.setOnClickListener {
                openActivity(PurchaseHistoryActivity::class.java)
                closeDrawer()
            }
            tvKakao.setOnClickListener {
                if (!Common.recommendKAKAO(this@MainActivity))
                    showToast(this@MainActivity, getString(R.string.kakaotalk_not_install))
            }
            tvReview.setOnClickListener {
                Common.openMarketUrl(this@MainActivity)
            }
            tvServerSetting.setOnClickListener {  }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
//        FirebaseMessaging.getInstance().subscribeToTopic(AppConfig.APP_KEY)
        mViewModel.fetchUserPoint()
        loadBanner()
    }

    override fun onResume()
    {
        super.onResume()
    }

    override fun onBackPressed()
    {
        if (closeDrawer())
            return

        mViewModel.onBackPress()
    }

    private var m_mgrAdsBanner : AdsMgr? = null
    private fun loadBanner()
    {
        val builder = Builder(this)
            .addAdsBannerListener(object : BannerAdsListener
            {
                override fun onBannerFailToLoad(errorMessage: String)
                {
                    println("onBannerFailToLoad")
                }

                override fun onBannerLoaded()
                {
                    println("onBannerLoaded")
                }
            })
        if (AppConfig.DEBUG)
            builder.addTestDevice(AppConfig.TESTKEY)

        m_mgrAdsBanner = AdsMobile.getAdsMgr(builder)?.apply {
            loadBanner(mBinding.layoutMain.llBanner, AdsMobile.getMainBannerData())
        }
    }

    private fun openDrawer()
    {
        mBinding.run {
            if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }
    private fun handleUserPoint(point : String)
    {
        mBinding.layoutMain.run {
            tvPoint.text = Common.toPointFormat(point)
        }
    }
    private fun handleUserName(nickname : String)
    {
        mBinding.layoutMain.run {
            tvNickname.text = nickname
        }
    }
    private fun handleRefundUseLiveData(refundUse : String)
    {
        mBinding.layoutMain.run {
            llRefund.visibility = if (refundUse == "1") View.VISIBLE else View.GONE
        }
    }
    private fun handleOpenDialogFlag(flag : Int)
    {
        when(flag)
        {
            MainViewModel.BACK_FLAG_REVIEW ->
            {
                openGeneralDialog(DialogSheet.DIALOG_REVIEW, object : BaseDialog.OnDialogResult
                {
                    override fun onDialogResult(nResult: Int, data: Any)
                    {
                        mDialog.dismiss()
                        if (nResult == BaseDialog.RES_OK) {
                            closeActivity()
                        } else if (nResult == BaseDialog.RES_REVIEW) {
                            Common.openMarketUrl(this@MainActivity)
                            mViewModel.cacheReviewLastPopupTime(-1)
                        }
                    }
                })
                mViewModel.cacheReviewLastPopupTime(System.currentTimeMillis())
            }

            MainViewModel.BACK_FLAG_EXIT -> openGeneralDialog(DialogSheet.DIALOG_EXIT, object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    mDialog.dismiss()
                    if (nResult == BaseDialog.RES_OK) {
                        closeActivity()
                    } else if (nResult == BaseDialog.RES_REVIEW) {

                    }
                }
            })
        }
    }

    private fun openGeneralDialog(sheet: DialogSheet, callback : BaseDialog.OnDialogResult)
    {
        mDialog.run {
            if (isShowing)
            {
                dismiss()
                return
            }

            setOnDialogResult(callback)
            showDialog(sheet)
        }
    }

    private fun closeDrawer() : Boolean
    {
        mBinding.run {
            if (drawerLayout.isDrawerOpen(GravityCompat.START))
            {
                drawerLayout.closeDrawer(GravityCompat.START)
                return true
            }
            return false
        }
    }

    private fun openNoticeListActivity(noticeType : Int)
    {
        val i = Intent(this, NoticeListActivity::class.java).apply {
            putExtra(NoticeListActivity.EXTRA_LIST_TYPE, noticeType)
        }
        startActivity(i)
    }
}