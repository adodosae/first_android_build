package com.hinos.rewardsample.ui.imoge

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.RadioButton
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityImogeBinding
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.purchase.history.PurchaseHistoryActivity
import com.hinos.rewardsample.ui.web.TermsWebActivity
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImogeActivity : BaseActivity<ActivityImogeBinding, ImogeViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_imoge
    override val mViewModel: ImogeViewModel by viewModels()

    private val mRadioBtnList = ArrayList<RadioButton>()

    private val mImogePointList : List<ImogeItem> = arrayListOf(
        ImogeItem("1100", "100"),
        ImogeItem("2200", "200"),
        ImogeItem("3300", "300"),
        ImogeItem("4400", "400")
    )

    override fun observeViewModel()
    {
        mViewModel.run {
            userPointResponseLiveData.observe(this@ImogeActivity, ::handleUserPointResponseLiveData)
            purchaseBtnFlag.observe(this@ImogeActivity, ::handlePurchaseBtnFlag)
            purchaseResponseLiveData.observe(this@ImogeActivity, ::handlePurchaseResponseLiveData)
            getCommonPointLiveData().observe(this@ImogeActivity) { mBinding.run { tvPoint.text = Common.toPointFormat(it) }
            }
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvPurchaseHistory.setOnClickListener { openActivity(PurchaseHistoryActivity::class.java) } // 구매 내역
            tvPurchase.setOnClickListener { openBottomSheet(BottomSheet.REQUEST_IMOGE) } // 카톡 이모티콘을 신청 하시겠습니까?
            tvPresent.setOnClickListener { openBottomSheet(BottomSheet.PRESENT_PHONE_NUMBER) } // 선물
            ivHelp.setOnClickListener { openTermsActivity(getString(R.string.imoge_purchase_help_title), mViewModel.kakaoHelpLiveData.value ?: "") }

            mRadioBtnList.run {
                clear()
                add(radioButton1)
                add(radioButton2)
                add(radioButton3)
                add(radioButton4)

                forEach {
                    it.setOnClickListener(mOnClickRadioListener)
                }
            }

            etLink.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    isActivePurchaseBtn()
                }

                override fun afterTextChanged(s: Editable?) {

                }
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        mViewModel.fetchUserPoint()
    }

    private val mOnClickRadioListener = View.OnClickListener {
        mRadioBtnList.forEach { radio ->
            radio.isChecked = radio.id == it.id
        }
        isActivePurchaseBtn()
    }

    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.run {
            setOnDialogResult(object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    dismiss()
                    if (nResult == BaseDialog.RES_OK)
                    {
                        var index = -1
                        mRadioBtnList.forEachIndexed { i, radioButton ->
                            if (radioButton.isChecked)
                                index = i
                        }

                        if (index == -1 || mImogePointList.size <= index)
                            return

                        if (sheet == BottomSheet.REQUEST_IMOGE)
                        {
                            mViewModel.requestImoge(
                                buy_point = mImogePointList[index].point,
                                buy_choco = mImogePointList[index].choco,
                                buy_url = mBinding.etLink.text.toString(),
                                send_phone_num = Common.getDevicePhoneNumber(this@ImogeActivity),
                                send_type = "1"
                            )
                            // "1" 나에게
                        } else if (sheet == BottomSheet.PRESENT_PHONE_NUMBER)
                        {
                            val number = data as String
                            mViewModel.requestImoge(
                                buy_point = mImogePointList[index].point,
                                buy_choco = mImogePointList[index].choco,
                                buy_url = mBinding.etLink.text.toString(),
                                send_phone_num = number,
                                send_type = "2"
                            )
                        }
                    }
                }
            })
            showDialog(sheet, params)
        }
    }

    private fun handleUserPointResponseLiveData(status : Resource<PurchaseResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading ->
                {
                    loaderPoint.visibility = View.VISIBLE
                    tvPoint.visibility = View.INVISIBLE
                }
                is Resource.Success -> status.data?.let {
                    loaderPoint.visibility = View.INVISIBLE
                    tvPoint.visibility = View.VISIBLE
                }
                is Resource.DataError ->
                {
                    loaderPoint.visibility = View.INVISIBLE
                    tvPoint.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun handlePurchaseBtnFlag(isActive : Boolean)
    {
        mBinding.run {
            tvPurchase.run {
                isClickable = isActive
                setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
            }
            tvPresent.run {
                isClickable = isActive
                setTextColor(Color.parseColor(if (isActive) "#5c5c5c" else "#e3e3e3"))
            }
        }
    }

    private fun handlePurchaseResponseLiveData(status : Resource<PurchaseResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    showToast(getString(R.string.request_imoge_ok))
                    closeActivity()
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun isActivePurchaseBtn()
    {
        mBinding.run {
            var index = -1
            mRadioBtnList.forEachIndexed { i, radioButton ->
                if (radioButton.isChecked)
                    index = i
            }
            mViewModel.isValidValue(etLink.text.toString(), index)
        }
    }

    private fun openTermsActivity(title : String, url : String)
    {
        val i = Intent(this, TermsWebActivity::class.java).apply {
            putExtra(TermsWebActivity.EXTRA_TERMS_TITLE, title)
            putExtra(TermsWebActivity.EXTRA_TERMS_URL, url)
        }
        startActivity(i)
    }

    inner class ImogeItem(val point : String, val choco : String)
}