package com.hinos.rewardsample.ui.offerwall

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.item.ItemOfferWall
import com.hinos.rewardsample.data.dto.item.OfferWallType
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OfferWallViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val openOfferWallPrivate = MutableLiveData<ItemOfferWall?>(null)
    val openOfferWall: LiveData<ItemOfferWall?> get() = openOfferWallPrivate
    var userKey : String = ""

    init {
        viewModelScope.launch {
            userKey = mDataRepository.getCacheUserKey()
        }
    }


    val offerWallList : List<ItemOfferWall> = ArrayList<ItemOfferWall>().apply {
        add(ItemOfferWall(no = "1", title = "1번 충전소", OfferWallType.NAS))
        add(ItemOfferWall(no = "2", title = "2번 충전소",  OfferWallType.ADPOPCORN))
        add(ItemOfferWall(no = "3", title = "3번 충전소", OfferWallType.TNK))
    }

    fun openOfferWall(item : ItemOfferWall)
    {
        openOfferWallPrivate.value = item
    }
}
