package com.hinos.rewardsample.ui.phone_auth

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.PhoneAuthResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityPhoneAuthBinding
import com.hinos.rewardsample.receiver.SMSReceiver
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.sign.`in`.SignInActivity
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PhoneAuthActivity : BaseActivity<ActivityPhoneAuthBinding, PhoneAuthViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_phone_auth
    override val mViewModel: PhoneAuthViewModel by viewModels()

    @Inject
    lateinit var mReceiverSms : SMSReceiver

    override fun observeViewModel() {
        mViewModel.run {
            phoneAuth.observe(this@PhoneAuthActivity, ::handlePhoneAuth)
            nextBtnFlag.observe(this@PhoneAuthActivity, ::handlePhoneAuthBtnFlag)
            phoneTimer.observe(this@PhoneAuthActivity, ::handlePhoneTimer)
        }
    }

    override fun initListener() {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvSendPhoneAuth.setOnClickListener {
                mViewModel.requestPhoneAuth(etPhoneNum.text.toString())
            }

            etPhoneProof.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    val proofKey = s.toString()
                    if (ValueChecker.isValidProofKeyLength(proofKey) && !etPhoneNum.isEnabled) {
                        mViewModel.nextBtnFlag.value = true
                    }
                }
            })

            tvNext.setOnClickListener {
                mViewModel.certificatePhoneNumber(etPhoneNum.text.toString(), etPhoneProof.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding.etPhoneNum.run {
            setText(Common.getDevicePhoneNumber(this@PhoneAuthActivity))
            isEnabled = false
        }
        mReceiverSms.registerReceiver(this)
    }

    private fun handlePhoneTimer(millions : Long)
    {
        mBinding.run {
            if (millions > 0L) {
                tvPhoneAuthCount.run {
                    text = Common.convertTimeString(millions)
                    visibility = View.VISIBLE
                }
                return
            }

            tvPhoneAuthCount.visibility = View.INVISIBLE
        }
    }

    private fun handlePhoneAuthBtnFlag(isActive : Boolean)
    {
        mBinding.tvNext.run {
            isClickable = isActive
            setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
        }
    }

    private fun handlePhoneAuth(status : Resource<PhoneAuthResponse>)
    {
        mBinding.run {
            tvPhoneNumError.visibility = View.GONE
            tvPhoneAuthError.visibility = View.GONE
            loaderView.visibility = View.GONE
            when(status) {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { response ->
                    etPhoneProof.isEnabled = true
                    if (response.valid_time.isEmpty()) { // 인증번호 검증
                        openSignInActivity()
                    } else { // 인증번호 요청
                        startSMSRetriver()
                    }
                }
                is Resource.DataError ->
                {
                    val errorMsg = getErrorString(status.errorCode ?: 0)
                    when(status.errorCode)
                    {
                        ErrorCase.INVALID_PHONE_LENGTH -> {
                            tvPhoneNumError.text = errorMsg
                            tvPhoneNumError.visibility = View.VISIBLE
                        }
                        ErrorCase.INVALID_PHONE_ALREADY_SEND, ErrorCase.INVALID_PHONE_SEND_MAX_SIZE, ErrorCase.INVALID_PHONE_ETC -> {
                            tvPhoneAuthError.text = errorMsg
                            tvPhoneAuthError.visibility = View.VISIBLE
                        }
                        else -> {
                            showToast(errorMsg)
                        }
                    }
                }
            }
        }
//        showToast(status.toString())
    }

    private fun startSMSRetriver()
    {
        mBinding.run {
            val client = SmsRetriever.getClient(this@PhoneAuthActivity)
            val task = client.startSmsRetriever()

            task.addOnSuccessListener {
                mReceiverSms.setOnSMSListener(object : SMSReceiver.OnSMSListener {
                    override fun onSMSListener(proofKey: String) {
                        etPhoneProof.setText(proofKey)
                    }
                })
            }

            task.addOnFailureListener {
                Log.e("startSMSRetriver", "addOnFailureListener $it")
            }
        }
    }

    private fun openSignInActivity()
    {
        openActivity(SignInActivity::class.java)
        closeActivity()
    }

    override fun onDestroy()
    {
        super.onDestroy()
        mReceiverSms.unregisterReceiver(this)
    }
}