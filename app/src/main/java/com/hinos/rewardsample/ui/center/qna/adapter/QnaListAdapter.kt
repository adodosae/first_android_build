package com.hinos.rewardsample.ui.center.qna.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.response.QnaListResponse
import com.hinos.rewardsample.databinding.ItemQnaListBinding
import com.hinos.rewardsample.ui.center.qna.history.QnaHistoryViewModel
import com.hinos.rewardsample.ui.views.CornerTextView
import com.hinos.rewardsample.util.Common

class QnaListAdapter(private val viewModel: QnaHistoryViewModel) : RecyclerView.Adapter<QnaListAdapter.QnaListViewHolder>()
{
    private var mItems : MutableList<QnaListResponse.Data> = mutableListOf()

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            viewModel.openQnaDetail(item as QnaListResponse.Data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QnaListViewHolder
    {
        val itemBinding = ItemQnaListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return QnaListViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: QnaListViewHolder, position: Int)
    {
        holder.bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int
    {
        return mItems.size
    }

    fun setNewData(newItems : List<QnaListResponse.Data>)
    {
        mItems.clear()
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }


//    (REQ:기타, QNA:단순문의, ERR:오류)
    inner class QnaListViewHolder(private val itemBinding : ItemQnaListBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : QnaListResponse.Data, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                root.setOnClickListener { recyclerItemListener.onItemSelected(item) }
                tvTitle.text = item.title
                tvDate.text = item.wr_time
                tvType.text = Common.getBoardTypeString(item.bd_name)

                val s = CornerTextView(root.context)
                s.setStrokeColor(Color.parseColor("#ced4da"))
                // 답변유무 0:N, 1:Y
                if (item.reply == "0")
                {
                    tvReply.text = root.context.getString(R.string.qna_answer_wait)
                    tvReply.setStrokeColor(Color.parseColor("#ced4da"))
                    tvReply.setTextColor(Color.parseColor("#ced4da"))
                } else {
                    tvReply.text = root.context.getString(R.string.qna_answer_complete)
                    tvReply.setStrokeColor(Color.parseColor("#4582f9"))
                    tvReply.setTextColor(Color.parseColor("#4582f9"))
                }
            }
        }
    }

}