package com.hinos.rewardsample.ui.purchase.detail

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.PurchaseDetailResponse
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityPurchaseDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PurchaseDetailActivity : BaseActivity<ActivityPurchaseDetailBinding, PurchaseDetailViewModel>()
{
    override fun observeViewModel()
    {
        mViewModel.run {
            purchaseDetailLiveData.observe(this@PurchaseDetailActivity, ::handlePurchaseDetailLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    override val mLayoutResID: Int = R.layout.activity_purchase_detail
    override val mViewModel: PurchaseDetailViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initDetail()
    }

    private fun initDetail()
    {
        val seq = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_SEQ) ?: ""
        val type = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_TYPE) ?: ""

        if (seq.isEmpty() || type.isEmpty())
            closeActivity()

        mViewModel.requestDetail(seq, type)

        val brand = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_BRAND) ?: ""
        mBinding.run {
            tvBrand.text = brand
        }
    }

    private fun handlePurchaseDetailLiveData(status : Resource<PurchaseDetailResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { data ->
                    loaderView.visibility = View.GONE
                    tvGoodsName.text = data.goods_name
                    tvContent.text = data.goods_content
                    tvDeadLine.text = "${data.use_date}${getString(R.string.under_text)}"
                    when (data.goods_state)
                    {
                        "1" ->
                        {
                            tvState.text = getString(R.string.purchase_wait_text)
                            tvState.setTextColor(Color.parseColor("#ffffff"))
                            tvState.setBackgroundColor(Color.parseColor("#4582f9"))
                        }
                        "2" ->
                        {
                            tvState.text = getString(R.string.purchase_complete_text)
                            tvState.setTextColor(Color.parseColor("#4582f9"))
                            tvState.setBackgroundColor(Color.parseColor("#e9f1fb"))
                        }
                        else ->
                        {
                            tvState.text = getString(R.string.purchase_fail_text)
                            tvState.setTextColor(Color.parseColor("#ffffff"))
                            tvState.setBackgroundColor(Color.parseColor("#e45f5f"))
                        }
                    }
                    tvPincode.text = data.pincode
                    Glide.with(this@PurchaseDetailActivity).load(data.goods_img).into(ivIcon)
                    Glide.with(this@PurchaseDetailActivity).load(data.pincode_img).into(ivBarcode)

                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }
}