package com.hinos.rewardsample.ui.login

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.FindEmailResponse
import com.hinos.rewardsample.data.dto.response.FindPwdResponse
import com.hinos.rewardsample.data.dto.response.LoginResponse
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityLoginBinding
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.main.MainActivity
import com.hinos.rewardsample.ui.phone_auth.PhoneAuthActivity
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped

@AndroidEntryPoint
@ActivityScoped
class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>()
{
    companion object
    {
        const val EXTRA_EMAIL = "EXTRA_EMAIL"
    }

    override val mViewModel : LoginViewModel by viewModels()
    override val mLayoutResID: Int = R.layout.activity_login


    override fun observeViewModel()
    {
        mViewModel.run {
            loginLiveData.observe(this@LoginActivity, ::handleLoginResult)
            humanChangeStateLiveData.observe(this@LoginActivity, ::handleChangeHumanState)
            findEmailLiveData.observe(this@LoginActivity, ::handleEmailResult)
            findPwdLiveData.observe(this@LoginActivity, ::handleFindPwdResult)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            tvLogin.setOnClickListener {
                requestLogin(etEmail.text.toString(), etPwd.text.toString())
            }
            tvSignIn.setOnClickListener {
//                openActivity(SignInActivity::class.java)
                openActivity(PhoneAuthActivity::class.java)
            }
            tvFindID.setOnClickListener {
                openBottomSheet(BottomSheet.FIND_EMAIL)
            }
            tvFindPwd.setOnClickListener {
                openBottomSheet(BottomSheet.FIND_PWD)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        mBinding.run {
            val receiveEmail = intent.getStringExtra(EXTRA_EMAIL)
            etEmail.setText(receiveEmail)
            intent.removeExtra(EXTRA_EMAIL)
        }
    }

    private fun requestLogin(email : String, pwd : String)
    {
        mBinding.run {
            mViewModel.requestLogin(email, pwd)
        }
    }

    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.run {
            setOnDialogResult(object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    dismiss()
                    if (nResult == BaseDialog.RES_OK)
                    {
                        if (sheet == BottomSheet.FIND_EMAIL) {
                            val phoneNumber = data.toString()
                            mViewModel.requestFindEmail(phoneNumber)
                        }
                        else if (sheet == BottomSheet.SELECT_EMAIL) {

                        }
                        else if (sheet == BottomSheet.FIND_PWD) {
                            val email = data.toString()
                            mViewModel.requestFindPwd(email)
                        }
                        else if (sheet == BottomSheet.HUMAN_LOGIN) {
                            mViewModel.requestChangeHumanState()
                        }
                    }
                }
            })
            showDialog(sheet, params)
        }
    }

    private fun handleLoginResult(status : Resource<LoginResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    if (it.dormant_type == "1")
                    {
                        openActivity(MainActivity::class.java)
                        closeActivity()
                    } else if (it.dormant_type == "2") { // 휴면 계정
                        openBottomSheet(BottomSheet.HUMAN_LOGIN)
                    }
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleChangeHumanState(status : Resource<ResCode>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    showToast(getString(R.string.human_unlock_message))
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleFindPwdResult(status: Resource<FindPwdResponse>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    showToast(getString(R.string.send_email_password_ok))
                }
                is Resource.DataError -> {
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleEmailResult(status: Resource<FindEmailResponse>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { data ->
                    if (!data.emailList.isNullOrEmpty()) {
                        val params = HashMap<String, Any>().apply {
                            put("data", data.emailList)
                        }
                        openBottomSheet(BottomSheet.SELECT_EMAIL, params)
                    } else {
                        showToast(getString(R.string.not_cache_email_text))
                    }
                }
                is Resource.DataError -> {
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }
}