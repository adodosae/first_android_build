package com.hinos.rewardsample.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager

import com.hinos.rewardsample.databinding.FragmentNoticeListBinding

import com.hinos.rewardsample.ui.center.qna.adapter.QnaListAdapter
import com.hinos.rewardsample.ui.center.qna.history.QnaHistoryViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QnaListFragment : Fragment()
{
    companion object
    {
        private const val EXTRA_PAGE_NAME = "EXTRA_PAGE_NAME"
        private const val EXTRA_PAGE_TYPE = "EXTRA_PAGE_TYPE"

        fun newInstance(pageName : String, pageType : String) : QnaListFragment
        {
            return QnaListFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTRA_PAGE_NAME, pageName)
                    putString(EXTRA_PAGE_TYPE, pageType)
                }
            }
        }
    }



    private lateinit var mBinding : FragmentNoticeListBinding
    private val mViewModel: QnaHistoryViewModel by activityViewModels()

    private val mAdtQnaList : QnaListAdapter by lazy { QnaListAdapter(mViewModel) }

    lateinit var mPageName : String
    lateinit var mPageType : String



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        mBinding = FragmentNoticeListBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        arguments?.run {
            mPageName = getString(EXTRA_PAGE_NAME)!!
            mPageType = getString(EXTRA_PAGE_TYPE)!!
        }

        initRecyclerView()
    }

    private fun initRecyclerView()
    {
//        mViewModel = (activity as NoticeListActivity).mViewModel
//        mAdtNoticeList = NoticeListAdapter(mViewModel)
        mBinding.recyclerView.run {
            layoutManager = LinearLayoutManager(activity)
            adapter = mAdtQnaList
        }
        val list = mViewModel.qnaListLiveData.value?.data?.qnaList
        list ?: return

        val filterList = list.filter {
            if (mPageType == "ALL")
                return@filter true
            else if (mPageType == "WAIT" && it.reply == "0")
                return@filter true
            else if (mPageType == "COMPLETE" && it.reply == "1")
                return@filter true

            return@filter false
        }

        mAdtQnaList.setNewData(filterList)
    }
}
