package com.hinos.rewardsample.ui.center

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.base.EmptyViewModel
import com.hinos.rewardsample.databinding.ActivityCenterBinding
import com.hinos.rewardsample.ui.center.qna.QnaWriteActivity
import com.hinos.rewardsample.ui.center.notice.NoticeListActivity
import com.hinos.rewardsample.ui.center.qna.history.QnaHistoryActivity
import com.hinos.rewardsample.ui.web.TermsWebActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CenterActivity : BaseActivity<ActivityCenterBinding, EmptyViewModel>()
{
    override fun observeViewModel()
    {

    }

    override fun initListener()
    {
        mBinding.run {
            tvNotice.setOnClickListener { openNoticeListActivity(NoticeListActivity.LIST_NOTICE_TYPE) }
            tvFAQ.setOnClickListener { openNoticeListActivity(NoticeListActivity.LIST_FAQ_TYPE) }
            tvQnaHistory.setOnClickListener { openActivity(QnaHistoryActivity::class.java) }
            tvQna.setOnClickListener { openActivity(QnaWriteActivity::class.java) }
            ivBack.setOnClickListener { closeActivity() }
            tvShowServiceWeb.setOnClickListener { openTermsActivity(getString(R.string.terms_service_title), mViewModel.서비스이용약관.value ?: "") }
            tvShowPrivateWeb.setOnClickListener { openTermsActivity(getString(R.string.terms_private_title), mViewModel.개인정보처리방침.value ?: "") }
        }
    }

    override val mViewModel : EmptyViewModel by viewModels()
    override val mLayoutResID: Int = R.layout.activity_center


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
    }

    private fun openTermsActivity(title : String, url : String)
    {
        val i = Intent(this, TermsWebActivity::class.java).apply {
            putExtra(TermsWebActivity.EXTRA_TERMS_TITLE, title)
            putExtra(TermsWebActivity.EXTRA_TERMS_URL, url)
        }
        startActivity(i)
    }

    private fun openNoticeListActivity(type : Int)
    {
        val intent = Intent(this@CenterActivity, NoticeListActivity::class.java).apply {
            putExtra(NoticeListActivity.EXTRA_LIST_TYPE, type)
        }
        startActivity(intent)
    }
}