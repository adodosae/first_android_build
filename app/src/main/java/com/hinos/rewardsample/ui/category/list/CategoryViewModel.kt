package com.hinos.rewardsample.ui.category.list

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.CategoryRequest
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(app : Application, dataRepository : DataRepository): BaseViewModel(app, dataRepository)
{
    val mItems : MutableList<CategoryResponse.Data> = mutableListOf()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateStoreItemLiveData = MutableLiveData<StoreResponse.Data>()
    val storeItemLiveData : LiveData<StoreResponse.Data> get() = privateStoreItemLiveData


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateCategoryLiveData = MutableLiveData<Resource<CategoryResponse>>()
    val categoryLiveData : LiveData<Resource<CategoryResponse>> get() = privateCategoryLiveData


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateClickEvtCategory = MutableLiveData<CategoryResponse.Data?>()
    val clickEvtCategory: LiveData<CategoryResponse.Data?> get() = privateClickEvtCategory

    fun initCategory(item : StoreResponse.Data?)
    {
        item?.let {
            viewModelScope.launch {
                privateStoreItemLiveData.value = it
                requestCategory()
            }
        }

    }

    fun requestCategory()
    {
        viewModelScope.launch {

            val nKey = privateCategoryLiveData.value?.data?.nkey
            if (nKey == "-1") return@launch

            val data = privateStoreItemLiveData.value
            data ?: return@launch

            val pageCnt = nKey ?: "0"

            privateCategoryLiveData.value = Resource.Loading()
            val request = CategoryRequest.toRequest(
                app,
                mDataRepository.getCacheUserKey(),
                data.cate_id,
                mDataRepository.getCacheUpdateKey(),
                pageCnt,
                "20"
            )
            mDataRepository.requestCategory(request).collect {
                delay(1 * 300)
                privateCategoryLiveData.value = it
            }
        }
    }

    fun openCategoryDetail(data : CategoryResponse.Data)
    {
        viewModelScope.launch {
            privateClickEvtCategory.value = data
            privateClickEvtCategory.value = null
        }
    }
}
