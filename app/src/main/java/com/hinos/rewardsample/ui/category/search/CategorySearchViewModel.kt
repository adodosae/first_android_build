package com.hinos.rewardsample.ui.category.search

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.CategoryRequest
import com.hinos.rewardsample.data.dto.request.CategorySearchRequest
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.CategorySearchResponse
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategorySearchViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    val mItems : MutableList<CategorySearchResponse.Data> = mutableListOf()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateCategorySearchLiveData = MutableLiveData<Resource<CategorySearchResponse>>()
    val categoryLSearchLiveData : LiveData<Resource<CategorySearchResponse>> get() = privateCategorySearchLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateClickEvtCategory = MutableLiveData<CategorySearchResponse.Data?>()
    val clickEvtCategory: LiveData<CategorySearchResponse.Data?> get() = privateClickEvtCategory

    private var prevSearchText : String = ""

    fun requestCategorySearch(searchText : String, fromPaging : Boolean)
    {
//        if (searchText.length < 2)
//        {
//            privateCategorySearchLiveData.value = Resource.DataError(ErrorCase.INVALID_SEARCH_TEXT_EMPTY)
//            return
//        }

//        if (searchText == prevSearchText)
//        {
//            return
//        }
//

        viewModelScope.launch {

            val pageCnt : String
            if (fromPaging)
            {
                if (categoryLSearchLiveData.value?.data?.nkey == "-1")
                    return@launch

                pageCnt = privateCategorySearchLiveData.value?.data?.nkey ?: "0"
            }
            else
            {
                if (searchText == prevSearchText)
                {
                    return@launch
                }
                prevSearchText = searchText
                pageCnt = "0"
            }
            privateCategorySearchLiveData.value = Resource.Loading()
            val request = CategorySearchRequest.toRequest(
                app,
                mDataRepository.getCacheUserKey(),
                searchText,
                mDataRepository.getCacheUpdateKey(),
                pageCnt,
                "20"
            )
            mDataRepository.requestCategorySearch(request).collect {
                delay(1 * 300)
                it.data?.fromPaging = fromPaging
                privateCategorySearchLiveData.value = it

            }
//            mDataRepository.requestCategorySearch(request).collect {
////                if (it.data?.categorySearchList != null && it.data.categorySearchList.isNotEmpty())
////                {
////                    val list = it.data.categorySearchList
////                    privateCategorySearchLiveData.value?.data?.categorySearchList?.addAll(list)
////                }
//
//                privateCategorySearchLiveData.value = it
//            }

        }
    }

    fun openCategoryDetail(data : CategorySearchResponse.Data)
    {
        viewModelScope.launch {
            privateClickEvtCategory.value = data
            privateClickEvtCategory.value = null
        }
    }

//    fun resetNextKey()
//    {
//        privateCategorySearchLiveData.value?.data?.let {
//            it.categorySearchList.clear()
//            it.nkey = ""
//        }
//    }
}
