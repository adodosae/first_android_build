package com.hinos.rewardsample.ui.sign.`in`

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.OverlapRequest
import com.hinos.rewardsample.data.dto.request.SignInRequest
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    private val phoneNumber : String = Common.getDevicePhoneNumber(app)

    val signInData = MutableLiveData<Resource<ResCode>>()

    val checkEmail = MutableLiveData<String>() // 아이디 중복 확인
    val checkNickname = MutableLiveData<String>() // 닉네임 중복 확인

    val checkServiceAgree = MutableLiveData(false) // 서비스 이용약관 동의
    val checkPriAgree = MutableLiveData(false) // 개인정보 취급방침 동의

    private val nextBtnPrivate = MutableLiveData(false) // 가입하기
    val nextBtn: LiveData<Boolean> get() = nextBtnPrivate


    val 개인정보처리방침 : MutableLiveData<String> = MutableLiveData<String>()
    val 서비스이용약관 : MutableLiveData<String> = MutableLiveData<String>()

    init {
        viewModelScope.launch {
            개인정보처리방침.value = mDataRepository.getCachePrivacy()
            서비스이용약관.value = mDataRepository.getCacheTermSofService()
        }
    }


    private fun isValidValue(email : String,
                     pwd : String,
                     rePwd : String,
                     nickname : String,
                     recommend : String) : Int
    {
        val isValidPhoneNumber = ValueChecker.isValidPhoneNum(phoneNumber)
        val isValidEmail = ValueChecker.isValidEmail(email)
        val isValidPwd = ValueChecker.isValidPassword(pwd)
        val isValidRePwd = ValueChecker.isValidPassword(rePwd) && pwd == rePwd // 비밀번호가 일치하지 않습니다.
        val isValidNickName = ValueChecker.isValidNickName(nickname)
        val isValidRecommend = ValueChecker.isValidRecommend(recommend)

        if (!isValidPhoneNumber) {
            // 핸드폰 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_PHONE_LENGTH
        }

        if (!isValidEmail) {
            // 이메일 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_EMAIL_FORM
        }
        if (checkEmail.value != email) {
            // 이메일 중복확인 ㄱㄱ
            return ErrorCase.ETC_ERROR_CHECK_EMAIL
        }

        if (!isValidPwd) {
            // 비밀번호 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_PASSWORD_SIZE
        }

        if (!isValidRePwd) {
            // 비밀번호 확인 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_RE_PASSWORD_ETC
        }
        if (!isValidNickName) {
            // 닉네임 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_NAME_SIZE
        }
        if (checkNickname.value != nickname) {
            // 닉네임 중복확인 ㄱㄱ
            return ErrorCase.ETC_ERROR_CHECK_NICKNAME
        }
        if (!isValidRecommend) {
            // 추천인 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_RECOMMEND_ETC
        }

        if (recommend == nickname) {
            return ErrorCase.ETC_ERROR_RECO_NICKNAME
        }

        if (checkServiceAgree.value != true) {
            // 서비스 이용약관 동의 해야됨 ㄱㄱ
            return ErrorCase.ETC_ERROR_CHECK_SERVICE
        }

        if (checkPriAgree.value != true) {
            // 개인 정보 취급방침 동의해야됨 ㄱㄱ
            return ErrorCase.ETC_ERROR_CHECK_PRIVATE
        }

        return 0
    }

    fun isWrapValue(email : String,
                    pwd : String,
                    rePwd : String,
                    nickname : String,
                    recommend : String)
    {
        nextBtnPrivate.value = isValidValue(email, pwd, rePwd, nickname, recommend) == 0
    }


    fun signInUser(email : String,
               pwd : String,
               rePwd : String,
               nickname : String,
               recommend : String)
    {
        val resultCode = isValidValue(email, pwd, rePwd, nickname, recommend)
        if (resultCode != 0)
        {
            signInData.value = Resource.DataError(resultCode)
            return
        }
        signInData.value = Resource.Loading()
        viewModelScope.launch {
            mDataRepository.signInUser(
                    SignInRequest.toRequest(
                            context = getApplication(),
                            email = email,
                            pwd = pwd,
                            nickname = nickname,
                            recommend = recommend
                    )
            ).collect {
                signInData.value = it
            }
        }
    }

    fun requestOverlapEmail(email : String)
    {
        if (checkEmail.value == email) {
            return
        }

        val isValidEmail = ValueChecker.isValidEmail(email)
        if (!isValidEmail) {
            // 이메일 형식 잘못됨 ㄱㄱ
            signInData.value = Resource.DataError(ErrorCase.INVALID_EMAIL_FORM)
            return
        }

        signInData.value = Resource.Loading()
        viewModelScope.launch {
            mDataRepository.requestOverlap(
                OverlapRequest(
                    app_key = AppConfig.APP_KEY,
                    user_key = mDataRepository.getCacheUserKey(),
                    version = Common.getAppVersion(getApplication()),
                    overlap_type = "1",
                    overlap_val = email
                )
            ).collect {
                if (it.data?.res_code == "0")
                {
                    checkEmail.value = email
                } else {
                    signInData.value = Resource.DataError(ErrorCase.INVALID_EMAIL_OVERLAP)
                }
            }
        }
    }

    fun requestOverlapNickname(nickName : String, operation : () -> Unit)
    {
        if (checkNickname.value == nickName) {
            return
        }

        val isValidNickname = ValueChecker.isValidNickName(nickName)
        if (!isValidNickname) {
            signInData.value = Resource.DataError(ErrorCase.INVALID_NAME_SIZE)
            return
        }

        signInData.value = Resource.Loading()
        viewModelScope.launch {
            mDataRepository.requestOverlap(
                OverlapRequest(
                    app_key = AppConfig.APP_KEY,
                    user_key = mDataRepository.getCacheUserKey(),
                    version = Common.getAppVersion(getApplication()),
                    overlap_type = "2",
                    overlap_val = nickName
                )
            ).collect {
                if (it.data?.res_code == "0")
                {
                    checkNickname.value = nickName
                } else {
                    signInData.value = Resource.DataError(ErrorCase.INVALID_NAME_OVERLAP)
                }
                operation()
            }
        }
    }
}
