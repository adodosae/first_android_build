package com.hinos.rewardsample.ui.category.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.CategorySearchResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityCategorySearchBinding
import com.hinos.rewardsample.ui.category.adapter.CategorySearchAdapter
import com.hinos.rewardsample.ui.category.detail.CategoryDetailActivity
import com.hinos.rewardsample.util.ErrorCase
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategorySearchActivity : BaseActivity<ActivityCategorySearchBinding, CategorySearchViewModel>()
{
    override fun observeViewModel()
    {
        mViewModel.run {
            categoryLSearchLiveData.observe(this@CategorySearchActivity, ::handleCategorySearchLiveData)
            clickEvtCategory.observe(this@CategorySearchActivity, ::handleCategoryDetailLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            ivSearch.setOnClickListener { searchCategory() }
            etSearch.setOnEditorActionListener { _, actionId, _ ->
                var handled = false
                if (actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    searchCategory()
                    handled = true
                }
                return@setOnEditorActionListener handled
            }
        }
    }

    override val mViewModel : CategorySearchViewModel by viewModels()
    override val mLayoutResID: Int = R.layout.activity_category_search
    private val mAdtCategorySearch : CategorySearchAdapter by lazy { CategorySearchAdapter(mViewModel) }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initRecyclerView()
    }

    private fun initRecyclerView()
    {

        mBinding.recyclerView.run {
            val mgrLayoutManager = LinearLayoutManager(this@CategorySearchActivity)
            layoutManager = mgrLayoutManager
            adapter = mAdtCategorySearch


            addOnScrollListener(object : RecyclerView.OnScrollListener()
            {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
                {
                    if (!mAdtCategorySearch.mShowMore)
                    {
                        val lastVisibleItemPosition = mgrLayoutManager.findLastCompletelyVisibleItemPosition()
                        val itemTotalCount = mAdtCategorySearch.itemCount -2

                        if (lastVisibleItemPosition >= itemTotalCount)
                        {
                            mViewModel.requestCategorySearch(mBinding.etSearch.text.toString(), true)
                        }
                    }
                }
            })
        }
    }

    private fun handleCategorySearchLiveData(status : Resource<CategorySearchResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> mAdtCategorySearch.loading()
                is Resource.Success -> status.data?.let {
                    if (it.fromPaging)
                        setAddData(it.categorySearchList)
                    else
                        setNewData(it.categorySearchList)
                }
                is Resource.DataError ->
                {
                    clearData(status.errorCode ?: -1)
                }
            }
        }
    }

    private fun handleCategoryDetailLiveData(data : CategorySearchResponse.Data?)
    {
        data ?: return

        val responseData = CategoryResponse.Data(data.goods_code, data.goods_name, data.goods_img, data.goods_price, data.goods_name)
        val i = Intent(this, CategoryDetailActivity::class.java).apply {
            putExtra(CategoryDetailActivity.EXTRA_CATEGORY_DETAIL_DATA, responseData)
        }
        openActivity(i)
    }
    private fun setAddData(newData : List<CategorySearchResponse.Data>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            tvHelp.visibility = View.INVISIBLE
            recyclerView.visibility = View.VISIBLE
            mAdtCategorySearch.addNewData(newData)
            mAdtCategorySearch.hideMore()
        }
    }
    private fun setNewData(newData : List<CategorySearchResponse.Data>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            tvHelp.visibility = View.INVISIBLE
            recyclerView.visibility = View.VISIBLE
            mAdtCategorySearch.setNewData(newData)
            mAdtCategorySearch.hideMore()
        }
    }
    private fun clearData(errorCode : Int)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            showToast(errorCode)
            when(errorCode)
            {
                ErrorCase.SERVER_ERROR_SEARCH_EMPTY ->
                {
                    tvHelp.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    mAdtCategorySearch.clearData()
                    mAdtCategorySearch.hideMore()
                }
            }
        }
    }

    var prevSearch : String = ""
    private fun searchCategory()
    {
        val searchText = mBinding.etSearch.text.toString()
        if (searchText.length < 2)
        {
            showToast(ErrorCase.INVALID_SEARCH_TEXT_EMPTY)
            return
        }

        if (searchText == prevSearch)
            return

        prevSearch = searchText

        mViewModel.requestCategorySearch(mBinding.etSearch.text.toString(), false)
    }
}