package com.hinos.rewardsample.ui.phone_auth

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.PhoneAuthRequest
import com.hinos.rewardsample.data.dto.response.PhoneAuthResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class PhoneAuthViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{

    private val TAG = this.javaClass.simpleName

    private val phoneAuthPrivate = MutableLiveData<Resource<PhoneAuthResponse>>() // 휴대폰 인증 시도
    val phoneAuth: LiveData<Resource<PhoneAuthResponse>> get() = phoneAuthPrivate


    private val nextBtnFlagPrivate = MutableLiveData<Boolean>(false) // 휴대폰 인증 다음 버튼
    val nextBtnFlag = nextBtnFlagPrivate

    private val phoneTimerPrivate = MutableLiveData<Long>(-1)
    val phoneTimer = phoneTimerPrivate

    fun requestPhoneAuth(phoneNumber : String) // 휴대폰 인증번호 요청
    {
        val isValidPhoneNumLength = ValueChecker.isValidPhoneNumLength(phoneNumber)
        if (!isValidPhoneNumLength) {
            phoneAuthPrivate.value = Resource.DataError(ErrorCase.INVALID_PHONE_LENGTH)
            return
        }

        if (phoneTimerPrivate.value != -1L) {
            phoneAuthPrivate.value = Resource.DataError(ErrorCase.INVALID_PHONE_ALREADY_SEND)
            return
        }

        val signatures = Common.getAppSignatures(app)
        if (signatures.isEmpty()) {
            phoneAuthPrivate.value = Resource.DataError(-1)
            return
        }


        viewModelScope.launch {
            phoneAuthPrivate.value = Resource.Loading()
            mDataRepository.requestPhoneAuth(
                PhoneAuthRequest(
                    app_key = AppConfig.APP_KEY,
                    android_id = Common.getAndroidId(app),
                    version = Common.getAppVersion(app),
                    phone_num = phoneNumber,
                    hash_key = signatures[0],
                    proof_key = ""
                )
            ).collect { response ->
                response.data?.let {
                    if (it.valid_time.isNotEmpty()) {
                        val validTime = (it.valid_time).toLong()
                        startAuthTime(validTime)
                    }
                }
                phoneAuthPrivate.value = response
            }
        }
    }

    fun certificatePhoneNumber(phoneNumber : String, proofKey : String) // 휴대폰 인증번호 확인
    {
        val isValidPhoneNumLength = ValueChecker.isValidPhoneNumLength(phoneNumber)
        if (!isValidPhoneNumLength) {
            phoneAuthPrivate.value = Resource.DataError(ErrorCase.INVALID_PHONE_LENGTH)
            return
        }

        val signatures = Common.getAppSignatures(app)
        if (signatures.isEmpty())
        {
            phoneAuthPrivate.value = Resource.DataError(-1)
            return
        }


        viewModelScope.launch {
            phoneAuthPrivate.value = Resource.Loading()
            mDataRepository.requestPhoneAuth(
                PhoneAuthRequest(
                    app_key = AppConfig.APP_KEY,
                    android_id = Common.getAndroidId(app),
                    version = Common.getAppVersion(app),
                    phone_num = phoneNumber,
                    hash_key = signatures[0],
                    proof_key = proofKey
                )
            ).collect {
                phoneAuthPrivate.value = it
            }
        }
    }

    private var mTimerJob : Job? = null

    private fun startAuthTime(millions : Long)
    {
        mTimerJob?.cancel()
        mTimerJob = viewModelScope.launch {
            var time = millions
            while (time > 0) {
                phoneTimerPrivate.value = time
                delay(1 * 1000)
                time -= 1000
            }
            phoneTimerPrivate.value = -1
            mTimerJob?.cancel()
        }
    }
}
