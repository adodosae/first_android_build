package com.hinos.rewardsample.ui.web

import android.os.Bundle
import android.webkit.WebViewClient
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.base.EmptyViewModel
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityTermsWebBinding
import com.hinos.rewardsample.ui.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TermsWebActivity : BaseActivity<ActivityTermsWebBinding, EmptyViewModel>()
{
    companion object
    {
        val EXTRA_TERMS_URL = "EXTRA_TERMS_URL"
        val EXTRA_TERMS_TITLE = "EXTRA_TERMS_TITLE"
    }

    override val mLayoutResID: Int = R.layout.activity_terms_web
    override val mViewModel: EmptyViewModel by viewModels()

    override fun observeViewModel() {
    }


    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()

        val title = intent.getStringExtra(EXTRA_TERMS_TITLE) ?: ""
        val url = intent.getStringExtra(EXTRA_TERMS_URL) ?: ""

        if (title.isEmpty() || url.isEmpty()) {
            showToast(this.applicationContext, getString(R.string.error_etc))
            closeActivity()
        }

        mBinding.run {
            tvTitle.text = title
            wvContents.webViewClient = WebViewClient()
            wvContents.loadUrl(url)
        }
    }
}