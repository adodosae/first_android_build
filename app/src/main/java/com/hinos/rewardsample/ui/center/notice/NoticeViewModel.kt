package com.hinos.rewardsample.ui.center.notice

import android.app.Application
import android.content.Intent
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.NoticeListRequest
import com.hinos.rewardsample.data.dto.request.NoticeReadRequest
import com.hinos.rewardsample.data.dto.request.RefundRequest
import com.hinos.rewardsample.data.dto.response.*
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.ui.fragment.NoticeListFragment
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.SingleLiveEvent
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import okhttp3.internal.immutableListOf
import javax.annotation.concurrent.Immutable
import javax.inject.Inject

@HiltViewModel
class NoticeViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    companion object
    {
        const val EXTRA_NOTICE_DETAIL_SEQ = "EXTRA_NOTICE_DETAIL_SEQ"
        const val EXTRA_NOTICE_DETAIL_TYPE = "EXTRA_NOTICE_DETAIL_TYPE"
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateNoticeListLiveData = MutableLiveData<Resource<NoticeListResponse>>()
    val noticeListLiveData : LiveData<Resource<NoticeListResponse>> get() = privateNoticeListLiveData


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateFAQListLiveData = MutableLiveData<Resource<NoticeListResponse>>()
    val faqListLiveData : LiveData<Resource<NoticeListResponse>> get() = privateFAQListLiveData


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateNoticeListResponseLiveData = MutableLiveData<Resource<Int>>()
    val noticeListResponseLiveData : LiveData<Resource<Int>> get() = privateNoticeListResponseLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateNoticeReadLiveData = MutableLiveData<Resource<NoticeReadResponse>>()
    val noticeReadLiveData : LiveData<Resource<NoticeReadResponse>> get() = privateNoticeReadLiveData


    val clickEvtNoticeDetail: SingleLiveEvent<NoticeListResponse.Data> = SingleLiveEvent()


    // ViewPager 안에 프레그먼트를 넣으면 재생성할때 문제가 발생한다.
    // 따라서 페이지 타입에 따라 각각 데이터 요청을 해야 한다.
    fun requestBoard(bd_name : String) {
        viewModelScope.launch {

            val data: MutableLiveData<Resource<NoticeListResponse>> = if (bd_name == "NOTICE")
                privateNoticeListLiveData
            else
                privateFAQListLiveData

            data.value = Resource.Loading()

            val request = NoticeListRequest.toRequest(app, mDataRepository.getCacheUserKey(), bd_name)
            mDataRepository.requestBoard(request).collect {
                data.value = it
            }




/*        viewModelScope.launch {
            privateNoticeListResponseLiveData.value = Resource.Loading()

            val requestNotice = NoticeListRequest.toRequest(app, mDataRepository.getCacheUserKey(), "NOTICE")
            val requestFAQ = NoticeListRequest.toRequest(app, mDataRepository.getCacheUserKey(), "FAQ")

            val flow1 = mDataRepository.requestBoard(requestNotice)
            val flow2 = mDataRepository.requestBoard(requestFAQ)

            flow1.zip(flow2) { noticeResponse, faqResponse ->
                privateNoticeListLiveData.value = noticeResponse.data?.boardList ?: ArrayList()
                privateFAQListLiveData.value = faqResponse.data?.boardList ?: ArrayList()

                if (noticeResponse is Resource.DataError)
                    return@zip Resource.DataError<Int>(noticeResponse.errorCode!!)
                else if (faqResponse is Resource.DataError)
                    return@zip Resource.DataError<Int>(faqResponse.errorCode!!)
                return@zip Resource.Success(0)
            }.collect {
                privateNoticeListResponseLiveData.value = it
            }
        }*/
        }
    }

    fun requestNoticeDetail(bd_name : String, seq : String)
    {
        viewModelScope.launch {
            privateNoticeReadLiveData.value = Resource.Loading()

            val request = NoticeReadRequest.toRequest(
                app,
                mDataRepository.getCacheUserKey(),
                bd_name,
                seq
            )

            mDataRepository.requestNoticeDetail(request).collect {
                privateNoticeReadLiveData.value = it
            }
        }

    }

    fun openNoticeDetail(data : NoticeListResponse.Data)
    {
        viewModelScope.launch {
            clickEvtNoticeDetail.value = data
        }
    }
}
