package com.hinos.rewardsample.ui.dialog

import android.app.Activity
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatDialog
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.util.AppConfig
import com.rdev.adfactory.AdsMgr
import com.rdev.adfactory.AdsMobile
import com.rdev.adfactory.Builder
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
open class GeneralDialog @Inject constructor(@ActivityContext val activity: Activity) : AppCompatDialog(
    activity,
    R.style.FullScreenDialogTheme
), BaseDialog
{
    private var mDialogResultCallback : BaseDialog.OnDialogResult? = null
    private  var mClosed = true
    private var mgrAds : AdsMgr? = null

    override fun onBackPressed()
    {
        sendResult(BaseDialog.RES_CANCEL, 0)
    }

    override fun dismiss()
    {
        super.dismiss()
        mClosed = true
    }

    fun showDialog(type: DialogSheet, params : HashMap<String, Any>? = null)
    {
        if (!mClosed)
            return

        mClosed = false

        val layout : View
        when(type)
        {
            DialogSheet.DIALOG_REVIEW -> {
                layout = layoutInflater.inflate(R.layout.dialog_exit_with_review, null).apply {
                    findViewById<TextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                    findViewById<TextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<TextView>(R.id.tvReview).setOnClickListener {
                        sendResult(BaseDialog.RES_REVIEW, -1)
                    }
                }
            }

            DialogSheet.DIALOG_EXIT ->
            {
                if (AdsMobile.getInitailized())
                {
                    if (AdsMobile.getExitNativeData().isNotEmpty())
                    {
                        val builder = Builder(activity)
//                            .addXwLogMode(AppConfig.DEBUG)
                            .addNativeLayout(
                                R.layout.ad_native_admob_large,
                                R.layout.ad_native_cauly_large,
                                R.layout.ad_native_facebook_large,
                                R.layout.ad_native_wad_large
                            )
                        if (AppConfig.DEBUG)
                            builder.addTestDevice(AppConfig.TESTKEY)


                        mgrAds = AdsMobile.getAdsMgr(builder)
                    }
                }

                layout = layoutInflater.inflate(R.layout.dialog_exit, null).apply {
                    val llNative = findViewById<LinearLayout>(R.id.llNative)
                    if (mgrAds != null)
                        llNative.visibility = View.VISIBLE
                     else
                        llNative.visibility = View.GONE
                    findViewById<TextView>(R.id.tvOK).setOnClickListener {
                        if (llNative.childCount > 0) {
                            mgrAds?.sendNativeExposure()
                        }
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                    findViewById<TextView>(R.id.tvCancel).setOnClickListener {
                        if (llNative.childCount > 0) {
                            mgrAds?.sendNativeExposure()
                        }
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    mgrAds?.loadNative(llNative, AdsMobile.getExitNativeData())
                }
            }
            DialogSheet.DIALOG_NETWORK_ERROR ->
            {
                layout = layoutInflater.inflate(R.layout.bottom_request_imoge, null).apply {
                    findViewById<TextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                    findViewById<TextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                }
            }

            DialogSheet.DIALOG_MUST_UPDATE ->
            {
                layout = layoutInflater.inflate(R.layout.dialog_update, null).apply {
                    findViewById<TextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                    findViewById<TextView>(R.id.tvCancel).apply {
                        visibility = View.GONE
                        setOnClickListener {
                            sendResult(BaseDialog.RES_CANCEL, -1)
                        }
                    }
                }
            }

            DialogSheet.DIALOG_SHOULD_UPDATE ->
            {
                layout = layoutInflater.inflate(R.layout.dialog_update, null).apply {
                    findViewById<TextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                    findViewById<TextView>(R.id.tvCancel).apply {
                        visibility = View.VISIBLE
                        setOnClickListener {
                            sendResult(BaseDialog.RES_CANCEL, -1)
                        }
                    }
                }
            }
        }

        setContentView(layout)
        show()
    }

    private fun sendResult(nRes: Int, data: Any)
    {
        if (mClosed)
            return

        if (isShowing)
            dismiss()

        mDialogResultCallback?.onDialogResult(nRes, data)
    }

    fun setOnDialogResult(h: BaseDialog.OnDialogResult)
    {
        mDialogResultCallback = h
    }

}


enum class DialogSheet
{
    DIALOG_REVIEW, DIALOG_EXIT, DIALOG_NETWORK_ERROR, DIALOG_MUST_UPDATE, DIALOG_SHOULD_UPDATE
}