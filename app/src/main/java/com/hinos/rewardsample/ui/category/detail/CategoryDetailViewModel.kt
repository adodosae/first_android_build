package com.hinos.rewardsample.ui.category.detail

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.CategoryDetailRequest
import com.hinos.rewardsample.data.dto.response.CategoryDetailResponse
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryDetailViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateCategoryItemLiveData = MutableLiveData<CategoryResponse.Data>()
    val categoryItemLiveData : LiveData<CategoryResponse.Data> get() = privateCategoryItemLiveData



    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateCategoryDetailLiveData = MutableLiveData<Resource<CategoryDetailResponse>>()
    val categoryDetailLiveData : LiveData<Resource<CategoryDetailResponse>> get() = privateCategoryDetailLiveData



    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privatePurchaseResponseLiveData = MutableLiveData<Resource<PurchaseResponse>>()
    val purchaseResponseLiveData : LiveData<Resource<PurchaseResponse>> get() = privatePurchaseResponseLiveData

    init
    {
        requestSessionUser()
    }


    fun initCategoryDetail(data : CategoryResponse.Data?)
    {
        data ?: return
        viewModelScope.launch {
            privateCategoryItemLiveData.value = data
        }
    }

    fun requestCategoryDetail(data : CategoryResponse.Data)
    {
        viewModelScope.launch {
            val request = CategoryDetailRequest.toRequest(app, mDataRepository.getCacheUserKey(), data.goods_id, mDataRepository.getCacheUpdateKey())
            mDataRepository.requestCategoryDetail(request).collect {
                privateCategoryDetailLiveData.value = it
            }
        }
    }

    fun purchaseProduct()
    {
        viewModelScope.launch {
            val data = privateCategoryDetailLiveData.value?.data
            data ?: return@launch


            val point = getCommonPointLiveData().value!!
            if (data.goods_price.toInt() > point)
            {
                privatePurchaseResponseLiveData.value = Resource.DataError(ErrorCase.SERVER_ERROR_LESS_POINT)
                return@launch
            }


            privatePurchaseResponseLiveData.value = Resource.Loading()

            val buyRequest = CategoryDetailRequest.toRequest(app, mDataRepository.getCacheUserKey(), data.goods_id, mDataRepository.getCacheUpdateKey())
            mDataRepository.purchaseProduct(buyRequest).collect {
                val userPoint = it.data?.user_point
                if (!userPoint.isNullOrEmpty())
                    getCommonPointLiveData().value = userPoint.toInt()
                privatePurchaseResponseLiveData.value = it
            }
        }
    }
}
