package com.hinos.rewardsample.ui.sign.out

import android.graphics.Color

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivitySignOutBinding
import com.hinos.rewardsample.receiver.LogoutReceiver
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.dialog.GeneralSheet

import com.hinos.rewardsample.ui.login.LoginActivity
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SignOutActivity : BaseActivity<ActivitySignOutBinding, SignOutViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_sign_out
    override val mViewModel: SignOutViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            signOutResponseResult.observe(this@SignOutActivity, ::handleSignOutResult)
            nextBtnState.observe(this@SignOutActivity, ::handleNextBtnState)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            etPwd.addTextChangedListener {
                tvPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etPwd.text.toString())) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }
            ivBack.setOnClickListener { closeActivity() }
            tvNext.setOnClickListener { openBottomSheet(etPwd.text.toString()) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
    }

    private fun openBottomSheet(pwd : String)
    {
        mSheet.setOnDialogResult(object : BaseDialog.OnDialogResult
        {
            override fun onDialogResult(nResult: Int, data: Any)
            {
                if (nResult == BaseDialog.RES_OK)
                {
                    mViewModel.signOutUser(pwd)
                }
                mSheet.dismiss()
            }
        })
        mSheet.showDialog(BottomSheet.SIGN_OUT)
    }

    private fun handleSignOutResult(status : Resource<ResCode>)
    {
        mBinding.run {
            tvPasswordError.visibility = View.GONE
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { _ -> // 회원가입 완료
                    LogoutReceiver.send(this@SignOutActivity)
                    openActivity(LoginActivity::class.java)
                }
                is Resource.DataError ->
                {
                    val errorMsg = getErrorString(status.errorCode ?: 0)
                    when(status.errorCode)
                    {
                        ErrorCase.INVALID_PASSWORD_ETC, ErrorCase.INVALID_PASSWORD_SIZE, ErrorCase.SERVER_ERROR_ID_AND_PASSWORD_INCORRECT ->
                        {
                            tvPasswordError.run {
                                text = errorMsg
                                visibility = View.VISIBLE
                            }
                        }
                        else -> {
                            showToast(errorMsg)
                        }
                    }
                }
            }
        }
    }

    private fun handleNextBtnState(isActive : Boolean)
    {
        mBinding.tvNext.run {
            isClickable = isActive
            setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
        }
    }

    private fun checkNextBtnState()
    {
        mBinding.run {
            mViewModel.isWrapValue(etPwd.text.toString())
        }
    }
}