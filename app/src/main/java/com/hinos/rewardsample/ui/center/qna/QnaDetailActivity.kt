package com.hinos.rewardsample.ui.center.qna

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.QnaListResponse
import com.hinos.rewardsample.databinding.ActivityQnaDetailBinding
import com.hinos.rewardsample.ui.center.qna.history.QnaHistoryViewModel
import com.hinos.rewardsample.ui.refund.history.RefundHistoryViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QnaDetailActivity : BaseActivity<ActivityQnaDetailBinding, QnaHistoryViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_qna_detail
    override val mViewModel: QnaHistoryViewModel by viewModels()

    override fun observeViewModel()
    {

    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            ivQuestionFold.setOnClickListener {
                foldQuestion(!mQuestionFold)
            }
            ivAnswerFold.setOnClickListener {
                foldAnswer(!mAnswerFold)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initViewSetting()
    }

    private fun initViewSetting()
    {
        val data = intent.getParcelableExtra<QnaListResponse.Data>(QnaHistoryViewModel.EXTRA_QNA_DETAIL)
        data ?: closeActivity()
        foldQuestion(false)
        mBinding.run {
            tvTitle.text = data?.title
            tvDate.text = data?.wr_time
            tvContents.text = data?.contents
            tvAnswer.text = data?.reply_contents

            if (data?.reply == "0")
            {
                tvReply.text = root.context.getString(R.string.qna_answer_wait)
                tvReply.setStrokeColor(Color.parseColor("#ced4da"))
                tvReply.setTextColor(Color.parseColor("#ced4da"))
                foldQuestion(false)
                foldAnswer(true)
                tvAnswerHelp.visibility = View.GONE
                tvAnswerDate.visibility = View.GONE
                ivAnswerFold.visibility = View.GONE
                under2.visibility = View.INVISIBLE

            } else {
                tvReply.text = root.context.getString(R.string.qna_answer_complete)
                tvReply.setStrokeColor(Color.parseColor("#4582f9"))
                tvReply.setTextColor(Color.parseColor("#4582f9"))
                foldQuestion(false)
                foldAnswer(false)
                tvAnswerHelp.visibility = View.VISIBLE
                tvAnswerDate.visibility = View.VISIBLE
                ivAnswerFold.visibility = View.VISIBLE
                under2.visibility = View.VISIBLE
            }

            iv1.setOnClickListener {
                ZoomActivity.openActivity(this@QnaDetailActivity, data, 0)
            }
            iv2.setOnClickListener {
                ZoomActivity.openActivity(this@QnaDetailActivity, data, 1)
            }
            iv3.setOnClickListener {
                ZoomActivity.openActivity(this@QnaDetailActivity, data, 2)
            }

            if (!data?.img_url1.isNullOrEmpty())
                Glide.with(this@QnaDetailActivity).load(data?.img_url1).into(iv1)
            else
                iv1.visibility = View.GONE

            if (!data?.img_url2.isNullOrEmpty())
                Glide.with(this@QnaDetailActivity).load(data?.img_url2).into(iv2)
            else
                iv2.visibility = View.GONE

            if (!data?.img_url3.isNullOrEmpty())
                Glide.with(this@QnaDetailActivity).load(data?.img_url3).into(iv3)
            else
                iv3.visibility = View.GONE
        }
    }


    var mQuestionFold = false
    private fun foldQuestion(fold : Boolean)
    {
        mBinding.run {
            if (fold)
            {
                containerQuestion.visibility = View.GONE
                under1.visibility = View.VISIBLE
                Glide.with(ivQuestionFold).load(R.drawable.ic_dropdown).into(ivQuestionFold)
            }
             else
            {
                containerQuestion.visibility = View.VISIBLE
                under1.visibility = View.INVISIBLE
                Glide.with(ivQuestionFold).load(R.drawable.ic_dropup).into(ivQuestionFold)
            }
        }

        mQuestionFold = fold
    }

    var mAnswerFold = false
    private fun foldAnswer(fold : Boolean)
    {
        mBinding.run {
            if (fold)
            {
                containerAnswer.visibility = View.GONE
                under2.visibility = View.VISIBLE
                Glide.with(ivAnswerFold).load(R.drawable.ic_dropdown).into(ivAnswerFold)
            }
            else
            {
                containerAnswer.visibility = View.VISIBLE
                under2.visibility = View.INVISIBLE
                Glide.with(ivAnswerFold).load(R.drawable.ic_dropup).into(ivAnswerFold)
            }
        }
        mAnswerFold = fold
    }

}