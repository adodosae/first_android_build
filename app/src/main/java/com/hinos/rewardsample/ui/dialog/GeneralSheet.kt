package com.hinos.rewardsample.ui.dialog

import android.content.Context
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.FindEmailResponse
import com.hinos.rewardsample.ui.views.CornerTextView
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

@ActivityScoped
open class GeneralSheet @Inject constructor(@ActivityContext context: Context, attrs : Int = R.style.BottomSheetDialog) : BottomSheetDialog(
    context,
    attrs
), BaseDialog
{
    private var mDialogResultCallback : BaseDialog.OnDialogResult? = null
    private  var mClosed = true

    override fun onBackPressed() {
        sendResult(BaseDialog.RES_CANCEL, 0)
    }

    override fun dismiss() {
        super.dismiss()
        mClosed = true
    }

    fun showDialog(type: BottomSheet, params : HashMap<String, Any>? = null)
    {
        if (!mClosed)
            return

        mClosed = false

        val layout : View
        when(type)
        {
            BottomSheet.FIND_PWD -> {
                layout = layoutInflater.inflate(R.layout.bottom_search_password, null).apply {
                    val etEmail = findViewById<EditText>(R.id.etEmail)
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener { // 취소
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener { // 비밀번호 재설정
                        val email = etEmail.text.toString()
                        val isValid = ValueChecker.isValidEmail(email)
                        if (!isValid) {
                            Toast.makeText(context, ErrorCase.errorHashtable[ErrorCase.INVALID_EMAIL_FORM]!!, Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }
                        sendResult(BaseDialog.RES_OK, email)
                    }
                }
            }

            BottomSheet.INVALID_RECOMMEND -> {
                layout = layoutInflater.inflate(R.layout.bottom_recommend_invalid, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.FIND_EMAIL -> {
                layout = layoutInflater.inflate(R.layout.bottom_find_email, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        val phoneNumber = findViewById<EditText>(R.id.etPhoneNum).text.toString()
                        val isValid = ValueChecker.isValidPhoneNum(phoneNumber)
                        if (!isValid) {
                            Toast.makeText(context, ErrorCase.errorHashtable[ErrorCase.INVALID_PHONE_LENGTH]!!, Toast.LENGTH_SHORT).show()
                            return@setOnClickListener
                        }
                        sendResult(BaseDialog.RES_OK, phoneNumber)
                    }
                }
            }

            BottomSheet.SELECT_EMAIL -> {
                layout = layoutInflater.inflate(R.layout.bottom_select_email, null).apply {
                    val dataArray = params?.get("data") as ArrayList<FindEmailResponse.Data>
//                    val emails : List<String> = params?.get("emailList") as List<String>
                    val llList = findViewById<LinearLayout>(R.id.llList)
                    for (i in dataArray.indices) {
                        val itemLayout = layoutInflater.inflate(R.layout.item_select_email, null).apply {
                            layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                        }
                        itemLayout.run {
                            findViewById<TextView>(R.id.tvEmail).text = Common.hideEmail(dataArray[i].email)
                            if (i == dataArray.size-1) {
                                findViewById<View>(R.id.vUnderLine).visibility = View.INVISIBLE
                            }
                        }
                        llList.addView(itemLayout)
                    }

                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.HUMAN_LOGIN -> {
                layout = layoutInflater.inflate(R.layout.bottom_human_sign, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.PURCHASE_PRODUCT -> {
                layout = layoutInflater.inflate(R.layout.bottom_purchase_product, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.REQUEST_IMOGE -> {
                layout = layoutInflater.inflate(R.layout.bottom_request_imoge, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.REQUEST_REFUND_POINT -> {
                layout = layoutInflater.inflate(R.layout.bottom_refund_point, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.REQUEST_REFUND_POINT_CANCEL -> {
                layout = layoutInflater.inflate(R.layout.bottom_refund_point_cancel, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }
            BottomSheet.REQUEST_INQUIRIES -> {
                layout = layoutInflater.inflate(R.layout.bottom_request_inquiries, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }
            BottomSheet.PRESENT_PHONE_NUMBER -> {
                layout = layoutInflater.inflate(R.layout.bottom_input_phone, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        val phoneNumber = findViewById<EditText>(R.id.etPhone).text.toString()
                        sendResult(BaseDialog.RES_OK, phoneNumber)
                    }
                }
            }
            BottomSheet.CONFIRM_INQUIRIES -> {
                layout = layoutInflater.inflate(R.layout.bottom_confirm_inquiries, null).apply {
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }
            BottomSheet.BACK_CANCEL -> {
                layout = layoutInflater.inflate(R.layout.bottom_back_cancel, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }
            BottomSheet.SIGN_OUT -> {
                layout = layoutInflater.inflate(R.layout.bottom_signout, null).apply {
                    findViewById<CornerTextView>(R.id.tvCancel).setOnClickListener {
                        sendResult(BaseDialog.RES_CANCEL, -1)
                    }
                    findViewById<CornerTextView>(R.id.tvOK).setOnClickListener {
                        sendResult(BaseDialog.RES_OK, -1)
                    }
                }
            }

            BottomSheet.BOTTOM_INQUIRY -> {
                layout = layoutInflater.inflate(R.layout.bottom_inquiry_type, null).apply {
                    findViewById<TextView>(R.id.tvType1).apply {
                        setOnClickListener {
                            sendResult(BaseDialog.RES_OK, tag)
                        }
                    }
                    findViewById<TextView>(R.id.tvType2).apply {
                        setOnClickListener {
                            sendResult(BaseDialog.RES_OK, tag)
                        }
                    }
                    findViewById<TextView>(R.id.tvType3).apply {
                        setOnClickListener {
                            sendResult(BaseDialog.RES_OK, tag)
                        }
                    }
                }
            }
        }

        setContentView(layout)
        show()
    }

    private fun sendResult(nRes: Int, data: Any)
    {
        if (mClosed)
            return

        if (isShowing)
            dismiss()

        mDialogResultCallback?.onDialogResult(nRes, data)
    }

    fun setOnDialogResult(h: BaseDialog.OnDialogResult)
    {
        mDialogResultCallback = h
    }
}


enum class BottomSheet
{
    FIND_EMAIL,
    SELECT_EMAIL,
    FIND_PWD,
    INVALID_RECOMMEND,
    HUMAN_LOGIN,
    PURCHASE_PRODUCT,
    REQUEST_IMOGE,
    PRESENT_PHONE_NUMBER,
    REQUEST_REFUND_POINT,
    REQUEST_REFUND_POINT_CANCEL,
    REQUEST_INQUIRIES,
    CONFIRM_INQUIRIES,
    BACK_CANCEL,
    SIGN_OUT,
    BOTTOM_INQUIRY
}