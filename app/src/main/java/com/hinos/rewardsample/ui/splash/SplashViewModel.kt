package com.hinos.rewardsample.ui.splash

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.AppSettingRequest
import com.hinos.rewardsample.data.dto.request.LoginRequest
import com.hinos.rewardsample.data.dto.request.SplashRequest
import com.hinos.rewardsample.data.dto.response.AppSettingResponse
import com.hinos.rewardsample.data.dto.response.LoginResponse
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.dto.response.SplashResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val privateAppSettingResponseLiveData = MutableLiveData<Resource<AppSettingResponse>>()
    val appSettingResponseLiveData : LiveData<Resource<AppSettingResponse>> get() = privateAppSettingResponseLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    val privateSplashResponseLiveData = MutableLiveData<Resource<SplashResponse>>()
    val splashResponseLiveData : LiveData<Resource<SplashResponse>> get() = privateSplashResponseLiveData



    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val loginLiveDataPrivate = MutableLiveData<Resource<LoginResponse>>()
    val loginLiveData: LiveData<Resource<LoginResponse>> get() = loginLiveDataPrivate

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val remainMillionsLiveDataPrivate = MutableLiveData<Long>()
    val timeOutLiveData: LiveData<Long> get() = remainMillionsLiveDataPrivate



    fun requestAppSetting()
    {
        viewModelScope.launch {
            privateAppSettingResponseLiveData.value = Resource.Loading()

            val request = AppSettingRequest.toRequest(
                app,
                mDataRepository.getCacheUserKey()
            )

            mDataRepository.run {
                requestAppSetting(request).collect { response ->
                    response.data?.let { settingResponse ->
                        cacheAppVer(settingResponse.app_ver)
                        cacheMinVer(settingResponse.min_ver)
                        cachePopVer(settingResponse.pop_ver)
                        cacheMarketUrl(settingResponse.market_url)

                        cacheInviteUrl(response.data.invite)
                        cacheKakaoHelp(response.data.kakaohelp)
                        cacheRefundHelp(response.data.refundhelp)
                        cachePrivacy(response.data.privacy)
                        cacheTermSofService(response.data.termsofservice)

                        privateAppSettingResponseLiveData.value = response
                    }
                }
            }
        }
    }

    fun requestSplash()
    {
        viewModelScope.launch {
            val userKey = mDataRepository.getCacheUserKey()
            val loginKey = mDataRepository.getCacheLoginKey()

            val request = SplashRequest.toRequest(
                context = app,
                user_key = userKey,
                login_key = loginKey
            )

            mDataRepository.run {
                requestSplash(request).collect {
                    if (it.data?.email != null && it.data.email.isNotEmpty()) {
                        cacheEmail(it.data.email)
                        if (userKey.isNotEmpty() && loginKey.isNotEmpty())
                        {
                            requestAutoLogin()
                            return@collect
                        }
                    }
                    privateSplashResponseLiveData.value = it
                }
            }
        }
    }

    private fun requestAutoLogin()
    {
        viewModelScope.launch {
            loginLiveDataPrivate.value = Resource.Loading()
            mDataRepository.run {
                doLogin(
                    LoginRequest.toRequest(app, getCacheUserKey(), getCacheLoginKey(), getCacheEmail(), "", Common.getDevicePhoneNumber(app))
                ).collect { response ->
                    response.data?.let { loginResponse ->
                        run {
                            cacheNickname(loginResponse.nickname)
                            cacheLoginKey(loginResponse.login_key)
                            cacheUserKey(loginResponse.user_key)
                            getCommonPointLiveData().value = loginResponse.point.toInt()
                            cacheRefund(loginResponse.use_refund)
                            cacheUpdateKey(loginResponse.ukey)
                        }
                    }

                    val remainTime = remainMillionsLiveDataPrivate.value ?: -1
                    if (remainTime > 0) {
                        delay(remainTime)
                    }
                    loginLiveDataPrivate.value = response
                }
            }
        }
    }

    fun startTimer(timeMillions : Long)
    {
        viewModelScope.launch {
            if (timeMillions <= 0L) {
                cancel()
                return@launch
            }

            val remainTime = timeMillions - (1 * 1000)
            remainMillionsLiveDataPrivate.value = remainTime
            delay(remainTime)
            startTimer(remainTime)
        }
    }
}
