package com.hinos.rewardsample.ui.user_info

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityNewPwdBinding
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewPwdActivity : BaseActivity<ActivityNewPwdBinding, NewPwdViewModel>()
{

    override val mLayoutResID: Int = R.layout.activity_new_pwd
    override val mViewModel: NewPwdViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            nextBtnLiveData.observe(this@NewPwdActivity, ::handleNextBtnState)
            changePwdLiveData.observe(this@NewPwdActivity, ::handleErrorMsgLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvNext.setOnClickListener { mViewModel.requestChangePassword(etPwd.text.toString(), etNewPwd.text.toString(), etReNewPwd.text.toString()) }
            etPwd.addTextChangedListener {
                val pwd = etPwd.text.toString()
                tvPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(pwd)) "#4582f9" else "#e45f5f"))
                tvPasswordError.visibility = View.GONE
                checkNextBtnState()
            }
            etNewPwd.addTextChangedListener {
                tvNewPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etPwd.text.toString())) "#4582f9" else "#e45f5f"))
                tvReNewPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etNewPwd.text.toString()) && etNewPwd.text.toString() == etReNewPwd.text.toString()) "#4582f9" else "#e45f5f"))
                tvNewPasswordError.visibility = View.GONE
                tvReNewPasswordError.visibility = View.GONE
                checkNextBtnState()
            }
            etReNewPwd.addTextChangedListener {
                tvNewPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etPwd.text.toString())) "#4582f9" else "#e45f5f"))
                tvReNewPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etReNewPwd.text.toString()) && etNewPwd.text.toString() == etReNewPwd.text.toString()) "#4582f9" else "#e45f5f"))
                tvNewPasswordError.visibility = View.GONE
                tvReNewPasswordError.visibility = View.GONE
                checkNextBtnState()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
    }

    private fun handleNextBtnState(isActive : Boolean)
    {
        mBinding.tvNext.run {
            isClickable = isActive
            setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
        }
    }

    private fun handleErrorMsgLiveData(status : Resource<ResCode>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> {
                    loaderView.visibility = View.VISIBLE

//                    tvPwdHelp.setTextColor(Color.parseColor("#8c000000"))
//                    tvNewPwdHelp.setTextColor(Color.parseColor("#8c000000"))
//                    tvReNewPwdHelp.setTextColor(Color.parseColor("#8c000000"))
//
//                    tvPasswordError.visibility = View.GONE
//                    tvNewPasswordError.visibility = View.GONE
//                    tvReNewPasswordError.visibility = View.GONE
                }
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE

                    tvPwdHelp.setTextColor(Color.parseColor("#4582f9"))
                    tvNewPwdHelp.setTextColor(Color.parseColor("#4582f9"))
                    tvReNewPwdHelp.setTextColor(Color.parseColor("#4582f9"))

                    tvPasswordError.visibility = View.GONE
                    tvNewPasswordError.visibility = View.GONE
                    tvReNewPasswordError.visibility = View.GONE

                    showToast(getString(R.string.change_password_success))

                    closeActivity()
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let {
//                        tvPwdHelp.setTextColor(Color.parseColor("#8c000000"))
//                        tvNewPwdHelp.setTextColor(Color.parseColor("#8c000000"))
//                        tvReNewPwdHelp.setTextColor(Color.parseColor("#8c000000"))

                        tvPasswordError.visibility = View.GONE
                        tvNewPasswordError.visibility = View.GONE
                        tvReNewPasswordError.visibility = View.GONE

                        tvPwdHelp.setTextColor(Color.parseColor("#8c000000"))
                        when(it)
                        {
                            ErrorCase.SERVER_ERROR_PASSWORD_MISMATCH, ErrorCase.INVALID_PASSWORD_SIZE ->
                            {
                                tvPwdHelp.setTextColor(Color.parseColor("#e45f5f"))
                                tvPasswordError.visibility = View.VISIBLE
                                tvPasswordError.text = getErrorString(it)
                            }
                            ErrorCase.INVALID_NEW_CHANGE_PASSWORD_SIZE ->
                            {
                                tvNewPwdHelp.setTextColor(Color.parseColor("#e45f5f"))
                                tvNewPasswordError.visibility = View.VISIBLE
                                tvNewPasswordError.text = getErrorString(it)
                            }
                            ErrorCase.INVALID_NEW_CHANGE_PASSWORD_EQUAL ->
                            {
                                tvNewPwdHelp.setTextColor(Color.parseColor("#e45f5f"))
                                tvNewPasswordError.visibility = View.VISIBLE
                                tvNewPasswordError.text = getErrorString(it)

                                tvReNewPwdHelp.setTextColor(Color.parseColor("#e45f5f"))
                                tvReNewPasswordError.visibility = View.VISIBLE
                                tvReNewPasswordError.text = getErrorString(it)
                            }
                            else ->
                            {
                                showToast(getErrorString(it))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun checkNextBtnState()
    {
        mBinding.run {
            mViewModel.isWrapValue(etPwd.text.toString(), etNewPwd.text.toString(), etReNewPwd.text.toString())
        }
    }
}