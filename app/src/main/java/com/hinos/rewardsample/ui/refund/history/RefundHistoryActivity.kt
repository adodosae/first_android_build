package com.hinos.rewardsample.ui.refund.history

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.RefundHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityRefundHistoryBinding
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailViewModel
import com.hinos.rewardsample.ui.refund.adapter.RefundHistoryAdapter
import com.hinos.rewardsample.ui.refund.detail.RefundDetailActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RefundHistoryActivity : BaseActivity<ActivityRefundHistoryBinding, RefundHistoryViewModel>()
{

    override val mLayoutResID: Int = R.layout.activity_refund_history
    override val mViewModel: RefundHistoryViewModel by viewModels()
    private val mAdtRefundHistory : RefundHistoryAdapter by lazy { RefundHistoryAdapter(mViewModel) }

    override fun observeViewModel()
    {
        mViewModel.run {
            refundHistoryLiveData.observe(this@RefundHistoryActivity, ::handleRefundHistoryLiveData)
            clickEvtRefundHistory.observe(this@RefundHistoryActivity, ::handleClickEvtRefundHistory)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    private fun initRecyclerView()
    {
        mBinding.recyclerView.run {
            layoutManager = LinearLayoutManager(this@RefundHistoryActivity)
            adapter = mAdtRefundHistory
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initRecyclerView()
        mViewModel.requestRefundHistory()
    }

    private fun handleRefundHistoryLiveData(status : Resource<RefundHistoryResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    mAdtRefundHistory.setNewData(it.historyItems)
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleClickEvtRefundHistory(data : RefundHistoryResponse.Data)
    {
        val i = Intent(this, RefundDetailActivity::class.java).apply {
            putExtra(PurchaseDetailViewModel.EXTRA_DETAIL_SEQ, data.seq)
            putExtra(PurchaseDetailViewModel.EXTRA_DETAIL_TYPE, "3")
        }
        startActivity(i)
    }
}