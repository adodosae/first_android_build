package com.hinos.rewardsample.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * ViewPager 안에 photoView를 넣고 사진을 확대하면 java.lang.IllegalArgumentException: pointerIndex out of range 에러 발생.
 * 터치 이벤트 예외 처리
 */

class XwViewPager : ViewPager
{
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean
    {
        try {
            return super.onInterceptTouchEvent(ev)
        }catch (e : Exception)
        {
            return false
        }
    }
}