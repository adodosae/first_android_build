package com.hinos.rewardsample.ui.refund.detail

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.PurchaseDetailResponse
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityRefundDetailBinding
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailViewModel
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RefundDetailActivity : BaseActivity<ActivityRefundDetailBinding, PurchaseDetailViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_refund_detail
    override val mViewModel: PurchaseDetailViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            purchaseDetailLiveData.observe(this@RefundDetailActivity, ::handlePurchaseDetailLiveData)
            refundCancelLiveData.observe(this@RefundDetailActivity, ::handleRefundCancelLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvCancel.setOnClickListener { openBottomSheet(BottomSheet.REQUEST_REFUND_POINT_CANCEL) }
        }
    }

    private fun initDetail()
    {
        val seq = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_SEQ) ?: ""
        val type = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_TYPE) ?: ""

        if (seq.isEmpty() || type.isEmpty())
            closeActivity()

        mViewModel.requestDetail(seq, type)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initDetail()
    }

    private fun handlePurchaseDetailLiveData(status : Resource<PurchaseDetailResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { data ->
                    loaderView.visibility = View.GONE
                    tvRequestDate.text = data.create_date?.let { it.split(" ")[0].replace("-", ".") }
                    tvSendDate.text = data.send_date?.let { it.split(" ")[0].replace("-", ".") }

                    tvBank.text = data.bank_name
                    tvAccount.text = data.account_num
                    tvAccountName.text = data.account_name
                    tvPoint.text = Common.toPointFormat(data.refund_amount, false)
                    tvEtc.text = data.refund_memo.replace("\\n", System.getProperty("line.separator"))

                    if (data.detail_type == "1") // 기프티쇼
                    {
                        stickBarWait.active(data.run_state == "1")
                        tvWait.setTextColor(Color.parseColor(if (data.run_state == "1") "#4582f9" else "#ced4da"))

                        tvCancel.visibility = if (data.run_state == "1") View.VISIBLE else View.GONE

                        stickBarFail.active(data.run_state == "2" || data.run_state == "5", true)
                        tvFail.setTextColor(Color.parseColor(if (data.run_state == "2" || data.run_state == "5") "#e45f5f" else "#ced4da"))

                        stickBarComplete.active(data.run_state == "3")
                        tvComplete.setTextColor(Color.parseColor(if (data.run_state == "3") "#4582f9" else "#ced4da"))
                    }
                    else if (data.detail_type == "2") // 카카오
                    {
                        stickBarWait.active(data.run_state == "1")
                        tvWait.setTextColor(Color.parseColor(if (data.run_state == "1") "#4582f9" else "#ced4da"))

                        tvCancel.visibility = if (data.run_state == "1") View.VISIBLE else View.GONE

                        stickBarFail.active(data.run_state == "2" || data.run_state == "5", true)
                        tvFail.setTextColor(Color.parseColor(if (data.run_state == "2" || data.run_state == "5") "#e45f5f" else "#ced4da"))

                        stickBarComplete.active(data.run_state == "3")
                        tvComplete.setTextColor(Color.parseColor(if (data.run_state == "3") "#4582f9" else "#ced4da"))
                    }
                    else if (data.detail_type == "3") // 환급
                    {
                        stickBarWait.active(data.run_state == "1" || data.run_state == "2")
                        tvWait.setTextColor(Color.parseColor(if (data.run_state == "1" || data.run_state == "2") "#4582f9" else "#ced4da"))

                        tvCancel.visibility = if (data.run_state == "1") View.VISIBLE else View.GONE

                        stickBarFail.active(data.run_state == "4", true)
                        tvFail.setTextColor(Color.parseColor(if (data.run_state == "4") "#e45f5f" else "#ced4da"))

                        stickBarComplete.active(data.run_state == "3")
                        tvComplete.setTextColor(Color.parseColor(if (data.run_state == "3") "#4582f9" else "#ced4da"))
                    }
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleRefundCancelLiveData(status : Resource<PurchaseResponse>) {
        mBinding.run {
            when (status) {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { _ ->
                    loaderView.visibility = View.GONE
                    showToast(getString(R.string.detail_refund_cancel_text))
                    closeActivity()
                }
                is Resource.DataError -> {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it) }
                }
            }
        }
    }

    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.run {
            setOnDialogResult(object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    dismiss()
                    if (nResult == BaseDialog.RES_OK)
                    {
                        if (sheet == BottomSheet.REQUEST_REFUND_POINT_CANCEL) {
                            mViewModel.cancelRefund()
                        }
                    }
                }
            })
            showDialog(sheet, params)
        }
    }

}