package com.hinos.rewardsample.ui.refund.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.response.RefundHistoryResponse
import com.hinos.rewardsample.databinding.ItemRefundHistoryBinding
import com.hinos.rewardsample.ui.refund.history.RefundHistoryViewModel
import com.hinos.rewardsample.util.Common

class RefundHistoryAdapter(private val refundHistoryViewModel : RefundHistoryViewModel) : RecyclerView.Adapter<RefundHistoryAdapter.RefundHistoryHolder>()
{
    private var mItems : MutableList<RefundHistoryResponse.Data> = mutableListOf()

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            refundHistoryViewModel.openRefundHistoryDetail(item as RefundHistoryResponse.Data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RefundHistoryHolder {
        val itemBinding = ItemRefundHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RefundHistoryHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: RefundHistoryHolder, position: Int) {
        holder.bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int = mItems.size

    fun setNewData(newItems : List<RefundHistoryResponse.Data>)
    {
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

    class RefundHistoryHolder(private val itemBinding : ItemRefundHistoryBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : RefundHistoryResponse.Data, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                ivDetail.setOnClickListener { recyclerItemListener.onItemSelected(item) }
                tvDate.text = item.refund_date.split(" ")[0].replace("-",".")
                tvBank.text = item.bank_name
                tvPoint.text = Common.toCashFormat(item.refund_price, root.context.getString(R.string.cash_suffix))
            }
        }
    }
}