package com.hinos.rewardsample.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.hinos.rewardsample.R
import com.hinos.rewardsample.databinding.FragmentZoomBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ZoomFragment(val strUrl : String) : Fragment()
{
    companion object
    {
        fun newInstance(_strUrl: String) : ZoomFragment
        {
            return ZoomFragment(_strUrl)
        }
    }

    private lateinit var m_binding : FragmentZoomBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        m_binding = DataBindingUtil.inflate(inflater, R.layout.fragment_zoom, container, false)
        Glide.with(context!!).load(strUrl).into(m_binding.ivPictureContent)
        return m_binding.root
    }
}