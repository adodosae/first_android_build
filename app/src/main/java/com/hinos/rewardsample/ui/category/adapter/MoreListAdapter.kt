package com.hinos.rewardsample.ui.category.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.databinding.ItemMoreBinding

abstract class MoreListAdapter(V : BaseViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    val LIST_NORMAL : Int = 0
    val LIST_MORE : Int = 1

    var mShowMore = false

    fun loading()
    {
        if (mShowMore)
            return

        mShowMore = true
        notifyItemInserted(itemCount)
    }

    fun hideMore()
    {
        if (mShowMore)
        {
            notifyItemRemoved(itemCount)
            mShowMore = false
        }
    }

    inner class MoreViewBinding(itemBinding : ItemMoreBinding) : RecyclerView.ViewHolder(itemBinding.root)

}