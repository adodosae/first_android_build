package com.hinos.rewardsample.ui.center.adapter


import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.hinos.rewardsample.R

import com.hinos.rewardsample.ui.fragment.QnaListFragment
import javax.inject.Inject

class QnaViewPagerAdapter @Inject constructor(mgr : FragmentManager, private val mQnaFragments : List<QnaListFragment>) : FragmentPagerAdapter(mgr)
{
    override fun getItem(position: Int): Fragment
    {
        return mQnaFragments[position]
    }

    override fun getCount(): Int
    {
        return mQnaFragments.size
    }

    override fun getPageTitle(position: Int): CharSequence
    {
        return mQnaFragments[position].mPageName
    }


    private val mTabs = mutableListOf<TextView>()
    private val mIndicators = mutableListOf<View>()


    fun setUpWithCustomTabLayout(position: Int, viewPager : ViewPager, clTab : ViewGroup)
    {
        viewPager.addOnPageChangeListener(mOnChangeListener)

        val tvTab1 = clTab.findViewById<TextView>(R.id.tvTab1)
        val tvTab2 = clTab.findViewById<TextView>(R.id.tvTab2)
        val tvTab3 = clTab.findViewById<TextView>(R.id.tvTab3)

        mTabs.add(tvTab1)
        mTabs.add(tvTab2)
        mTabs.add(tvTab3)

        val vIndicate1 = clTab.findViewById<View>(R.id.vIndicator1)
        val vIndicate2 = clTab.findViewById<View>(R.id.vIndicator2)
        val vIndicate3 = clTab.findViewById<View>(R.id.vIndicator3)

        mIndicators.add(vIndicate1)
        mIndicators.add(vIndicate2)
        mIndicators.add(vIndicate3)

        mTabs.forEachIndexed { index, textView ->
            textView.setOnClickListener {
                viewPager.setCurrentItem(index, true)
            }
        }

        viewPager.currentItem = position
        mOnChangeListener.onPageSelected(position)
    }

    fun selectTab(position : Int)
    {
        for (i in mTabs.indices)
        {
            mTabs[i].setTextColor(Color.parseColor(if (position == i) "#4582f9" else "#ced4da"))
        }

        for (i in mIndicators.indices)
        {
            mIndicators[i].setBackgroundColor(Color.parseColor(if (position == i) "#4582f9" else "#ced4da"))
        }
    }

    private val mOnChangeListener = object : ViewPager.OnPageChangeListener
    {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {

        }

        override fun onPageSelected(position: Int)
        {
            selectTab(position)
        }

        override fun onPageScrollStateChanged(state: Int)
        {

        }
    }
}