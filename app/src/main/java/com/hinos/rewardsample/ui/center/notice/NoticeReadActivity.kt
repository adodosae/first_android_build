package com.hinos.rewardsample.ui.center.notice

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.NoticeReadResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityNoticeReadBinding
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NoticeReadActivity : BaseActivity<ActivityNoticeReadBinding, NoticeViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_notice_read
    override val mViewModel: NoticeViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.noticeReadLiveData.observe(this, ::handleNoticeReadLiveData)
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initDetail()
    }

    private fun initDetail()
    {
        val bdName = intent.getStringExtra(NoticeViewModel.EXTRA_NOTICE_DETAIL_TYPE) ?: ""
        val seq = intent.getStringExtra(NoticeViewModel.EXTRA_NOTICE_DETAIL_SEQ) ?: ""

        if (seq.isEmpty() || bdName.isEmpty())
        {
            closeActivity()
            return
        }

        mViewModel.requestNoticeDetail(
            bdName, seq
        )

        mBinding.run {
            tvName.setText(if (bdName == "NOTICE") R.string.center_notice else R.string.center_faq)
        }
    }

    private fun handleNoticeReadLiveData(status : Resource<NoticeReadResponse>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { data ->
                    tvTitle.text = data.title
                    tvDate.text = data.wr_date
                    tvContents.text = data.contents

                }
                is Resource.DataError ->
                {
                    val errorMsg = getErrorString(status.errorCode ?: 0)
                    showToast(errorMsg)
                }
            }
        }
    }
}