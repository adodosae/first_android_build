package com.hinos.rewardsample.ui.center.notice

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.NoticeListResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityNoticeListBinding
import com.hinos.rewardsample.di.ActivityModule
import com.hinos.rewardsample.ui.center.adapter.NoticeViewPagerAdapter
import com.hinos.rewardsample.ui.fragment.NoticeListFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NoticeListActivity : BaseActivity<ActivityNoticeListBinding, NoticeViewModel>()
{
    companion object
    {
        const val EXTRA_LIST_TYPE = "EXTRA_LIST_TYPE"
        const val LIST_NOTICE_TYPE = 0 // 공지사항
        const val LIST_FAQ_TYPE = 1 // FAQ
    }

    override val mLayoutResID: Int = R.layout.activity_notice_list
    override val mViewModel: NoticeViewModel by viewModels()

    private var mCurrentPosition : Int = 0

    private lateinit var mAdtPager : NoticeViewPagerAdapter

    @Inject
    lateinit var mFragments : List<NoticeListFragment>

    override fun observeViewModel()
    {
        mViewModel.run {
//            noticeListLiveData.observe(this@NoticeListActivity, ::handleNoticeList)
//            faqListLiveData.observe(this@NoticeListActivity, ::handleFAQList)
            noticeListResponseLiveData.observe(this@NoticeListActivity, ::handleResponseResult)
            clickEvtNoticeDetail.observe(this@NoticeListActivity, ::handleClickEvt)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }

        }
    }

    private fun initViewPager()
    {
        mBinding.run {
            mAdtPager = NoticeViewPagerAdapter(this@NoticeListActivity.supportFragmentManager, mFragments)
            viewPager.adapter = mAdtPager
            mAdtPager.setUpWithCustomTabLayout(mCurrentPosition, viewPager, llTab)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initViewSetting()
    }

    private fun initViewSetting()
    {
        mCurrentPosition = intent.getIntExtra(EXTRA_LIST_TYPE, 0)
        initViewPager()
    }

    private fun handleResponseResult(status : Resource<Int>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleClickEvt(data : NoticeListResponse.Data)
    {
        val i = Intent(this, NoticeReadActivity::class.java).apply {
            putExtra(NoticeViewModel.EXTRA_NOTICE_DETAIL_SEQ, data.seq)
            putExtra(NoticeViewModel.EXTRA_NOTICE_DETAIL_TYPE, data.bd_name)
        }
        startActivity(i)
    }
}