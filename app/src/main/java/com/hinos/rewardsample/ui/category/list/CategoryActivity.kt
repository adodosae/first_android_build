package com.hinos.rewardsample.ui.category.list

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityCategoryBinding
import com.hinos.rewardsample.ui.category.adapter.CategoryListAdapter
import com.hinos.rewardsample.ui.category.detail.CategoryDetailActivity
import com.hinos.rewardsample.ui.category.search.CategorySearchActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CategoryActivity : BaseActivity<ActivityCategoryBinding, CategoryViewModel>()
{
    companion object
    {
        const val EXTRA_CATEGORY_DATA : String = "EXTRA_CATEGORY_DATA"
    }

    override fun observeViewModel()
    {
        mViewModel.run {
            storeItemLiveData.observe(this@CategoryActivity, ::handleStoreItemLiveData)
            categoryLiveData.observe(this@CategoryActivity, ::handleCategoryResponseLiveData)
            clickEvtCategory.observe(this@CategoryActivity, ::handleClickListEvent)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            ivSearch.setOnClickListener { openActivity(CategorySearchActivity::class.java) }
        }
    }

    private fun initRecyclerView()
    {
        mBinding.recyclerView.run {
            val mgrLayoutManager = LinearLayoutManager(this@CategoryActivity)
            layoutManager = mgrLayoutManager
            adapter = mAdtCategoryList


            addOnScrollListener(object : RecyclerView.OnScrollListener()
            {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
                {
                    if (!mAdtCategoryList.mShowMore)
                    {
                        val lastVisibleItemPosition = mgrLayoutManager.findLastCompletelyVisibleItemPosition()
                        val itemTotalCount = mAdtCategoryList.itemCount -2

                        if (lastVisibleItemPosition >= itemTotalCount)
                        {
                            mViewModel.requestCategory()
                        }
                    }
                }
            })
        }
    }

    override val mViewModel : CategoryViewModel by viewModels()
    override val mLayoutResID: Int = R.layout.activity_category
    private val mAdtCategoryList : CategoryListAdapter by lazy { CategoryListAdapter(mViewModel) }


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initRecyclerView()
        initCategory()
    }

    private fun initCategory()
    {
        val data = intent.getParcelableExtra<StoreResponse.Data?>(EXTRA_CATEGORY_DATA)
        mViewModel.initCategory(data)
    }

    private fun handleStoreItemLiveData(data : StoreResponse.Data?)
    {
        data ?: return
        mBinding.tvBarTitle.text = data.cate_name
    }

    private fun handleCategoryResponseLiveData(status : Resource<CategoryResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> mAdtCategoryList.loading()
                is Resource.Success -> status.data?.let {
                    mAdtCategoryList.addNewData(it.categoryList)
                    mAdtCategoryList.hideMore()
                }
                is Resource.DataError ->
                {
                    mAdtCategoryList.hideMore()
                }
            }
        }
    }

    private fun handleClickListEvent(data : CategoryResponse.Data?)
    {
        data ?: return

        val i = Intent(this, CategoryDetailActivity::class.java).apply {
            putExtra(CategoryDetailActivity.EXTRA_CATEGORY_DETAIL_DATA, data)
        }
        openActivity(i)
    }

}