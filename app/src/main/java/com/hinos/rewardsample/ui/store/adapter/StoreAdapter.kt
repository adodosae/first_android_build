package com.hinos.rewardsample.ui.store.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.databinding.ItemStoreBinding
import com.hinos.rewardsample.ui.store.StoreViewModel

class StoreAdapter(private val storeViewModel : StoreViewModel) : RecyclerView.Adapter<StoreAdapter.StoreViewHolder>()
{
    private var mItems : MutableList<StoreResponse.Data> = mutableListOf()

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            storeViewModel.openCategoryList(item as StoreResponse.Data)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        val itemBinding = ItemStoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StoreViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        holder.bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int = mItems.size

    fun setNewData(newItems : List<StoreResponse.Data>)
    {
        mItems.clear()
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

    class StoreViewHolder(private val itemBinding : ItemStoreBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : StoreResponse.Data, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                Glide.with(ivIcon.context).load(item.cate_thumb).into(ivIcon)
                vgRoot.setOnClickListener { recyclerItemListener.onItemSelected(item) }
                tvName.text = item.cate_name
            }
        }
    }
}