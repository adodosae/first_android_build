package com.hinos.rewardsample.ui.login

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.request.FindEmailRequest
import com.hinos.rewardsample.data.dto.request.FindPwdRequest
import com.hinos.rewardsample.data.dto.request.LoginRequest
import com.hinos.rewardsample.data.dto.response.FindEmailResponse
import com.hinos.rewardsample.data.dto.response.FindPwdResponse
import com.hinos.rewardsample.data.dto.response.LoginResponse
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val loginLiveDataPrivate = MutableLiveData<Resource<LoginResponse>>()
    val loginLiveData: LiveData<Resource<LoginResponse>> get() = loginLiveDataPrivate


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val humanChangeStateLiveDataPrivate = MutableLiveData<Resource<ResCode>>()
    val humanChangeStateLiveData: LiveData<Resource<ResCode>> get() = humanChangeStateLiveDataPrivate

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val findEmailLiveDataPrivate = MutableLiveData<Resource<FindEmailResponse>>()
    val findEmailLiveData: LiveData<Resource<FindEmailResponse>> get() = findEmailLiveDataPrivate

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val findPwdLiveDataPrivate = MutableLiveData<Resource<FindPwdResponse>>()
    val findPwdLiveData: LiveData<Resource<FindPwdResponse>> get() = findPwdLiveDataPrivate





    fun requestLogin(email: String, pwd : String)
    {
        val phoneNumber = Common.getDevicePhoneNumber(getApplication())
        val resultCode = isValidValue(email, pwd, phoneNumber)
        if (resultCode != 0) {
            loginLiveDataPrivate.value = Resource.DataError(resultCode)
            return
        }

        viewModelScope.launch {
            loginLiveDataPrivate.value = Resource.Loading()
            mDataRepository.doLogin(
                    LoginRequest.toRequest(
                            context = getApplication(),
                            user_key = "",
                            login_key = "",
                            email = email,
                            pwd = pwd,
                            phone_num = phoneNumber
                    )
            ).collect {
                it.data?.let { loginResponse ->
                    mDataRepository.run {
                        cacheEmail(email)
                        cacheNickname(loginResponse.nickname)
                        cacheLoginKey(loginResponse.login_key)
                        cacheUserKey(loginResponse.user_key)
                        getCommonPointLiveData().value = loginResponse.point.toInt()
                        cacheRefund(loginResponse.use_refund)
                        cacheUpdateKey(loginResponse.ukey)
                    }
                }
                loginLiveDataPrivate.value = it
            }
        }
    }

    fun requestChangeHumanState()
    {
        viewModelScope.launch {

            humanChangeStateLiveDataPrivate.value = Resource.Loading()

            val request = BaseRequest.toRequest(
                app,
                mDataRepository.getCacheUserKey()
            )

            mDataRepository.requestChangeHumanState(request).collect {
                humanChangeStateLiveDataPrivate.value = it
            }
        }
    }

    fun requestFindEmail(phoneNumber: String)
    {
        viewModelScope.launch {
            val isValidPhoneNumber = ValueChecker.isValidPhoneNum(phoneNumber)
            if (!isValidPhoneNumber) {
                findEmailLiveDataPrivate.value = Resource.DataError(ErrorCase.INVALID_PASSWORD_SIZE)
                return@launch
            }

            findEmailLiveDataPrivate.value = Resource.Loading()

            mDataRepository.findEmail(
                FindEmailRequest.toRequest(
                    context = getApplication(),
                    user_key = mDataRepository.getCacheUserKey(),
                    phone_num = phoneNumber
                )
            ).collect {
                findEmailLiveDataPrivate.value = it
            }
        }
    }

    fun requestFindPwd(email : String)
    {
        val phoneNumber = Common.getDevicePhoneNumber(getApplication())
        val resultCode = isValidValue(email, phoneNumber)
        if (resultCode != 0) {
            loginLiveDataPrivate.value = Resource.DataError(resultCode)
            return
        }

        viewModelScope.launch {
            loginLiveDataPrivate.value = Resource.Loading()
            mDataRepository.findPwd(
                FindPwdRequest.toRequest(
                    context = getApplication(),
                    user_key = mDataRepository.getCacheUserKey(),
                    email = email,
                    phone_num = phoneNumber
                )
            ).collect {
                findPwdLiveDataPrivate.value = it
            }
        }
    }

    private fun isValidValue(email : String,
                             pwd : String,
                             phoneNumber : String) : Int
    {
        val isValidPhoneNumber = ValueChecker.isValidPhoneNum(phoneNumber)
        val isValidEmail = ValueChecker.isValidEmail(email)
        val isValidPwd = ValueChecker.isValidPassword(pwd)

        if (!isValidPhoneNumber) {
            // 핸드폰 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_PHONE_LENGTH
        }

        if (!isValidEmail) {
            // 이메일 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_EMAIL_FORM
        }

        if (!isValidPwd) {
            // 비밀번호 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_PASSWORD_SIZE
        }
        return 0
    }

    private fun isValidValue(email : String,
                             phoneNumber : String) : Int
    {
        val isValidPhoneNumber = ValueChecker.isValidPhoneNum(phoneNumber)
        val isValidEmail = ValueChecker.isValidEmail(email)

        if (!isValidPhoneNumber) {
            // 핸드폰 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_PHONE_LENGTH
        }

        if (!isValidEmail) {
            // 이메일 형식 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_EMAIL_FORM
        }
        return 0
    }
}