package com.hinos.rewardsample.ui.imoge.detail

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.PurchaseDetailResponse
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityImogeDetailBinding
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailViewModel
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImogeDetailActivity : BaseActivity<ActivityImogeDetailBinding, PurchaseDetailViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_imoge_detail
    override val mViewModel: PurchaseDetailViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            purchaseDetailLiveData.observe(this@ImogeDetailActivity, ::handlePurchaseDetailLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    private fun initDetail()
    {
        val seq = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_SEQ) ?: ""
        val type = intent.getStringExtra(PurchaseDetailViewModel.EXTRA_DETAIL_TYPE) ?: ""

        if (seq.isEmpty() || type.isEmpty())
            closeActivity()

        mViewModel.requestDetail(seq, type)
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initDetail()
    }

    private fun handlePurchaseDetailLiveData(status : Resource<PurchaseDetailResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { data ->
                    loaderView.visibility = View.GONE
                    tvRequestDate.text = data.create_date?.let { it.split(" ")[0].replace("-", ".") }
                    tvSendDate.text = data.send_date?.let { it.split(" ")[0].replace("-", ".") }
                    tvEtc.text = data.payment_memo?.replace("\\n", System.getProperty("line.separator"))
                    tvPhone.text = data.send_phone_num
                    tvPoint.text = Common.toPointFormat(data.buy_point, false)+"("+data.buy_choco+getString(R.string.detail_imoge_point)+")"

                    stickBarWait.active(data.run_state == "1")
                    tvWait.setTextColor(Color.parseColor(if (data.run_state == "1") "#4582f9" else "#ced4da"))

                    stickBarFail.active(data.run_state == "2", true)
                    tvFail.setTextColor(Color.parseColor(if (data.run_state == "2") "#e45f5f" else "#ced4da"))

                    stickBarComplete.active(data.run_state == "3")
                    tvComplete.setTextColor(Color.parseColor(if (data.run_state == "3") "#4582f9" else "#ced4da"))
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

}