package com.hinos.rewardsample.ui.point.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.data.dto.response.PointHistoryResponse

import com.hinos.rewardsample.databinding.ItemPointHistoryBinding
import com.hinos.rewardsample.databinding.ItemPointHistoryMyBinding
import com.hinos.rewardsample.ui.point.history.PointHistoryViewModel
import com.hinos.rewardsample.util.Common

class PointHistoryAdapter(private val historyViewModel : PointHistoryViewModel) : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{
    private var mPoint : String = "0"
    private val mItems : ArrayList<PointHistoryResponse.Data> = ArrayList()

    override fun getItemViewType(position: Int): Int
    {
        return if (position == 0)
            1
         else
            2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder
    {
        if (viewType == 1)
            return InfoViewHolder(ItemPointHistoryMyBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        return PointViewHolder(ItemPointHistoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
    {
        if (position == 0) {
            val infoViewHolder = holder as InfoViewHolder
            infoViewHolder.bind()
        } else {
            val pointViewHOlder = holder as PointViewHolder
            pointViewHOlder.bind(position -1)
        }
    }

    override fun getItemCount(): Int = mItems.size + 1

    fun setNewData(newPoint : String, newItems : List<PointHistoryResponse.Data>)
    {
        mPoint = newPoint
        mItems.clear()
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

    fun setNewData(newPoint : String)
    {
        mPoint = newPoint
        notifyItemChanged(0)
    }

    inner class PointViewHolder(private val itemBinding : ItemPointHistoryBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(position : Int) {
            itemBinding.run {
                val item = mItems[position]
                tvPoint.text = Common.toPointFormat(item.use_point)
                if (item.use_point.contains("-"))
                    tvPoint.setTextColor(Color.parseColor("#b3000000"))
                else
                    tvPoint.setTextColor(Color.parseColor("#4582f9"))

                tvDate.text = item.point_date.replace("-", ".")
                tvTitle.text = item.product_name
                tvPointType.text = item.point_type
            }
        }
    }

    inner class InfoViewHolder(private val itemBinding : ItemPointHistoryMyBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind() {
            itemBinding.run {
                tvPoint.text = Common.toPointFormat(mPoint)
            }
        }
    }
}