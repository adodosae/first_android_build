package com.hinos.rewardsample.ui.main

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    companion object
    {
        const val BACK_FLAG_REVIEW = 1
        const val BACK_FLAG_EXIT = 2
    }

    val backPressFlagLiveData : MutableLiveData<Int> = MutableLiveData()
    val userNameLiveData : MutableLiveData<String> = MutableLiveData()
    val inviteUrlLiveData : MutableLiveData<String> = MutableLiveData()
    val refundUseLiveData : MutableLiveData<String> = MutableLiveData()

    init
    {
        getCacheUserName()
        getCacheInviteUrl()
        getCacheRefundUse()
    }

    private fun getCacheUserName()
    {
        viewModelScope.launch {
            userNameLiveData.value = mDataRepository.getCacheNickname()
        }
    }


    private fun getCacheInviteUrl()
    {
        viewModelScope.launch {
            inviteUrlLiveData.value = mDataRepository.getCacheInviteUrl()
        }
    }

    private fun getCacheRefundUse()
    {
        viewModelScope.launch {
            refundUseLiveData.value = mDataRepository.getCacheRefund()
        }
    }

    fun cacheReviewLastPopupTime(millions : Long)
    {
        viewModelScope.launch {
            mDataRepository.cacheReviewPopupTime(millions)
        }
    }

    fun onBackPress()
    {
        viewModelScope.launch {
            val reviewOpenTime = mDataRepository.getCacheReviewPopupTime()
            if (reviewOpenTime != -1L && Common.isExpired(reviewOpenTime, AppConfig.RECOMMAND_ITV))
                backPressFlagLiveData.value = BACK_FLAG_REVIEW // 리뷰 다이얼로그
             else
                backPressFlagLiveData.value = BACK_FLAG_EXIT // 종료 다이얼로그
        }
    }
}
