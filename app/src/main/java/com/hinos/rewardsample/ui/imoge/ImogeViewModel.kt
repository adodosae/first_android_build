package com.hinos.rewardsample.ui.imoge

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.ImogeRequest
import com.hinos.rewardsample.data.dto.response.*
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImogeViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privatePurchaseBtnFlag = MutableLiveData<Boolean>(false)
    val purchaseBtnFlag: LiveData<Boolean> get() = privatePurchaseBtnFlag

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privatePurchaseResponseLiveData = MutableLiveData<Resource<PurchaseResponse>>()
    val purchaseResponseLiveData: LiveData<Resource<PurchaseResponse>> get() = privatePurchaseResponseLiveData

    val kakaoHelpLiveData : MutableLiveData<String> = MutableLiveData()

    init {
        viewModelScope.launch {
            kakaoHelpLiveData.value = mDataRepository.getCacheKakaoHelp()
        }
    }

    fun isValidValue(link : String, selectBtn : Int)
    {
        viewModelScope.launch {
            privatePurchaseBtnFlag.value = link.isNotEmpty() && selectBtn >= 0
        }
    }

    fun requestImoge(buy_point : String, buy_choco : String, buy_url : String, send_type : String, send_phone_num : String)
    {
        viewModelScope.launch {
            if (send_type == "2") // 선물
            {
                val isValidPhoneNumLength = ValueChecker.isValidPhoneNumLength(send_phone_num)
                if (!isValidPhoneNumLength) {
                    privatePurchaseResponseLiveData.value = Resource.DataError(ErrorCase.INVALID_PHONE_LENGTH)
                    return@launch
                }
            }

            if (buy_point.toInt() > getCommonPointLiveData().value!!)
            {
                privatePurchaseResponseLiveData.value = Resource.DataError(ErrorCase.SERVER_ERROR_LESS_POINT)
                return@launch
            }

            if (!buy_url.contains("http"))
            {
                privatePurchaseResponseLiveData.value = Resource.DataError(ErrorCase.SERVER_ERROR_INVALID_LINK)
                return@launch
            }

            val request = ImogeRequest.toRequest(
                context = app,
                user_key = mDataRepository.getCacheUserKey(),
                email = mDataRepository.getCacheEmail(),
                nickname = mDataRepository.getCacheNickname(),
                buy_point = buy_point,
                buy_choco = buy_choco,
                buy_url = buy_url,
                send_type = send_type,
                send_phone_num = send_phone_num
            )

            mDataRepository.requestPurchaseImoge(request).collect {
                val userPoint = it.data?.user_point
                if (!userPoint.isNullOrEmpty())
                    getCommonPointLiveData().value = userPoint.toInt()
                privatePurchaseResponseLiveData.value = it
            }
        }
    }

}
