package com.hinos.rewardsample.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class IconView extends ImageView
{
    private static final String TAG = IconView.class.getSimpleName();
    private int m_nViewWidth = 0;
    private int m_nViewHeight = 0;
    private Paint m_pnt = new Paint();

    public IconView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public IconView(final Context context, final AttributeSet attrs,
                    final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int width, int height)
    {
        super.onMeasure(width, height);
        int w = getMeasuredWidth();
        int h = getMeasuredHeight();
        int min = Math.min(w, h);

        setMeasuredDimension(min, min);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);
        m_nViewWidth	= w;
        m_nViewHeight = h;

    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
    }
    protected void onDraw2(Canvas canvas)
    {
        Drawable drawable = getDrawable();

        RectF rcBound = new RectF();

        rcBound.left		= 0 + getPaddingLeft();
        rcBound.right		= m_nViewWidth - getPaddingRight();
        rcBound.top			= 0 + getPaddingTop();
        rcBound.bottom		= m_nViewHeight - getPaddingBottom();


        if(drawable == null)
        {
            return;
        }

        m_pnt.setColor(Color.rgb(255, 255, 0));
        m_pnt.setStrokeWidth(5);

        //canvas.drawRect(0, 0, m_nViewWidth, m_nViewHeight, m_pnt);
        canvas.drawLine(rcBound.left, rcBound.top, rcBound.right, rcBound.top, m_pnt);
        canvas.drawLine(rcBound.right, rcBound.top, rcBound.right, rcBound.bottom, m_pnt);
        canvas.drawLine(rcBound.right, rcBound.bottom, rcBound.left, rcBound.bottom, m_pnt);
        canvas.drawLine(rcBound.left, rcBound.bottom, rcBound.left, rcBound.top, m_pnt);


        return;
/*
		super.onDraw(canvas);
		System.out.println("Painting content");
		Paint paint  = new Paint(Paint.LINEAR_TEXT_FLAG);
		paint.setColor(0x0);
		paint.setTextSize(12.0F);
		System.out.println("Drawing text");
		canvas.drawText("Hello World in custom view", 0, 0, paint);
*/
    }

}
