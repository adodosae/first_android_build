package com.hinos.rewardsample.ui.sign.out

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.SignOutRequest
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class SignOutViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
//    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
//    private val  currentPwdPrivate = MutableLiveData<String>()
//    val currentPwdLiveData: LiveData<String> get() = currentPwdPrivate


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  signOutResponseResultPrivate = MutableLiveData<Resource<ResCode>>()
    val signOutResponseResult : LiveData<Resource<ResCode>> get() = signOutResponseResultPrivate


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  nextBtnPrivate = MutableLiveData<Boolean>(false)
    val nextBtnState : LiveData<Boolean> get() = nextBtnPrivate


    fun signOutUser(pwd : String)
    {
        viewModelScope.launch {
            val email = mDataRepository.getCacheEmail()
            val userKey = mDataRepository.getCacheUserKey()
            val result = isValidValue(pwd)
            if (result != 0)
            {
                signOutResponseResultPrivate.value = Resource.DataError(result)
                return@launch
            }

            if (!ValueChecker.isValidEmail(email)) {
                signOutResponseResultPrivate.value = Resource.DataError(ErrorCase.INVALID_EMAIL_FORM)
                return@launch
            }

            signOutResponseResultPrivate.value = Resource.Loading()

            mDataRepository.signOutUser(
                SignOutRequest.toRequest(
                    app,
                    userKey,
                    email,
                    pwd
                )
            ).collect {
                if (it.data?.res_code == "0") {
                    mDataRepository.clearAllUserCache()
                    getCommonPointLiveData().value = 0
                }
                signOutResponseResultPrivate.value = it
            }
        }
    }

    private fun isValidValue(pwd : String) : Int
    {        if (!ValueChecker.isValidPassword(pwd)) {
        return ErrorCase.INVALID_PASSWORD_SIZE
    }
        return 0

    }

    fun isWrapValue(pwd : String)
    {
        nextBtnPrivate.value = isValidValue(pwd) == 0
    }
}
