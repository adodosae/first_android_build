package com.hinos.rewardsample.ui.purchase.detail

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.PurchaseDetailRequest
import com.hinos.rewardsample.data.dto.request.RefundCancelRequest
import com.hinos.rewardsample.data.dto.response.PurchaseDetailResponse
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PurchaseDetailViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    companion object
    {
        const val EXTRA_DETAIL_SEQ = "EXTRA_DETAIL_SEQ"
        const val EXTRA_DETAIL_TYPE = "EXTRA_DETAIL_TYPE"
        const val EXTRA_DETAIL_BRAND = "EXTRA_DETAIL_BRAND"
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val privatePurchaseDetailLiveData: MutableLiveData<Resource<PurchaseDetailResponse>> = MutableLiveData()
    val purchaseDetailLiveData: LiveData<Resource<PurchaseDetailResponse>> = privatePurchaseDetailLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val privateRefundCancelLiveData: MutableLiveData<Resource<PurchaseResponse>> = MutableLiveData()
    val refundCancelLiveData: LiveData<Resource<PurchaseResponse>> = privateRefundCancelLiveData

    fun requestDetail(seq : String, type : String) {
        viewModelScope.launch {
            privatePurchaseDetailLiveData.value = Resource.Loading()
            val request = PurchaseDetailRequest.toRequest(app, mDataRepository.getCacheUserKey(), seq, type)
            mDataRepository.requestPurchaseDetail(request).collect {
                privatePurchaseDetailLiveData.value = it
            }

        }
    }

    fun cancelRefund()
    {
        viewModelScope.launch {
            val productId = privatePurchaseDetailLiveData.value?.data?.product_id ?: return@launch
            val request = RefundCancelRequest.toRequest(app, mDataRepository.getCacheUserKey(), productId)
            mDataRepository.cancelRefund(request).collect {
                val userPoint = it.data?.user_point
                if (!userPoint.isNullOrEmpty())
                    getCommonPointLiveData().value = userPoint.toInt()
                privateRefundCancelLiveData.value = it
            }
        }
    }
}
