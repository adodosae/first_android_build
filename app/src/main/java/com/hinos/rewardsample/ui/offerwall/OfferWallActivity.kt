package com.hinos.rewardsample.ui.offerwall

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.item.ItemOfferWall
import com.hinos.rewardsample.data.dto.item.OfferWallType
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityOfferwallBinding
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.offerwall.adapter.OfferWallAdapter
import com.hinos.rewardsample.ui.point.history.PointHistoryActivity
import com.hinos.rewardsample.util.AppConfig
import com.igaworks.adpopcorn.Adpopcorn
import com.igaworks.adpopcorn.interfaces.IAdPOPcornEventListener
import com.nextapps.naswall.NASWall
import com.rdev.adfactory.AdsMgr
import com.rdev.adfactory.AdsMobile
import com.rdev.adfactory.Builder
import com.rdev.adfactory.listener.BannerAdsListener
import com.tnkfactory.ad.AdListType
import com.tnkfactory.ad.TnkSession
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OfferWallActivity : BaseActivity<ActivityOfferwallBinding, OfferWallViewModel>()
{
    private val TAG = this.javaClass.simpleName

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvPointSave.setOnClickListener { openActivity(PointHistoryActivity::class.java) } // 포인트 내역
        }
    }

    override fun observeViewModel() {
        mViewModel.openOfferWall.observe(this, ::handleOpenOfferWall)
        mViewModel.userPointResponseLiveData.observe(this, ::handleUserPointResponseLiveData)
    }

    override val mLayoutResID: Int = R.layout.activity_offerwall
    override val mViewModel: OfferWallViewModel by viewModels()

    private var m_mgrAdsBanner : AdsMgr? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkUserSession()
//        CoroutineScope(Dispatchers.IO).launch {
//            mGAID = AdvertisingIdInfo.requestIdInfo(this@OfferWallActivity).id
//            Log.d(TAG, "onCreate: $mGAID")
//            mOfferWallViewModel.userKey = mGAID
//        }

        initListener()
        initRecyclerView()
        loadBanner()
    }

    private fun loadBanner()
    {
        val builder = Builder(this)
            .addAdsBannerListener(object : BannerAdsListener
            {
                override fun onBannerFailToLoad(errorMessage: String)
                {
                    println("onBannerFailToLoad")
                }

                override fun onBannerLoaded()
                {
                    println("onBannerLoaded")
                }
            })
        if (AppConfig.DEBUG)
            builder.addTestDevice(AppConfig.TESTKEY)

        m_mgrAdsBanner = AdsMobile.getAdsMgr(builder)?.apply {
            loadBanner(mBinding.llBanner, AdsMobile.getETCBannerData())
        }
    }

    override fun onRestart()
    {
        super.onRestart()
        mViewModel.fetchUserPoint()
    }

    private fun initRecyclerView()
    {
        mBinding.recyclerView.run {
            layoutManager = LinearLayoutManager(this@OfferWallActivity)
            adapter = OfferWallAdapter(mViewModel).apply { setNewData(mViewModel.offerWallList) }
        }
    }

    private fun handleOpenOfferWall(item : ItemOfferWall?)
    {
        item ?: return

        when(item.type)
        {
            OfferWallType.NAS ->
            {
                NASWall.init(this, false)
                NASWall.open(this, AppConfig.APP_KEY+"|"+mViewModel.userKey)
            }
            OfferWallType.ADPOPCORN ->
            {
                Adpopcorn.setUserId(this,AppConfig.APP_KEY+"|"+mViewModel.userKey)
                Adpopcorn.setEventListener(this, object : IAdPOPcornEventListener
                {
                    override fun OnClosedOfferWallPage()
                    {
                        Log.d(TAG, "OnClosedOfferWallPage: ")
                    }

                    override fun OnAgreePrivacy()
                    {
                        Log.d(TAG, "OnAgreePrivacy: a")

                    }

                    override fun OnDisagreePrivacy()
                    {
                        Log.d(TAG, "OnDisagreePrivacy: ")
                    }
                })
                Adpopcorn.openOfferWall(this)
            }
            OfferWallType.TNK ->
            {
                TnkSession.setCOPPA(this, true)
                TnkSession.setUserName(this, AppConfig.APP_KEY+"|"+mViewModel.userKey)
                TnkSession.setUserCat(this, AppConfig.APP_KEY)
                TnkSession.showAdList(this, "무료 캐시 받기", AdListType.ALL)
            }
        }
    }

    private fun handleUserPointResponseLiveData(status : Resource<PurchaseResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading ->
                {
                    loaderView.visibility = View.VISIBLE
                }
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                }
            }
        }
    }
}