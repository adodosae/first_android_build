package com.hinos.rewardsample.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.data.dto.response.NoticeListResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.FragmentNoticeListBinding
import com.hinos.rewardsample.ui.center.notice.NoticeListActivity
import com.hinos.rewardsample.ui.center.notice.NoticeViewModel
import com.hinos.rewardsample.ui.center.notice.adapter.NoticeListAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class NoticeListFragment : Fragment()
{
    companion object
    {
        private const val EXTRA_PAGE_NAME = "EXTRA_PAGE_NAME"
        private const val EXTRA_PAGE_TYPE = "EXTRA_PAGE_TYPE"

        fun newInstance(pageName : String, pageType : String) : NoticeListFragment
        {
            return NoticeListFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTRA_PAGE_NAME, pageName)
                    putString(EXTRA_PAGE_TYPE, pageType)
                }
            }
        }
    }



    private lateinit var mBinding : FragmentNoticeListBinding
    private val mViewModel: NoticeViewModel by activityViewModels()

    private val mAdtNoticeList : NoticeListAdapter by lazy { NoticeListAdapter(mViewModel) }

    lateinit var mPageName : String
    lateinit var mPageType : String



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
    {
        mBinding = FragmentNoticeListBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        arguments?.run {
            mPageName = getString(EXTRA_PAGE_NAME)!!
            mPageType = getString(EXTRA_PAGE_TYPE)!!
        }

        initRecyclerView()
        initObserve()
    }

    private fun initObserve()
    {
        mViewModel.run {
            if (mPageType == "NOTICE")
                noticeListLiveData.observe(activity!!, ::handleNoticeList)
            else
                faqListLiveData.observe(activity!!, ::handleNoticeList)
        }
    }

    private fun initRecyclerView()
    {
//        mViewModel = (activity as NoticeListActivity).mViewModel
//        mAdtNoticeList = NoticeListAdapter(mViewModel)
        mBinding.recyclerView.run {
            layoutManager = LinearLayoutManager(activity)
            adapter = mAdtNoticeList
        }
        mViewModel.requestBoard(mPageType)
    }

    fun setNewData(data : List<NoticeListResponse.Data>)
    {
        mAdtNoticeList.setNewData(data)
    }

    private fun handleNoticeList(status : Resource<NoticeListResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    mAdtNoticeList.setNewData(it.boardList)
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                }
            }
        }
    }

}
