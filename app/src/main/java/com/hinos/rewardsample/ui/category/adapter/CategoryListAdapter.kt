package com.hinos.rewardsample.ui.category.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.CategorySearchResponse
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.databinding.ItemCategoryListBinding
import com.hinos.rewardsample.databinding.ItemMoreBinding
import com.hinos.rewardsample.ui.category.list.CategoryViewModel
import com.hinos.rewardsample.util.Common

class CategoryListAdapter(private val categoryViewModel : CategoryViewModel) : MoreListAdapter(categoryViewModel)
{
    private var mItems : MutableList<CategoryResponse.Data> = categoryViewModel.mItems

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            categoryViewModel.openCategoryDetail(item as CategoryResponse.Data)
        }
    }

    override fun getItemViewType(position: Int): Int
    {
        val size = mItems.size
        if (position < size)
            return LIST_NORMAL

        if (!mShowMore)
            return LIST_NORMAL

        return LIST_MORE
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == LIST_NORMAL)
            return CategoryListViewHolder(ItemCategoryListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        else
            return MoreViewBinding(ItemMoreBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
    {
        if (getItemViewType(position) == LIST_MORE)
            return

        (holder as CategoryListViewHolder).bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int
    {
        if (mShowMore)
        {
            return mItems.size + 1
        }
        return mItems.size
    }

    fun addNewData(addItems : List<CategoryResponse.Data>)
    {
        mItems.addAll(addItems)
        notifyDataSetChanged()
    }

    fun setNewData(newItems : List<CategoryResponse.Data>)
    {
        mItems.clear()
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

//    fun clearData()
//    {
//        mItems.clear()
//        notifyDataSetChanged()
//    }

    inner class CategoryListViewHolder(private val itemBinding : ItemCategoryListBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : CategoryResponse.Data, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                root.setOnClickListener { recyclerItemListener.onItemSelected(item) }
                tvPoint.text = Common.toPointFormat(item.goods_price)
                tvSubTitle.text = item.brand_name
                tvTitle.text = item.goods_name

                if (item.goods_thumb.isEmpty())
                    Glide.with(ivIcon.context).clear(ivIcon)
                else
                    Glide.with(ivIcon.context).load(item.goods_thumb).into(ivIcon)
            }
        }
    }

}