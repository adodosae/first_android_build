package com.hinos.rewardsample.ui.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color

import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import com.hinos.rewardsample.R

import kotlin.math.roundToInt

/**
 * 2021.07.24 김윤규
 * 문의하기 엑티비티
 *
 */
class MenuItemView : LinearLayout
{
    private var m_ivMenuIcon: ImageView? = null
    private var m_tvMenuText: TextView? = null

    constructor(context: Context?) : super(context) {
        initView()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initView()
        getAttrs(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView()
        getAttrs(attrs)
    }

    private fun initView() {
        this.orientation = HORIZONTAL
        this.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        m_ivMenuIcon = ImageView(this.context)
        m_tvMenuText = TextView(this.context)
    }

    private fun getAttrs(attrs: AttributeSet?) {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.MenuItemView)
        setMenuImageView(typedArray)
        setMenuTextView(typedArray)
    }

    private fun setMenuImageView(typedArray: TypedArray) {
        val width = typedArray.getDimension(R.styleable.MenuItemView_iconWidth, 0f).roundToInt()
        val height = typedArray.getDimension(R.styleable.MenuItemView_iconHeight, 0f).roundToInt()
        val startMargin = typedArray.getDimension(R.styleable.MenuItemView_iconStartMargin, 0f).roundToInt()
        val endMargin = typedArray.getDimension(R.styleable.MenuItemView_iconEndMargin, 0f).roundToInt()
        val topMargin = typedArray.getDimension(R.styleable.MenuItemView_iconTopMargin, 0f).roundToInt()
        val bottomMargin = typedArray.getDimension(R.styleable.MenuItemView_iconBottomMargin, 0f).roundToInt()
        val drawableRes = typedArray.getResourceId(R.styleable.MenuItemView_iconSrc, R.drawable.ic_setting)

        val gravityFlag: Int = typedArray.getInt(R.styleable.MenuItemView_gravityIcon, Gravity.CENTER_VERTICAL)

        val params = LayoutParams(width, height)
        params.setMargins(startMargin, topMargin, endMargin, bottomMargin)
        params.gravity = gravityFlag
        m_ivMenuIcon?.run {
            layoutParams = params
            setBackgroundResource(drawableRes)
        }
        this.addView(m_ivMenuIcon)
    }

    private fun setMenuTextView(typedArray: TypedArray) {
        val nameTitle = typedArray.getString(R.styleable.MenuItemView_nameTitle)
        @ColorInt val textColor = typedArray.getColor(R.styleable.MenuItemView_nameColor, Color.parseColor("#000000"))

        val startMargin = typedArray.getDimension(R.styleable.MenuItemView_nameStartMargin, 0f).roundToInt()
        val endMargin = typedArray.getDimension(R.styleable.MenuItemView_nameEndMargin, 0f).roundToInt()
        val topMargin = typedArray.getDimension(R.styleable.MenuItemView_nameTopMargin, 0f).roundToInt()
        val bottomMargin = typedArray.getDimension(R.styleable.MenuItemView_nameBottomMargin, 0f).roundToInt()

        val gravityFlag : Int = typedArray.getInt(R.styleable.MenuItemView_gravityTitle, Gravity.CENTER_VERTICAL)

        val params = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        params.setMargins(startMargin, topMargin, endMargin, bottomMargin)
        params.gravity = gravityFlag

        m_tvMenuText?.run {
            layoutParams = params
            text = nameTitle
            setTextColor(textColor)
            gravity = Gravity.CENTER_VERTICAL
        }
        this.addView(m_tvMenuText)
    }


    fun setTitle(title : String)
    {
        m_tvMenuText?.run {
            text = title
        }
        this.invalidate()
    }
}