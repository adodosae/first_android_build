package com.hinos.rewardsample.ui.store

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateCategoryListLiveData = MutableLiveData<Resource<StoreResponse>>()
    val storeListLiveData: LiveData<Resource<StoreResponse>> get() = privateCategoryListLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateClickEvtCategory = MutableLiveData<StoreResponse.Data?>()
    val clickEvtStore: LiveData<StoreResponse.Data?> get() = privateClickEvtCategory

    val refundUseLiveData : MutableLiveData<String> = MutableLiveData()

    init
    {
        requestStoreCategory()
    }

    fun openCategoryList(item : StoreResponse.Data)
    {
        viewModelScope.launch {
            privateClickEvtCategory.value = item
            privateClickEvtCategory.value = null
        }
    }

    private fun requestStoreCategory()
    {
        viewModelScope.launch {
            privateCategoryListLiveData.value = Resource.Loading()
            val request = BaseRequest.toRequest(app, mDataRepository.getCacheUserKey())
            mDataRepository.requestStore(request).collect {
                privateCategoryListLiveData.value = it
            }
        }
    }

}
