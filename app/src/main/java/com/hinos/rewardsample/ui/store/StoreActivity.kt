package com.hinos.rewardsample.ui.store

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityStoreBinding
import com.hinos.rewardsample.ui.category.list.CategoryActivity
import com.hinos.rewardsample.ui.category.search.CategorySearchActivity
import com.hinos.rewardsample.ui.imoge.ImogeActivity
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.purchase.history.PurchaseHistoryActivity
import com.hinos.rewardsample.ui.refund.RefundActivity
import com.hinos.rewardsample.ui.store.adapter.StoreAdapter
import com.hinos.rewardsample.util.AppConfig
import com.rdev.adfactory.AdsMgr
import com.rdev.adfactory.AdsMobile
import com.rdev.adfactory.Builder
import com.rdev.adfactory.listener.BannerAdsListener
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class StoreActivity : BaseActivity<ActivityStoreBinding, StoreViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_store
    override val mViewModel: StoreViewModel by viewModels()
    private val mAdtStore : StoreAdapter by lazy { StoreAdapter(mViewModel) }
    private var m_mgrAdsBanner : AdsMgr? = null

    override fun observeViewModel()
    {
        mViewModel.run {
            storeListLiveData.observe(this@StoreActivity, ::handleStoreListData)
            clickEvtStore.observe(this@StoreActivity, ::handleClickEvtStore)
            refundUseLiveData.observe(this@StoreActivity, ::handleRefundUseLiveData)
        }
    }

    private fun initRecyclerView()
    {
        mBinding.recyclerViewCategory.run {
            layoutManager = GridLayoutManager(this@StoreActivity, 3)
            adapter = mAdtStore
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvPurchaseHistory.setOnClickListener { openActivity(PurchaseHistoryActivity::class.java) }
            llSearch.setOnClickListener { openActivity(CategorySearchActivity::class.java) }
            vgImoge.setOnClickListener { openActivity(ImogeActivity::class.java) }
            vgRefund.setOnClickListener { openActivity(RefundActivity::class.java) }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initRecyclerView()
        loadBanner()
    }

    private fun loadBanner()
    {
        val builder = Builder(this)
            .addAdsBannerListener(object : BannerAdsListener
            {
                override fun onBannerFailToLoad(errorMessage: String)
                {
                    println("onBannerFailToLoad")
                }

                override fun onBannerLoaded()
                {
                    println("onBannerLoaded")
                }
            })
        if (AppConfig.DEBUG)
            builder.addTestDevice(AppConfig.TESTKEY)

        m_mgrAdsBanner = AdsMobile.getAdsMgr(builder)?.apply {
            loadBanner(mBinding.llBanner, AdsMobile.getETCBannerData())
        }
    }

    private fun handleStoreListData(status : Resource<StoreResponse>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.categoryList?.let { data -> // 회원가입 완료
                    bottomLayout.visibility = View.VISIBLE
                    mAdtStore.setNewData(data)
                }
                is Resource.DataError ->
                {
                    bottomLayout.visibility = View.INVISIBLE
                    val errorMsg = getErrorString(status.errorCode ?: 0)
                    showToast(errorMsg)
                }
            }
        }
    }
    private fun handleClickEvtStore(data : StoreResponse.Data?)
    {
        data ?: return

        val i = Intent(this, CategoryActivity::class.java).apply {
            putExtra(CategoryActivity.EXTRA_CATEGORY_DATA, data)
        }
        openActivity(i)
    }
    private fun handleRefundUseLiveData(refundUse : String)
    {
        mBinding.run {
            vgRefund.visibility = if (refundUse == "1") View.VISIBLE else View.GONE
        }
    }
}