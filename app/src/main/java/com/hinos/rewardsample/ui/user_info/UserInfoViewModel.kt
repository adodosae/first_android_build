package com.hinos.rewardsample.ui.user_info

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.SignOutRequest
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class UserInfoViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateNicknameLiveData = MutableLiveData<String>()
    val nicknameLiveData: LiveData<String> get() = privateNicknameLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateEmailLiveData = MutableLiveData<String>()
    val emailLiveData: LiveData<String> get() = privateEmailLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privatePhoneLiveData = MutableLiveData<String>()
    val phoneLiveData: LiveData<String> get() = privatePhoneLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateSignOutResponseResult = MutableLiveData<Resource<ResCode>>()
    val signOutResponseResult : LiveData<Resource<ResCode>> get() = privateSignOutResponseResult


    fun getUserInfo()
    {
        viewModelScope.launch {
            privateNicknameLiveData.value = mDataRepository.getCacheNickname()
            privateEmailLiveData.value = mDataRepository.getCacheEmail()
            privatePhoneLiveData.value = Common.getDevicePhoneNumber(app)
        }
    }
}
