package com.hinos.rewardsample.ui.views

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.hinos.rewardsample.util.Common

class StickBar : View
{

    var mActive = false

    constructor(context: Context?) : super(context) {
        initViewSetting()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initViewSetting()
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initViewSetting()
    }

    private fun initViewSetting()
    {
        active(mActive)
    }

     fun active(isActive : Boolean, isFail : Boolean = false)
    {
        val drawable = GradientDrawable()
        if (isFail)
            drawable.setColor(Color.parseColor(if (isActive) "#e45f5f" else "#ced4da"))
         else
            drawable.setColor(Color.parseColor(if (isActive) "#4582f9" else "#ced4da"))

        drawable.cornerRadius = Common.dp(context, 2)
        background = drawable
        mActive = isActive
    }
}