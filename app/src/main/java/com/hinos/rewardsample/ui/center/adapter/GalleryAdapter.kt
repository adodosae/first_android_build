package com.hinos.rewardsample.ui.center.adapter

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.R
import com.hinos.rewardsample.util.Common

class GalleryAdapter(
    val m_arGallery : GalleryArray,
    val m_maxSize : Int,
    val m_mode : Int = MODE_INQUIRY,
    var m_hOnItemClickListener : OnItemClickListener,
    var m_hOnDeleteListener : OnItemClickListener?
)
    : RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>()
{
    companion object
    {
        const val GALLERY_LEFT = 1
        const val GALLERY_CENTER = 2
        const val GALLERY_RIGHT = 3

        const val MODE_INQUIRY = 1
        const val MODE_BOARD = 2
    }
    inner class GalleryViewHolder(v : View) : RecyclerView.ViewHolder(v)
    {
        val v_conGalleryContainer: ConstraintLayout = v.findViewById(R.id.v_conGalleryContainer)
        val v_ivPicture: ImageView = v.findViewById(R.id.v_ivPicture)
        val v_ivDelete : ImageView = v.findViewById(R.id.v_ivDelete)
        val v_ivDefault : ImageView = v.findViewById(R.id.v_ivDefault)
        val v_tvSize : TextView = v.findViewById(R.id.v_tvSize)
        val v_dimContainer : ConstraintLayout = v.findViewById(R.id.v_dimContainer)
    }

    override fun getItemViewType(position: Int): Int
    {
        when (position % 4)
        {
            0 -> return GALLERY_LEFT
            3 -> return  GALLERY_RIGHT
            else -> return GALLERY_CENTER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder
    {
        val v : View
        when(viewType)
        {
            GALLERY_LEFT    -> {
                v = LayoutInflater.from(parent.context).inflate(R.layout.item_grallery1, parent, false)
                val params = v.findViewById<ImageView>(R.id.v_ivDefault).layoutParams
                if (m_mode == MODE_BOARD)
                {
                    params.height = Common.dp(parent.context, 24).toInt()
                    params.width = Common.dp(parent.context, 24).toInt()
                }
            }
            GALLERY_CENTER    -> v = LayoutInflater.from(parent.context).inflate(R.layout.item_grallery2, parent, false)
            else            -> v = LayoutInflater.from(parent.context).inflate(R.layout.item_grallery3, parent, false)
        }
        return GalleryViewHolder(v)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int)
    {
        val vo = m_arGallery[position]

        when(getItemViewType(position))
        {
            GALLERY_LEFT ->
            {
                if (vo.bTemp) hideItemView(holder) else showItemView(holder, position)
            }
            else -> {
                showItemView(holder, position)
            }
        }
        holder.v_conGalleryContainer.setOnClickListener {
            m_hOnItemClickListener.onItemClickListener(position)
        }
    }

    private fun showItemView(vh : GalleryViewHolder, position: Int)
    {
        vh.v_ivPicture.visibility = View.VISIBLE
        vh.v_ivDelete.visibility = View.VISIBLE
        vh.v_ivDefault.visibility = View.GONE
        vh.v_tvSize.visibility = View.GONE
        vh.v_dimContainer.setBackgroundColor(Color.parseColor("#8c000000"))

        val bitmap = Common.getBitmapFromStorage(vh.v_ivPicture.context, m_arGallery[position].strUri)
        if (bitmap != null)
        {
            vh.v_ivPicture.setImageBitmap(bitmap)
        }
        vh.v_ivDelete.setOnClickListener {
            m_hOnDeleteListener?.onItemClickListener(position)
        }
    }

    private fun hideItemView(vh : GalleryViewHolder)
    {
        vh.v_ivPicture.visibility = View.INVISIBLE
        vh.v_ivDelete.visibility = View.INVISIBLE
        vh.v_tvSize.text = "${m_arGallery.size-1}/$m_maxSize"
        vh.v_tvSize.visibility = View.VISIBLE
        vh.v_dimContainer.background = null
    }

    override fun getItemCount(): Int
    {
        return m_arGallery.size
    }

    class GalleryArray : ArrayList<GalleryArray.GalleryVo>()
    {
        data class GalleryVo(val strUri : String, val bTemp : Boolean = false)
    }

    interface OnItemClickListener {
        fun onItemClickListener(position: Int)
    }
}