package com.hinos.rewardsample.ui.user_info

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.ChangePwdRequest
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewPwdViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    private val privateNextBtnLiveData = MutableLiveData(false)
    val nextBtnLiveData: LiveData<Boolean> get() = privateNextBtnLiveData

    private val privateChangePwdLivedata : MutableLiveData<Resource<ResCode>> = MutableLiveData()
    val changePwdLiveData: LiveData<Resource<ResCode>> get() = privateChangePwdLivedata

    private fun isValidValue(etPwd : String, etNewPwd : String, etReNewPwd : String) : Int
    {
        val isValidPwd = ValueChecker.isValidPassword(etPwd)
        val isValidNewPwd = ValueChecker.isValidPassword(etNewPwd)
        val isValidRePwd = ValueChecker.isValidPassword(etNewPwd) && etNewPwd == etReNewPwd
//        val isValidSameValue = etPwd == etNewPwd && etPwd == etReNewPwd

        if (!isValidPwd) {
            // 비밀번호 잘못됨 ㄱㄱ
            return ErrorCase.INVALID_PASSWORD_SIZE
        }

        if (!isValidNewPwd) {
            return ErrorCase.INVALID_NEW_CHANGE_PASSWORD_SIZE
        }

        if (!isValidRePwd) {
            return ErrorCase.INVALID_NEW_CHANGE_PASSWORD_EQUAL
        }
        return 0
    }

    fun isWrapValue(etPwd : String, etNewPwd : String, etReNewPwd : String)
    {
        viewModelScope.launch {
            val resultCode = isValidValue(etPwd, etNewPwd, etReNewPwd)
//            if (resultCode != 0)
//                privateErrorMsgLiveData.value = Resource.DataError(resultCode)
//            else
//                privateErrorMsgLiveData.value = Resource.Success(resultCode)

            privateNextBtnLiveData.value = resultCode == 0
        }
    }

    fun requestChangePassword(pwd : String, new_pwd1 : String, new_pwd2 : String)
    {
        viewModelScope.launch {
            val resultCode = isValidValue(pwd, new_pwd1, new_pwd2)
            if (resultCode != 0)
            {
                privateChangePwdLivedata.value = Resource.DataError(resultCode)
                return@launch
            }

            privateChangePwdLivedata.value = Resource.Loading()

            val request = ChangePwdRequest.toRequest(
                context = app,
                user_key = mDataRepository.getCacheUserKey(),
                email = mDataRepository.getCacheEmail(),
                pwd = pwd,
                new_pwd1 = new_pwd1,
                new_pwd2 = new_pwd2
            )

            mDataRepository.requestChangePassword(request).collect {
                privateChangePwdLivedata.value = it
            }
        }
    }
}