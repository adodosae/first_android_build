package com.hinos.rewardsample.ui.imoge.detail

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.request.ImogeRequest
import com.hinos.rewardsample.data.dto.response.PointHistoryResponse
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImogeDetailViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{

}
