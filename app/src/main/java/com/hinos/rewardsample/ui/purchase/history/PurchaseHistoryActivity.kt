package com.hinos.rewardsample.ui.purchase.history

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityPurchaseHistoryBinding
import com.hinos.rewardsample.ui.imoge.detail.ImogeDetailActivity
import com.hinos.rewardsample.ui.purchase.adapter.PurchaseHistoryAdapter
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailActivity
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailViewModel
import com.hinos.rewardsample.ui.refund.detail.RefundDetailActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PurchaseHistoryActivity : BaseActivity<ActivityPurchaseHistoryBinding, PurchaseHistoryViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_purchase_history
    override val mViewModel: PurchaseHistoryViewModel by viewModels()
    private val mAdtPurchaseHistory : PurchaseHistoryAdapter by lazy { PurchaseHistoryAdapter(mViewModel) }

    override fun observeViewModel()
    {
        mViewModel.run {
            purchaseHistoryItems.observe(this@PurchaseHistoryActivity, ::handlePurchaseHistoryItems)
            clickPurchaseHistory.observe(this@PurchaseHistoryActivity, ::handleClickListEvent)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initRecyclerView()
    }

    private fun initRecyclerView()
    {
        mBinding.recyclerView.run {
            layoutManager = LinearLayoutManager(this@PurchaseHistoryActivity)
            adapter = mAdtPurchaseHistory
        }
    }

    private fun handlePurchaseHistoryItems(status : Resource<PurchaseHistoryResponse>)
    {
        mBinding.run {
            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.historyItems?.let { data -> // 회원가입 완료
                    mAdtPurchaseHistory.setNewData(data)
                }
                is Resource.DataError ->
                {
                    val errorMsg = getErrorString(status.errorCode ?: 0)
                    showToast(errorMsg)
                }
            }
        }
    }

    private fun handleClickListEvent(data : PurchaseHistoryResponse.Data?)
    {
        data ?: return

        val i : Intent = when(data.purchase_type) {
            "2" -> Intent(this, ImogeDetailActivity::class.java)
            "3" -> Intent(this, RefundDetailActivity::class.java)
            else -> Intent(this, PurchaseDetailActivity::class.java).apply { putExtra(PurchaseDetailViewModel.EXTRA_DETAIL_BRAND, data.category) }
        }.apply {
            putExtra(PurchaseDetailViewModel.EXTRA_DETAIL_SEQ, data.seq)
            putExtra(PurchaseDetailViewModel.EXTRA_DETAIL_TYPE, data.purchase_type)
        }

        openActivity(i)
    }
}