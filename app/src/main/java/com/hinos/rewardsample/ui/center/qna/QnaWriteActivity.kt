package com.hinos.rewardsample.ui.center.qna

import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource

import com.hinos.rewardsample.databinding.ActivityQnaWriteBinding
import com.hinos.rewardsample.ui.center.adapter.GalleryAdapter
import com.hinos.rewardsample.ui.center.adapter.GalleryAdapter.Companion.MODE_INQUIRY
import com.hinos.rewardsample.ui.center.qna.history.QnaHistoryActivity
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.util.Common
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.XwPermission
import dagger.hilt.android.AndroidEntryPoint
import gun0912.tedimagepicker.builder.TedImagePicker
import gun0912.tedimagepicker.builder.listener.OnMultiSelectedListener
import gun0912.tedimagepicker.builder.type.AlbumType
import gun0912.tedimagepicker.builder.type.MediaType

//import gun0912.tedimagepicker.builder.TedImagePicker
//import gun0912.tedimagepicker.builder.listener.OnMultiSelectedListener
//import gun0912.tedimagepicker.builder.type.AlbumType
//import gun0912.tedimagepicker.builder.type.MediaType

@AndroidEntryPoint
class QnaWriteActivity : BaseActivity<ActivityQnaWriteBinding, QnaWriteViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_qna_write
    override val mViewModel: QnaWriteViewModel by viewModels()

    private val m_arGallery: GalleryAdapter.GalleryArray = GalleryAdapter.GalleryArray()
    private val m_adtGallery : GalleryAdapter by lazy { GalleryAdapter(
            m_arGallery,
        3,
        MODE_INQUIRY,
        mItemClickListener,
        mItemDeleteClickListener
    )
    }


    override fun observeViewModel()
    {
        mViewModel.run {
            typeLiveData.observe(this@QnaWriteActivity, ::handleTypeValue)
            nextBtnState.observe(this@QnaWriteActivity, ::handleNextBtnState)
            qnaWriteResponse.observe(this@QnaWriteActivity, ::handleQnaWriteResponse)

        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener {
                closeActivity()
            }
            ivKinds.setOnClickListener {
                openBottomSheet(BottomSheet.BOTTOM_INQUIRY)
            }
            tvType.setOnClickListener {
                openBottomSheet(BottomSheet.BOTTOM_INQUIRY)
            }
            tvNext.setOnClickListener {
                openBottomSheet(BottomSheet.REQUEST_INQUIRIES)
            }
            tvType.addTextChangedListener {
                checkNextBtnState()
            }
            etTitle.addTextChangedListener {
                checkNextBtnState()
            }
            etContents.addTextChangedListener {
                checkNextBtnState()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initRecyclerView()
    }

    override fun onBackPressed()
    {
        if (mViewModel.qnaWriteResponse.value is Resource.Loading)
        {
            return
        }
        super.onBackPressed()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        val result: Boolean = XwPermission.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            XwPermission.EnumPermission.STORAGE_PERMISSION
        )
        if (result)
            takePictures()
        else
            showToast(getString(R.string.should_has_picture_permission_message))

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun closeActivity()
    {
        mBinding.run {
            if ((mViewModel.typeLiveData.value ?: 0) < 0 ||
                etTitle.text.toString().isNotEmpty() ||
                etContents.text.toString().isNotEmpty() ||
                m_arGallery.size > 1
            )
            {
                openBottomSheet(BottomSheet.BACK_CANCEL)
                return@run
            }
            super.closeActivity()
        }
    }

    private fun initRecyclerView()
    {
        m_arGallery.run {
            clear()
            add(GalleryAdapter.GalleryArray.GalleryVo("", true))
        }

        mBinding.recyclerViewPicture.run {
            layoutManager = GridLayoutManager(this@QnaWriteActivity, 4)
            adapter = m_adtGallery
        }
    }

    private val mItemClickListener = object : GalleryAdapter.OnItemClickListener
    {
        override fun onItemClickListener(position: Int)
        {
            if (position == 0)
            {
                if (XwPermission.getPermissionState(this@QnaWriteActivity, XwPermission.EnumPermission.STORAGE_PERMISSION))
                {
                    takePictures()
                }
            }
        }

    }

    private val mItemDeleteClickListener = object : GalleryAdapter.OnItemClickListener
    {
        override fun onItemClickListener(position: Int)
        {
            if (!Common.isProperArray(m_arGallery, position))
                return

            m_arGallery.removeAt(position)
            m_adtGallery.notifyDataSetChanged()
        }

    }

    private fun checkNextBtnState()
    {
        mBinding.run {
            mViewModel.isWrapValue(etTitle.text.toString(), etContents.text.toString())
        }
    }

    private fun handleTypeValue(type : Int)
    {
        mBinding.tvType.run {
            if (type > 0) {
                text = mViewModel.typeToString(type)
                setTextColor(Color.BLACK)
            }
        }
    }

    private fun handleNextBtnState(isActive : Boolean)
    {
        mBinding.tvNext.run {
            isClickable = isActive
            setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
        }
    }

    private fun handleQnaWriteResponse(status : Resource<ResCode>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    openBottomSheet(BottomSheet.CONFIRM_INQUIRIES)
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { errorCode ->
                        showToast(errorCode)
                    }
                }
            }
        }

    }
    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.setOnDialogResult(object : BaseDialog.OnDialogResult
        {
            override fun onDialogResult(nResult: Int, data: Any)
            {
                mBinding.run {
                    if (nResult == BaseDialog.RES_OK)
                    {
                        when(sheet)
                        {
                            BottomSheet.BOTTOM_INQUIRY ->
                            {
                                val type = data.toString().toInt()
                                mViewModel.selectInquiryType(type)
                            }
                            BottomSheet.REQUEST_INQUIRIES ->
                            {
                                mViewModel.requestWriteInquiry(
                                    title = etTitle.text.toString(),
                                    contents = etContents.text.toString(),
                                    galleryList = m_arGallery
                                )
                            }
                            BottomSheet.CONFIRM_INQUIRIES ->
                            {
                                openActivity(QnaHistoryActivity::class.java)
                                finish()
                            }
                            else ->
                            {
                                finish()
                            }
                        }
                    }
                }
                mSheet.dismiss()
            }
        })
        mSheet.showDialog(sheet)
    }

    private fun takePictures()
    {
        TedImagePicker.with(this)
            .max(3, R.string.upload_gallery_limit)
            .backButton(R.drawable.ic_close)
            .albumType(AlbumType.DROP_DOWN)
            .cameraTileBackground(R.color.white)
            .buttonTextColor(R.color.black)
            .buttonText(R.string.upload_gallery_attachment)
            .zoomIndicator(false)
            .showCameraTile(false)
            .showTitle(false)
            .mediaType(MediaType.IMAGE)
            .buttonBackground(R.color.white)
            .startMultiImage(object : OnMultiSelectedListener {
                override fun onSelected(uriList: List<Uri>) {
                    m_arGallery.clear()
                    m_arGallery.add(GalleryAdapter.GalleryArray.GalleryVo("", true))
                    for (uri in uriList) {
                        m_arGallery.add(
                            GalleryAdapter.GalleryArray.GalleryVo(
                                uri.toString(),
                                false
                            )
                        )
                    }
                    m_adtGallery.notifyDataSetChanged()
                }
            })
    }
}