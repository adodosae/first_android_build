package com.hinos.rewardsample.ui.offerwall.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.base.RecyclerItemListener
import com.hinos.rewardsample.data.dto.item.ItemOfferWall
import com.hinos.rewardsample.databinding.ItemOfferwallBinding
import com.hinos.rewardsample.ui.offerwall.OfferWallViewModel

class OfferWallAdapter(private val offerWallViewModel : OfferWallViewModel) : RecyclerView.Adapter<OfferWallAdapter.OfferWallViewHolder>()
{
    private var mItems : MutableList<ItemOfferWall> = mutableListOf()

    private val mOnItemClickListener : RecyclerItemListener = object : RecyclerItemListener {
        override fun <T> onItemSelected(item: T) {
            offerWallViewModel.openOfferWall(item as ItemOfferWall)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferWallViewHolder {
        val itemBinding = ItemOfferwallBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return OfferWallViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: OfferWallViewHolder, position: Int) {
        holder.bind(mItems[position], mOnItemClickListener)
    }

    override fun getItemCount(): Int = mItems.size

    fun setNewData(newItems : List<ItemOfferWall>)
    {
        mItems.addAll(newItems)
        notifyDataSetChanged()
    }

    class OfferWallViewHolder(private val itemBinding : ItemOfferwallBinding) : RecyclerView.ViewHolder(itemBinding.root)
    {
        fun bind(item : ItemOfferWall, recyclerItemListener: RecyclerItemListener) {
            itemBinding.run {
                root.setOnClickListener { recyclerItemListener.onItemSelected(item) }
//                tvNo.text = item.no
                tvTitle.text = item.title
            }
        }
    }
}