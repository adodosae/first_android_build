package com.hinos.rewardsample.ui.user_info

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivityUserInfoBinding
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.sign.out.SignOutActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserInfoActivity : BaseActivity<ActivityUserInfoBinding, UserInfoViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_user_info
    override val mViewModel: UserInfoViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            nicknameLiveData.observe(this@UserInfoActivity, ::handleNicknameData)
            emailLiveData.observe(this@UserInfoActivity, ::handleEmailData)
            phoneLiveData.observe(this@UserInfoActivity, ::handlePhoneNumberData)
            signOutResponseResult.observe(this@UserInfoActivity, ::handleSignOutResponseResult)
        }
    }


    override fun initListener()
    {
        mBinding.run {
            tvChangePassword.setOnClickListener {

            }

            ivMore.setOnClickListener {
                openPopup()
            }

            ivBack.setOnClickListener {
                closeActivity()
            }

            clRoot.setOnTouchListener { _, event ->
                if (event.action == KeyEvent.ACTION_UP) {
                    if (cvSignOut.visibility == View.VISIBLE) {
                        cvSignOut.visibility = View.GONE
                        return@setOnTouchListener false
                    }
                }
                return@setOnTouchListener true
            }

            tvChangePassword.setOnClickListener {
                openActivity(NewPwdActivity::class.java)
            }

            cvSignOut.setOnClickListener {
                openActivity(SignOutActivity::class.java)
//                openActivity() // 회원탈퇴 하지마세요 엑티비티
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        mViewModel.getUserInfo()
    }

    override fun onBackPressed()
    {
        mBinding.run {
            if (cvSignOut.visibility == View.VISIBLE) {
                cvSignOut.visibility = View.GONE
                return
            }
        }
        super.onBackPressed()
    }

    private fun handleNicknameData(nickName : String)
    {
        mBinding.run {
            tvNickname.text = nickName
        }
    }

    private fun handleEmailData(email : String)
    {
        mBinding.run {
            tvEmail.text = email
        }
    }

    private fun handlePhoneNumberData(phoneNumber : String)
    {
        mBinding.run {
            tvPhoneNum.text = phoneNumber
        }
    }

    private fun handleSignOutResponseResult(status : Resource<ResCode>) {
        if (status.data?.res_code == "0") {

        }

    }

    private fun openPopup()
    {
        mBinding.cvSignOut.run {
            visibility = if (isVisible) View.GONE else View.VISIBLE
        }
    }
}