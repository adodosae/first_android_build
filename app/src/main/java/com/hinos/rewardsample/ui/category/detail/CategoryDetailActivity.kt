package com.hinos.rewardsample.ui.category.detail

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.CategoryDetailResponse
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityCategoryDetailBinding
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped

@AndroidEntryPoint
@ActivityScoped
class CategoryDetailActivity : BaseActivity<ActivityCategoryDetailBinding, CategoryDetailViewModel>()
{
    companion object
    {
        const val EXTRA_CATEGORY_DETAIL_DATA = "EXTRA_CATEGORY_DETAIL_DATA"
    }

    override fun observeViewModel()
    {
        mViewModel.run {
            categoryItemLiveData.observe(this@CategoryDetailActivity, ::handleCategoryItemLiveData)
            categoryDetailLiveData.observe(this@CategoryDetailActivity, ::handleCategoryDetailLiveData)
            purchaseResponseLiveData.observe(this@CategoryDetailActivity, ::handlePurchaseResponseLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
            tvPurchase.setOnClickListener { openBottomSheet(BottomSheet.PURCHASE_PRODUCT) }
        }
    }

    override val mViewModel : CategoryDetailViewModel by viewModels()
    override val mLayoutResID: Int = R.layout.activity_category_detail

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        checkUserSession()
        initCategoryDetail()
    }

    private fun initCategoryDetail()
    {
        val data = intent.getParcelableExtra<CategoryResponse.Data?>(EXTRA_CATEGORY_DETAIL_DATA)
        mViewModel.initCategoryDetail(data)
    }

    private fun handleCategoryItemLiveData(data : CategoryResponse.Data)
    {
        mViewModel.requestCategoryDetail(data)
    }

    private fun handleCategoryDetailLiveData(status : Resource<CategoryDetailResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    initGoods(it)
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handlePurchaseResponseLiveData(status : Resource<PurchaseResponse>)
    {
        mBinding.run {
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    loaderView.visibility = View.GONE
                    showToast(getString(R.string.purchase_completed_text))
                    closeActivity()
                }
                is Resource.DataError ->
                {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun initGoods(data : CategoryDetailResponse)
    {
        mBinding.run {
            Glide.with(this@CategoryDetailActivity).load(data.goods_thumb).into(ivIcon)
            tvBrand.text = data.brand_name
            tvGoodsName.text = data.goods_name
            tvPrice.text = Common.toPointFormat(data.goods_price)
            tvContent.text = data.goods_content
        }
    }

    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.run {
            setOnDialogResult(object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    dismiss()
                    if (nResult == BaseDialog.RES_OK)
                    {
                        if (sheet == BottomSheet.PURCHASE_PRODUCT) {
                            mViewModel.purchaseProduct()
                        }
                    }
                }
            })
            showDialog(sheet, params)
        }
    }
}