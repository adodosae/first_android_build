package com.hinos.rewardsample.ui.center.qna.history

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.data.dto.response.CategorySearchResponse
import com.hinos.rewardsample.data.dto.response.QnaListResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityQnaHistoryBinding
import com.hinos.rewardsample.ui.center.adapter.NoticeViewPagerAdapter
import com.hinos.rewardsample.ui.center.adapter.QnaViewPagerAdapter
import com.hinos.rewardsample.ui.center.qna.QnaDetailActivity
import com.hinos.rewardsample.ui.fragment.QnaListFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class QnaHistoryActivity : BaseActivity<ActivityQnaHistoryBinding, QnaHistoryViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_qna_history
    override val mViewModel: QnaHistoryViewModel by viewModels()

    private lateinit var mAdtPager : QnaViewPagerAdapter


    @Inject
    lateinit var mFragments : List<QnaListFragment>

    override fun observeViewModel()
    {
        mViewModel.run {
            qnaListLiveData.observe(this@QnaHistoryActivity, ::handleQnaListLiveData)
            clickEvtQnaDetail.observe(this@QnaHistoryActivity, ::handleQnaDetailLiveData)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            ivBack.setOnClickListener { closeActivity() }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        mViewModel.requestQnaHistory()
    }

    private fun initViewPager()
    {
        mBinding.run {
            mAdtPager = QnaViewPagerAdapter(this@QnaHistoryActivity.supportFragmentManager, mFragments)
            viewPager.adapter = mAdtPager
            mAdtPager.setUpWithCustomTabLayout(0, viewPager, llTab)
        }
    }

    private fun handleQnaListLiveData(status : Resource<QnaListResponse>) {
        mBinding.run {
            when (status) {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let {
                    initViewPager()
                }
                is Resource.DataError -> {
                    loaderView.visibility = View.GONE
                    status.errorCode?.let { showToast(it)}
                }
            }
        }
    }

    private fun handleQnaDetailLiveData(data : QnaListResponse.Data)
    {
        val i = Intent(this, QnaDetailActivity::class.java).apply {
            putExtra(QnaHistoryViewModel.EXTRA_QNA_DETAIL, data)
        }
        openActivity(i)
    }
}