package com.hinos.rewardsample.ui.purchase.history

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PurchaseHistoryViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{

    val purchaseHistoryItems: MutableLiveData<Resource<PurchaseHistoryResponse>> = MutableLiveData()

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateClickEvtPurchaseHistory = MutableLiveData<PurchaseHistoryResponse.Data?>()
    val clickPurchaseHistory: LiveData<PurchaseHistoryResponse.Data?> get() = privateClickEvtPurchaseHistory

    init
    {
        requestPurchaseHistory()
    }

    private fun requestPurchaseHistory() {
        viewModelScope.launch {
            purchaseHistoryItems.value = Resource.Loading()
            val request = BaseRequest.toRequest(app, mDataRepository.getCacheUserKey())
            mDataRepository.requestPurchaseHistory(request).collect {
                purchaseHistoryItems.value = it
            }
        }
    }

    fun openProductHistoryDetail(item : PurchaseHistoryResponse.Data)
    {
        viewModelScope.launch {
            privateClickEvtPurchaseHistory.value = item
            privateClickEvtPurchaseHistory.value = null
        }
    }
}
