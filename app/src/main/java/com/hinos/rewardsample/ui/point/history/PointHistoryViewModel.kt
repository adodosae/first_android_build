package com.hinos.rewardsample.ui.point.history

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.response.PointHistoryResponse
import com.hinos.rewardsample.data.dto.response.PurchaseHistoryResponse
import com.hinos.rewardsample.data.dto.response.StoreResponse
import com.hinos.rewardsample.data.remote.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PointHistoryViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privatePointHistoryLiveData = MutableLiveData<Resource<PointHistoryResponse>>()
    val pointHistoryLiveData: LiveData<Resource<PointHistoryResponse>> get() = privatePointHistoryLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateClickEvtPointHistory = MutableLiveData<PointHistoryResponse.Data?>()
    val clickEvtPointHistory: LiveData<PointHistoryResponse.Data?> get() = privateClickEvtPointHistory


    init
    {
        requestPointHistory()
    }

    fun requestPointHistory()
    {
        viewModelScope.launch {
            val request = BaseRequest.toRequest(
                app, mDataRepository.getCacheUserKey()
            )

            mDataRepository.requestPointHistory(request).collect {
                privatePointHistoryLiveData.value = it
            }
        }
    }


}
