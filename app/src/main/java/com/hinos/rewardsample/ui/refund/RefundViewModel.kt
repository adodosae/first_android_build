package com.hinos.rewardsample.ui.refund

import android.app.Application
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.RefundRequest
import com.hinos.rewardsample.data.dto.response.PurchaseResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import okhttp3.internal.immutableListOf
import javax.inject.Inject

@HiltViewModel
class RefundViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    val mRefundPoint : List<Int> = immutableListOf(20000, 30000, 40000, 50000)


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateSelectCashPosition = MutableLiveData<Int>(-1)
    val selectCashPosition : LiveData<Int> get() = privateSelectCashPosition

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateRemainPointLiveData = MutableLiveData<Int>()
    val remainPointLiveData: LiveData<Int> get() = privateRemainPointLiveData


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateRefundActiveLiveData = MutableLiveData<Boolean>(false)
    val refundActiveLiveData: LiveData<Boolean> get() = privateRefundActiveLiveData

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val  privateRefundResponseLiveData = MutableLiveData<Resource<PurchaseResponse>>()
    val refundResponseLiveData : LiveData<Resource<PurchaseResponse>> get() = privateRefundResponseLiveData

    val refundHelpUrlLiveData : MutableLiveData<String> = MutableLiveData()

    init {
        viewModelScope.launch {
            refundHelpUrlLiveData.value = mDataRepository.getCacheRefundHelp()
        }
    }

    fun selectCash(position: Int)
    {
        viewModelScope.launch {
            val remainPoint = getMyApp().mPointLiveData.value!! - mRefundPoint[position]
            privateSelectCashPosition.value = position
            privateRemainPointLiveData.value = remainPoint
        }
    }

    fun requestRefund(bank_name : String, account_num : String, account_name : String)
    {
        viewModelScope.launch {
            if (selectCashPosition.value == null)
            {
                privateRefundResponseLiveData.value = Resource.DataError(ErrorCase.SERVER_ERROR_LESS_POINT)
                return@launch
            }

            val point = mRefundPoint[selectCashPosition.value!!]


            val request = RefundRequest.toRequest(
                context = app,
                user_key = mDataRepository.getCacheUserKey(),
                email = mDataRepository.getCacheEmail(),
                nickname = mDataRepository.getCacheNickname(),
                refund_amount = point.toString(),
                bank_name = bank_name,
                account_num = account_num,
                account_name = account_name
            )

            mDataRepository.requestRefund(request).collect {
                val userPoint = it.data?.user_point
                if (!userPoint.isNullOrEmpty())
                    getCommonPointLiveData().value = userPoint.toInt()

                privateRefundResponseLiveData.value = it
            }
        }
    }

    private fun isValidValue(bank : String, account : String, accountHolder : String) : Int
    {
        val isValidBankCheck = ValueChecker.isValidBank(bank)
        val isValidAccountCheck = ValueChecker.isValidAccount(account)
        val isValidAccountHolderCheck = ValueChecker.isValidAccountName(accountHolder)

        if (!isValidBankCheck)
            return ErrorCase.INVALID_BANK_ETC


        if (!isValidAccountCheck)
            return ErrorCase.INVALID_ACCOUNT_ETC


        if (!isValidAccountHolderCheck)
            return ErrorCase.INVALID_HOLDER_ETC

        val remainPoint = privateRemainPointLiveData.value ?: -1
        if (remainPoint < 0)
            return ErrorCase.SERVER_ERROR_LESS_POINT


        return 0
    }

    fun isWrapValue(bank : String, account : String, accountHolder : String)
    {
        viewModelScope.launch {
            privateRefundActiveLiveData.value = isValidValue(bank, account, accountHolder) == 0
        }
    }


}
