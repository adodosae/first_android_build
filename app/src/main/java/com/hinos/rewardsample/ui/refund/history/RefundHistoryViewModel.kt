package com.hinos.rewardsample.ui.refund.history

import android.app.Application
import android.content.Intent
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.hinos.rewardsample.base.BaseViewModel
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.dto.response.CategoryResponse
import com.hinos.rewardsample.data.dto.response.PurchaseDetailResponse
import com.hinos.rewardsample.data.dto.response.RefundHistoryResponse
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.ui.purchase.detail.PurchaseDetailViewModel
import com.hinos.rewardsample.ui.refund.detail.RefundDetailActivity
import com.hinos.rewardsample.util.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RefundHistoryViewModel @Inject constructor(app : Application, repository : DataRepository): BaseViewModel(app, repository)
{
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    private val privateRefundHistoryLiveData: MutableLiveData<Resource<RefundHistoryResponse>> = MutableLiveData()
    val refundHistoryLiveData: LiveData<Resource<RefundHistoryResponse>> = privateRefundHistoryLiveData

    val clickEvtRefundHistory: SingleLiveEvent<RefundHistoryResponse.Data> = SingleLiveEvent()

    fun requestRefundHistory()
    {
        viewModelScope.launch {
            val request = BaseRequest.toRequest(
                context = app,
                mDataRepository.getCacheUserKey()
            )

            privateRefundHistoryLiveData.value = Resource.Loading()

            mDataRepository.requestRefundHistory(request).collect {
                privateRefundHistoryLiveData.value = it
            }
        }
    }

    fun openRefundHistoryDetail(item : RefundHistoryResponse.Data)
    {
        viewModelScope.launch {
            clickEvtRefundHistory.value = item
        }
    }
}
