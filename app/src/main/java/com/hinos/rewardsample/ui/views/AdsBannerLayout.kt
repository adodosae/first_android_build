package com.hinos.rewardsample.ui.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout
import com.hinos.rewardsample.R

class AdsBannerLayout : LinearLayout
{
    constructor(context: Context?) : super(context) {
        initViewSetting()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initViewSetting()
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initViewSetting()
    }

    private fun initViewSetting()
    {
        orientation = LinearLayout.HORIZONTAL
        setBackgroundColor(Color.BLACK)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int)
    {
        val height = resources.getDimension(R.dimen.ads_banner_layout_height)
        setMeasuredDimension(ViewGroup.LayoutParams.MATCH_PARENT, height.toInt())
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }
}