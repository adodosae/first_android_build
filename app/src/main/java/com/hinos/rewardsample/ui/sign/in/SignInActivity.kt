package com.hinos.rewardsample.ui.sign.`in`

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.widget.addTextChangedListener
import com.hinos.rewardsample.R
import com.hinos.rewardsample.base.BaseActivity
import com.hinos.rewardsample.base.BaseDialog
import com.hinos.rewardsample.data.dto.response.ResCode
import com.hinos.rewardsample.data.remote.Resource
import com.hinos.rewardsample.databinding.ActivityMainWrapperBinding
import com.hinos.rewardsample.databinding.ActivitySignInBinding
import com.hinos.rewardsample.ui.dialog.BottomSheet
import com.hinos.rewardsample.ui.dialog.GeneralSheet
import com.hinos.rewardsample.ui.main.MainViewModel
import com.hinos.rewardsample.ui.web.TermsWebActivity
import com.hinos.rewardsample.util.ErrorCase
import com.hinos.rewardsample.util.ValueChecker
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SignInActivity : BaseActivity<ActivitySignInBinding, SignInViewModel>()
{
    override val mLayoutResID: Int = R.layout.activity_sign_in
    override val mViewModel: SignInViewModel by viewModels()

    override fun observeViewModel()
    {
        mViewModel.run {
            nextBtn.observe(this@SignInActivity, ::handleNextBtnFlag)
            signInData.observe(this@SignInActivity, ::handleSignInResponse)
            checkEmail.observe(this@SignInActivity, ::handleCheckEmail)
            checkNickname.observe(this@SignInActivity, ::handleNickname)
        }
    }

    override fun initListener()
    {
        mBinding.run {
            etPwd.addTextChangedListener {
                val pwd = etPwd.text.toString()
                tvPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(pwd)) "#4582f9" else "#e45f5f"))
                tvRePwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etRePwd.text.toString()) && etRePwd.text.toString() == etPwd.text.toString()) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }
            etRePwd.addTextChangedListener {
                tvPwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etPwd.text.toString())) "#4582f9" else "#e45f5f"))
                tvRePwdHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidPassword(etRePwd.text.toString()) && etRePwd.text.toString() == etPwd.text.toString()) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }
            etRecommend.addTextChangedListener {
                tvRecommendHelp.setTextColor(Color.parseColor(if (ValueChecker.isValidRecommend(etRecommend.text.toString()) && etNickname.text.toString() != etRecommend.text.toString()) "#4582f9" else "#e45f5f"))
                checkNextBtnState()
            }

            ivBack.setOnClickListener { closeActivity() }
            ivShowServiceWeb.setOnClickListener { openTermsActivity(getString(R.string.terms_service_title), mViewModel.서비스이용약관.value ?: "") }
            ivShowPrivateWeb.setOnClickListener { openTermsActivity(getString(R.string.terms_private_title), mViewModel.개인정보처리방침.value ?: "") }
            tvNext.setOnClickListener {
                signIn()
            }
            cbServiceAgree.setOnCheckedChangeListener { _, isChecked ->
                mViewModel.checkServiceAgree.value = isChecked
                checkNextBtnState()
            }
            cbPriAgree.setOnCheckedChangeListener { _, isChecked ->
                mViewModel.checkPriAgree.value = isChecked
                checkNextBtnState()
            }
            tvValidEmail.setOnClickListener {
                mViewModel.requestOverlapEmail(etEmail.text.toString())
                checkNextBtnState()
            }
            tvValidNickname.setOnClickListener {
                mViewModel.requestOverlapNickname(etNickname.text.toString()) {
                    checkNextBtnState()
                }
            }
            ivClearEmail.setOnClickListener {
                mViewModel.checkEmail.value = ""
                etEmail.text.clear()
            }
            ivClearNickname.setOnClickListener {
                mViewModel.checkNickname.value = ""
                etNickname.text.clear()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    private fun signIn()
    {
        mBinding.run {
            val email = etEmail.text.toString()
            val password = etPwd.text.toString()
            val rePassword = etRePwd.text.toString()

            val nickName = etNickname.text.toString()
            val recommend = etRecommend.text.toString()

            mViewModel.signInUser(email, password, rePassword, nickName, recommend)
        }
    }

    private fun checkNextBtnState()
    {
        mBinding.run {
            mViewModel.isWrapValue(etEmail.text.toString(), etPwd.text.toString(), etRePwd.text.toString(), etNickname.text.toString(), etRecommend.text.toString())
        }
    }

    private fun handleSignInResponse(status : Resource<ResCode>)
    {
        mBinding.run {

            tvEmailError.visibility = View.GONE
            tvPasswordError.visibility = View.GONE
            tvRePasswordError.visibility = View.GONE
            tvNicknameError.visibility = View.GONE
            tvRecommendError.visibility = View.GONE

            loaderView.visibility = View.GONE
            when(status)
            {
                is Resource.Loading -> loaderView.visibility = View.VISIBLE
                is Resource.Success -> status.data?.let { _ -> // 회원가입 완료
                    closeActivity()
                }
                is Resource.DataError ->
                {
                    val errorMsg = getErrorString(status.errorCode ?: 0)
                    when(status.errorCode)
                    {
                        ErrorCase.SERVER_ERROR_RECOMMEND_OVERLAP -> {
                            openBottomSheet(BottomSheet.INVALID_RECOMMEND)
                        }
                        ErrorCase.INVALID_EMAIL_FORM, ErrorCase.ETC_ERROR_CHECK_EMAIL ->
                        {
                            tvEmailError.run {
                                text = errorMsg
                                visibility = View.VISIBLE
                            }
                        }
                        ErrorCase.INVALID_PASSWORD_SIZE -> {
                            tvPasswordError.run {
                                text = errorMsg
                                visibility = View.VISIBLE
                            }
                        }
                        ErrorCase.INVALID_RE_PASSWORD_ETC -> {
                            tvRePasswordError.run {
                                text = errorMsg
                                visibility = View.VISIBLE
                            }
                        }
                        ErrorCase.INVALID_NAME_SIZE, ErrorCase.ETC_ERROR_CHECK_NICKNAME -> {
                            tvNicknameError.run {
                                text = errorMsg
                                visibility = View.VISIBLE
                            }
                        }
                        ErrorCase.INVALID_RECOMMEND_ETC, ErrorCase.ETC_ERROR_RECO_NICKNAME, ErrorCase.SERVER_ERROR_RECOMMEND_SELF -> {
                            tvRecommendError.run {
                                text = errorMsg
                                visibility = View.VISIBLE
                            }
                        }
                        else -> {
                            showToast(errorMsg)
                        }
                    }
                }
            }
        }
    }

    private fun handleNextBtnFlag(isActive : Boolean)
    {
        mBinding.tvNext.run {
            isClickable = isActive
            setBackgroundColor(Color.parseColor(if (isActive) "#4582f9" else "#e9f1fb"))
        }
    }

    private fun handleCheckEmail(email : String)
    {
        mBinding.run {
            val success = etEmail.text.toString() == email
            etEmail.isEnabled = !success
            tvEmailHelp.setTextColor(Color.parseColor(if (success) "#4582f9" else "#e45f5f"))
            tvEmailError.visibility = View.GONE
            ivClearEmail.visibility = if (success) View.VISIBLE else View.GONE
            checkNextBtnState()
            loaderView.visibility = View.GONE
        }
    }

    private fun handleNickname(nickName : String)
    {
        mBinding.run {
            val success = etNickname.text.toString() == nickName
            etNickname.isEnabled = !success
            tvNicknameHelp.setTextColor(Color.parseColor(if (success) "#4582f9" else "#e45f5f"))

            tvNicknameError.visibility = View.GONE
            ivClearNickname.visibility = if (success) View.VISIBLE else View.GONE
            loaderView.visibility = View.GONE
            checkNextBtnState()
        }
    }



    private fun openTermsActivity(title : String, url : String)
    {
        val i = Intent(this@SignInActivity, TermsWebActivity::class.java).apply {
            putExtra(TermsWebActivity.EXTRA_TERMS_TITLE, title)
            putExtra(TermsWebActivity.EXTRA_TERMS_URL, url)
        }
        startActivity(i)
    }

    private fun openBottomSheet(sheet : BottomSheet, params : HashMap<String, Any>? = null)
    {
        mSheet.run {
            setOnDialogResult(object : BaseDialog.OnDialogResult
            {
                override fun onDialogResult(nResult: Int, data: Any)
                {
                    mSheet.dismiss()
                    if (sheet == BottomSheet.INVALID_RECOMMEND) {
                        closeActivity()
                    }
                }
            })
            showDialog(sheet, params)
        }
    }
}