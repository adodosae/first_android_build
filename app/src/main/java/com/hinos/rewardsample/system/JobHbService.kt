package com.hinos.rewardsample.system

import android.app.job.JobParameters
import android.app.job.JobService
import android.util.Log
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest

import com.hinos.rewardsample.data.local.JobPrefs
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class JobHbService : JobService() {

    private val TAG = JobHbService::class.java.simpleName

    @Inject
    lateinit var mRepository: DataRepository

    override fun onStartJob(params: JobParameters): Boolean
    {
        val context = applicationContext
        val currentTime = Common.getCurrentTime()
        val lastTime = JobPrefs.readLastHeartbeatCheckTime(context)
        val successTime = JobPrefs.readLastHeartbeatSuccessTime(context)
        var nItv: Long

        if (successTime > 0)
        {
            nItv = currentTime - successTime

            if (nItv < AppConfig.HEARTBEAT_ITV)
                return false
        }

        if (successTime > 0)
        {
            nItv = currentTime - lastTime
            if (nItv < AppConfig.HEARTBEAT_EITV)
                return false
        }

        CoroutineScope(Dispatchers.IO).launch {
            val request = BaseRequest.toRequest(
                applicationContext,
                mRepository.getCacheUserKey()
            )
            mRepository.directHeartBeat(request).data?.let {
                JobPrefs.writeLastHeartbeatCheckTime(applicationContext, currentTime)
                if (it.res_code == "0")
                {
                    JobPrefs.writeLastHeartbeatCheckResult(applicationContext, true)
                    JobPrefs.writeLastHeartbeatSuccessTime(applicationContext, currentTime)
                } else {
                    JobPrefs.writeLastHeartbeatCheckResult(applicationContext, false)
                }
            }
        }
//        val context = applicationContext
//        val currentTime = Common.getCurrentTime(context)
//        val lastTime = JobPrefs.readLastHeartbeatCheckTime(context)
//        val successTime = JobPrefs.readLastHeartbeatSuccessTime(context)
//        var nItv: Long
//
//        if (successTime > 0)
//        {
//            nItv = currentTime - successTime
//
//            if (nItv < AppConfig.HEARTBEAT_ITV)
//                return false
//        }
//
//        if (successTime > 0)
//        {
//            nItv = currentTime - lastTime
//            if (nItv < AppConfig.HEARTBEAT_EITV)
//                return false
//        }
//
//        AppService.startHeartBeat(applicationContext, AppService.CMD_HEARTBEAT)
        return true
    }

    override fun onStopJob(params: JobParameters): Boolean {
        return false
    }
}
