package com.hinos.rewardsample.system;

import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hinos.rewardsample.data.local.LocalData;
import com.hinos.rewardsample.util.AppConfig;
import com.hinos.rewardsample.util.Common;
import com.hinos.rewardsample.util.Encrypt;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService
{
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String token = FirebaseInstanceId.getInstance().getToken();

        sendRegistrationToServer(token);
    }

    private void sendRegistrationToServer(String token)
    {
        // Add custom implementation, as needed.
        Context context = getApplicationContext();
        OkHttpClient client = new OkHttpClient();

        LocalData localData = new LocalData(this);


        RequestBody body = new FormBody.Builder()
                .add("app_key", Encrypt.INSTANCE.encodeText(AppConfig.APP_KEY))
                .add("user_key", Encrypt.INSTANCE.encodeText(localData.getCacheUserKey()))
                .add("version", Common.INSTANCE.getAppVersion(this))
                .add("token", token)
                .build();

        //request
        Request request = new Request.Builder()
                .url(AppConfig.SCRIPT_REG_FCM)
                .post(body)
                .build();

        try {
            client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
