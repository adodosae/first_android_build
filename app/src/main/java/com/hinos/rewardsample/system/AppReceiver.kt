package com.hinos.rewardsample.system

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.hinos.rewardsample.base.BaseBroadcastReceiver
import com.hinos.rewardsample.data.local.JobPrefs
import com.hinos.rewardsample.util.AppConfig
import com.hinos.rewardsample.util.ComJob
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AppReceiver @Inject constructor() : BaseBroadcastReceiver()
{
    override fun onReceive(context: Context, intent: Intent)
    {
        super.onReceive(context, intent)
        val action = intent.action

//        if (action == "android.intent.action.USER_PRESENT")
//        {
//            sendHeartBeat(context)
//            return
//        } else

        if (action == Intent.ACTION_MY_PACKAGE_REPLACED)
        {
            if (Build.VERSION.SDK_INT >= AppConfig.NEW_HEARTBEAT_VERSION) {
                ComJob.checkJobSchedule(context)
            }
        }
        else if (action == Intent.ACTION_BOOT_COMPLETED)
        {
            if (Build.VERSION.SDK_INT >= AppConfig.NEW_HEARTBEAT_VERSION) {
                ComJob.checkJobSchedule(context)
            }
        }
    }
}
