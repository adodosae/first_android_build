package com.hinos.rewardsample.system;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hinos.rewardsample.R;
import com.hinos.rewardsample.ui.splash.SplashActivity;
import com.hinos.rewardsample.util.AppConfig;
import com.hinos.rewardsample.util.Common;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService
{
    static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    static final String MSGTYPE_APPOPEN = "APPOPEN";
    static final String MSGTYPE_PAGEOPEN = "PAGEOPEN";
    static final String MSGTYPE_OUTLINK = "OUTLINK";

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
            //RemoteMessage.Notification n = remoteMessage.getNotification();
            Map<String, String> map = remoteMessage.getData();

            String strType = "";
            String strTitle;
            String strBody;
            String strLink ="", strAppLinkId = "";
            int nAppVersion = Integer.parseInt(Common.INSTANCE.getAppVersion(getApplicationContext()));

            if(map != null)
            {
                strType = map.get("type");
            }

            if(strType != null && !strType.isEmpty())
            {
                int nMinVer = 0, nMaxVer = 0;

                strTitle = map.get("title");
                strBody = map.get("message"); if(strBody == null) strBody = "";
                String strMinVer = map.get("minver"); if(strMinVer == null) strMinVer = "";
                String strMaxVer = map.get("maxver"); if(strMaxVer == null) strMaxVer = "";
                strLink = map.get("link"); if(strLink == null) strLink = "";
                strAppLinkId = map.get("linkid"); if(strAppLinkId == null) strAppLinkId = "";

                try
                {
                    if(!strMinVer.isEmpty())
                        nMinVer = Integer.parseInt(strMinVer);
                    if(!strMaxVer.isEmpty())
                        nMaxVer = Integer.parseInt(strMaxVer);
                }
                catch(Exception e)
                {
                    nMinVer = 0;
                    nMaxVer = 0;
                }

                if(nMinVer > 0)
                {
                    if(nAppVersion < nMinVer)
                        return;
                }
                if(nMaxVer > 0)
                {
                    if(nAppVersion > nMaxVer)
                        return;
                }

                Log.d(TAG, "MsgType: " + strType);
                if(strType.equalsIgnoreCase(MSGTYPE_APPOPEN))
                {
                    sendNotification(strTitle, strBody);
                }
                else if(strType.equalsIgnoreCase(MSGTYPE_PAGEOPEN))
                {
/*				if(strAppLinkId.isEmpty())
					return;

				Intent intent = new Intent(this, IntroActivity.class);
				intent.putExtra(IntroActivity.EXT_APP_LINK_ID, strAppLinkId);
				sendNotification(strTitle, strBody, intent);
*/			}
                else if(strType.equalsIgnoreCase(MSGTYPE_OUTLINK))
                {
                    if(strLink.isEmpty())
                        return;

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(strLink));
                    sendNotification(strTitle, strBody, intent);
                }
            }
            else
            {
                RemoteMessage.Notification n = remoteMessage.getNotification();

                strTitle = n.getTitle();
                strBody = n.getBody();

                if(strTitle == null || strBody == null)
                    return;
                if(strBody.isEmpty())
                    return;

                sendNotification(strTitle, strBody);
            }

        }catch (Exception e)
        {
            Log.d(TAG, "onMessageReceived: " + e.getMessage());
        }
    }

    private void sendNotification(String strTitle, String strMessage) {
        Intent intent = new Intent(this, SplashActivity.class);

        sendNotification(strTitle, strMessage, intent);
    }
    private void sendNotification(String strTitle, String strMessage, Intent intent)
    {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder builder;
        NotificationManager nm = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String strId = getResources().getString(R.string.push_channel_id);
        String strName = getResources().getString(R.string.push_channel_name);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(strId, strName, NotificationManager.IMPORTANCE_DEFAULT);
            nm.createNotificationChannel(channel);
            builder = new NotificationCompat.Builder(this, channel.getId());
        }
        else
        {
            builder = new NotificationCompat.Builder(this, strId);
        }

        builder.setSmallIcon(getIcon())
                .setContentTitle(strTitle)
                .setContentText(strMessage)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        nm.notify(AppConfig.NOTIFICATION_ID, builder.build());
    }
    private int getIcon()
    {
        int nIcon = R.mipmap.ic_launcher;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            nIcon = R.mipmap.ic_launcher;
        }

        return nIcon;
    }
}
