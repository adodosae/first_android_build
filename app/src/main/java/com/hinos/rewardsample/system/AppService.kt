package com.hinos.rewardsample.system


import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.hinos.rewardsample.R
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.dto.request.BaseRequest
import com.hinos.rewardsample.data.local.JobPrefs
import com.hinos.rewardsample.util.Common
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject


/**
 * Created by apulsa on 2017-09-28.
 */

@AndroidEntryPoint
class AppService : LifecycleService()
{
    @Inject
    lateinit var mRepository: DataRepository
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate()
    {
        super.onCreate()

        val channel = NotificationChannel(
            "heartBeat", getString(R.string.app_name),
            NotificationManager.IMPORTANCE_LOW
        )

        (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
            .createNotificationChannel(channel)

        val notification: Notification = NotificationCompat.Builder(this, "heartBeat")
            .setContentTitle("")
            .setContentText("").build()
        startForeground(2, notification)
    }

    override fun onDestroy()
    {
        super.onDestroy()
        if (lifecycleScope.isActive)
            lifecycleScope.cancel()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int
    {
        try {
            intent?.let {
                val cmd = it.getIntExtra(EXTRA_CMD, 0)
                if (cmd != 0)
                {
                    when(cmd)
                    {
                        CMD_HEARTBEAT ->
                        {
                            cmdHeartbeat()
                        }
                    }
                }
            }
        } catch (e: Exception) {

        }
        return START_NOT_STICKY
    }

    private fun cmdHeartbeat()
    {
        lifecycleScope.launch {
            val request = BaseRequest.toRequest(
                applicationContext,
                mRepository.getCacheUserKey()
            )
            mRepository.directHeartBeat(request).data?.let {
                Log.d(TAG, "cmdHeartbeat: $it")
                val currentTime = Common.getCurrentTime()
                JobPrefs.writeLastHeartbeatCheckTime(applicationContext, currentTime)
                if (it.res_code == "0")
                {
                    JobPrefs.writeLastHeartbeatCheckResult(applicationContext, true)
                    JobPrefs.writeLastHeartbeatSuccessTime(applicationContext, currentTime)
                } else {
                    JobPrefs.writeLastHeartbeatCheckResult(applicationContext, false)
                }
            }
        }
    }


    companion object
    {
        val TAG = AppService::class.java.simpleName
        val EXTRA_CMD = "CMD"

        const val CMD_HEARTBEAT = 1

        fun startHeartBeat(context: Context, nCmd: Int)
        {
            val i = Intent(context, AppService::class.java)
            i.putExtra(EXTRA_CMD, nCmd)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                context.startForegroundService(i)
            } else {
                context.startService(i)
            }
        }
    }
}
