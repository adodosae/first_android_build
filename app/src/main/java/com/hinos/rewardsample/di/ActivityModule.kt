package com.hinos.rewardsample.di

import android.app.Activity
import android.content.Context
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.hinos.rewardsample.R
import com.hinos.rewardsample.ui.dialog.GeneralDialog
import com.hinos.rewardsample.ui.dialog.GeneralSheet
import com.hinos.rewardsample.ui.fragment.NoticeListFragment
import com.hinos.rewardsample.ui.fragment.QnaListFragment
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Named
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(ActivityComponent::class)
class ActivityModule
{
    @Provides
    fun provideGeneralSheet(@ActivityContext context : Context) : GeneralSheet
    {
        return GeneralSheet(context)
    }

    @Provides
    fun provideGeneralDialog(@ActivityScoped activity : Activity) : GeneralDialog
    {
        return GeneralDialog(activity)
    }

    @Provides
    fun provideCenterListFragments(@ActivityContext context: Context) : List<NoticeListFragment>
    {
        return listOf(
            NoticeListFragment.newInstance(context.getString(R.string.center_notice), "NOTICE"),
            NoticeListFragment.newInstance(context.getString(R.string.center_faq), "FAQ")
        )
    }

    @Provides
    fun provideQnaListFragments(@ActivityContext context: Context) : List<QnaListFragment>
    {
        return listOf(
            QnaListFragment.newInstance(context.getString(R.string.qna_answer_all), "ALL"),
            QnaListFragment.newInstance(context.getString(R.string.qna_answer_wait), "WAIT"),
            QnaListFragment.newInstance(context.getString(R.string.qna_answer_complete), "COMPLETE")
        )
    }
}