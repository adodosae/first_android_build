package com.hinos.rewardsample.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.hinos.rewardsample.MyApp
import com.hinos.rewardsample.data.DataRepository
import com.hinos.rewardsample.data.local.LocalData
import com.hinos.rewardsample.data.remote.NetworkConnectivity
import com.hinos.rewardsample.data.remote.RemoteData
import com.hinos.rewardsample.util.Network
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton
import kotlin.coroutines.CoroutineContext

@Module
@InstallIn(SingletonComponent::class)
class AppModule
{
    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun provideLocalRepository(@ApplicationContext context: Context): LocalData {
        return LocalData(context)
    }

    @Provides
    @Singleton
    fun provideCoroutineContext(): CoroutineContext {
        return Dispatchers.IO
    }

    @Provides
    @Singleton
    fun provideNetworkConnectivity(@ApplicationContext context: Context): NetworkConnectivity {
        return Network(context)
    }

    @Provides
    fun provideRepository(
        remoteRepository : RemoteData,
        localRepository : LocalData,
        ioDispatcher : CoroutineContext
    ): DataRepository {
        return DataRepository(remoteRepository, localRepository, ioDispatcher)
    }
}