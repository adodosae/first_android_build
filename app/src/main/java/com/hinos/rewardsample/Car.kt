package com.hinos.rewardsample

import javax.inject.Inject

class Car @Inject constructor(brand: Brand, engine: Engine) {
    fun information() {
        println("Car 정보 출력")

//        1대당 요금 5원
//        2대당 요금 3원
//        3대당 요금 1원
//
//
//
//        1 2 3 4 5
//            3 4
//          2 3 4 5 6 7
//
//        1대 주차 시간대 : 1, 6, 7 = 15
//        2대 주차 시간대 : 2, 5 = 12
//        3대 주차 시간대 : 3, 4 = 6
//
//
//
//        33????????????????
//
//
//        1시~6시
//        3시~5시
//        2시~8시
//
//
//        5 3 1
//
//        1 6
//        3 5
//        2 8
//
//
//        33
    }
}

class Engine @Inject constructor()
class Brand @Inject constructor()