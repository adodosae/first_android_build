package com.hinos.rewardsample.util

import com.hinos.rewardsample.R
import java.util.*

object ErrorCase
{
    // 서버 에러 1000~

    const val SERVER_ERROR_SMS_SEND_FAIL                = 5001 // SMS 전송실패
    const val SERVER_ERROR_LOGIN_AUTH_FAIL              = 5002 // 이미 인증했던 사용자
    const val SERVER_ERROR_PHONE_AUTH_COUNT_FAIL        = 5003 // 인증횟수 초과
    const val SERVER_ERROR_PHONE_AUTH_VALUE_FAIL        = 5004 // 인증번호 불일치
    const val SERVER_ERROR_SESSION_EXPIRED              = 5005 // 세션 만료
    const val SERVER_ERROR_OVERLAP                      = 5006 // 중복
    const val SERVER_ERROR_EXIST_PHONE_NUMBER           = 5007 // 이미 가입된 전화번호
    const val SERVER_ERROR_ID_AND_PASSWORD_INCORRECT    = 5008 // 아이디 비밀번호 불일치
    const val SERVER_ERROR_FIND_SIGN_HISTORY            = 5009 // 가입이력 없음
    const val SERVER_ERROR_SEND_EMAIL_FAIL              = 5010 // 이메일 방송 실패
    const val SERVER_ERROR_FORBIDDEN_ACCESS_MEMBER      = 5011 // 접속불가 회원(유효, 중지, 탈퇴인 회원)
    const val SERVER_ERROR_NOT_FOUND_USER_INFO          = 5012 // 가입이력 없음
    const val SERVER_ERROR_RECOMMEND_OVERLAP            = 5013 // 추천 이력 있음
    const val SERVER_ERROR_RECOMMEND_SELF               = 5014 // 본인 추천

    const val SERVER_ERROR_LESS_POINT                   = 5016 // 보유 포인트 부족

    const val SERVER_ERROR_SEARCH_EMPTY                 = 5018 // 검색 결과 없음
//    const val SERVER_ERROR_NEW_PASSWORD_MISMATCH        = 5019 // 새로운 비밀번호 1,2의 값이 다름
    const val SERVER_ERROR_PASSWORD_MISMATCH            = 5020 // 비밀번호 틀림
    const val SERVER_ERROR_PHONE_AUTH_TIME_OUT          = 5021 // 휴대폰 인증시간 만료
    const val SERVER_ERROR_PHONE_ALREADY_USER           = 5022 // 이미 가입된 단말기



    const val SERVER_ERROR_INVALID_LINK                 = 5025 // 링크 형식이 아님

    // 로컬 에러 10000~
    const val INVALID_EMAIL_SIZE            = 10101 // 1~100자가 아닐 경우
    const val INVALID_EMAIL_FORM            = 10102 // e-mail 형식이 아닐 경우
    const val INVALID_EMAIL_OVERLAP         = 10103 // 중복된 ID가 있을 경우
    const val INVALID_EMAIL_ETC             = 10104 // 그 외 모두

    const val INVALID_PASSWORD_SIZE         = 10105 // 4~16자가 아닐 경우
    const val INVALID_PASSWORD_ETC          = 10106 // 비밀번호 그 외 모두
    const val INVALID_RE_PASSWORD_ETC       = 10107 // 비밀번호 확인 그 외 모두

    const val INVALID_NAME_SIZE             = 10108 // 3~16자가 아닐 경우
    const val INVALID_NAME_OVERLAP          = 10109 // 중복된 닉네임이 있을 경우
    const val INVALID_NAME_ETC              = 10110 // 닉네임 그 외 모두

    const val INVALID_PHONE_LENGTH          = 10111 // 휴대폰번호 에러
    const val INVALID_PHONE_ALREADY_SEND    = 10112 // 인증번호가 전송되고 유호 시간이 지나지 않았을 때
    const val INVALID_PHONE_SEND_MAX_SIZE   = 10113 // 인증번호 하루에 10건
    const val INVALID_PHONE_ETC             = 10114 // 인증번호 그 외 모두

    const val INVALID_RECOMMEND_EMPTY       = 10115 // 추천인 해당 닉네임 없을 경우
    const val INVALID_RECOMMEND_ETC         = 10116 // 추천인 그 외 모두

    const val INVALID_TERNS                 = 10117 // 이용약관 하나라도 동의하지 않은 경우


    const val INVALID_BANK_SIZE             = 10201 // 은행명 1~10자가 아닐 경우
    const val INVALID_BANK_SPECIFIC         = 10202 // 은행명 특수문자는 적을 수 없습니다.
    const val INVALID_BANK_ETC              = 10203 // 은행명을 다시 확인해주세요.
    const val INVALID_ACCOUNT_SIZE          = 10204 // 계좌번호 1~30자가 아닐 경우
    const val INVALID_ACCOUNT_NOT_NUMBER    = 10205 // 계좌번호 숫자가 아닐 경우
    const val INVALID_ACCOUNT_ETC           = 10206 // 그 외 모두

    const val INVALID_HOLDER_SIZE           = 10207 // 예금주 1~20자 이내로 입력 해주세요.
    const val INVALID_HOLDER_SPECIFIC       = 10208 // 예금주 특수문자는 적을 수 없습니다.
    const val INVALID_HOLDER_ETC            = 10209 // 예금주 그 외 모두

    const val INVALID_OUT_PASSWORD_NOT_EQUAL = 10301 // 회원탈퇴 현재 비밀번호랑 다를 경우
    const val INVALID_OUT_PASSWORD_ETC       = 10302 // 회원탈퇴 변경 그 외 모두

    const val INVALID_CHANGE_PASSWORD_NOT_EQUAL     = 10401 // 비밀번호 변경 현재 비밀번호랑 다를 경우
    const val INVALID_CHANGE_PASSWORD_ETC           = 10402 // 비밀번호 변경 그 외 모두

    const val INVALID_NEW_CHANGE_PASSWORD_SIZE      = 10403 // 새 비밀번호 4~16자가 아닐 경우
    const val INVALID_NEW_CHANGE_PASSWORD_EQUAL     = 10404 // 새 비밀번호 현재 비밀번호랑 같은 경우
    const val INVALID_NEW_CHANGE_PASSWORD_ETC       = 10405 // 새 비밀번호 그 외 모두

    const val INVALID_SEARCH_TEXT_EMPTY             = 10406 // 검색어가 2자리 미만일때

    const val SERVER_NETWORK_ERROR                         = 10501 // 네트워크 에러


    const val ETC_ERROR_CHECK_EMAIL                 = 10701 // 이메일 중복체크 안했음
    const val ETC_ERROR_CHECK_NICKNAME              = 10702 // 닉네임 중복체크 안했음
    const val ETC_ERROR_CHECK_SERVICE               = 10703 // 서비스 이용약관 체크
    const val ETC_ERROR_CHECK_PRIVATE               = 10704 // 개인정보 체크
    const val ETC_ERROR_RECO_NICKNAME               = 10705 // 추천인과 닉네임이 같은 경우

    const val QNA_FILE_SIZE_LIMIT                   = 10801 // 제한 4mb




    val errorHashtable = Hashtable<Int, Int>().apply {
        // 서버에러
        put(SERVER_ERROR_SMS_SEND_FAIL, R.string.error_5001)
        put(SERVER_ERROR_LOGIN_AUTH_FAIL, R.string.error_5002)
        put(SERVER_ERROR_PHONE_AUTH_COUNT_FAIL, R.string.error_5003)
        put(SERVER_ERROR_PHONE_AUTH_VALUE_FAIL, R.string.error_5004)
        put(SERVER_ERROR_SESSION_EXPIRED, R.string.error_5005)
        put(SERVER_ERROR_OVERLAP, R.string.error_5006)
        put(SERVER_ERROR_EXIST_PHONE_NUMBER, R.string.error_5007)
        put(SERVER_ERROR_ID_AND_PASSWORD_INCORRECT, R.string.error_5008)
        put(SERVER_ERROR_FIND_SIGN_HISTORY, R.string.error_5009)
        put(SERVER_ERROR_SEND_EMAIL_FAIL, R.string.error_5010)
        put(SERVER_ERROR_FORBIDDEN_ACCESS_MEMBER, R.string.error_5011)
        put(SERVER_ERROR_NOT_FOUND_USER_INFO, R.string.error_5012)
        put(SERVER_ERROR_RECOMMEND_SELF, R.string.error_5014)

        put(SERVER_ERROR_LESS_POINT, R.string.error_5016)

        put(SERVER_ERROR_SEARCH_EMPTY, R.string.error_5018)
        put(SERVER_ERROR_PASSWORD_MISMATCH, R.string.error_10405)



        put(SERVER_ERROR_SEARCH_EMPTY, R.string.error_5018)
        put(SERVER_ERROR_SEARCH_EMPTY, R.string.error_5018)
        put(SERVER_ERROR_SEARCH_EMPTY, R.string.error_5018)

        put(SERVER_ERROR_PHONE_AUTH_TIME_OUT, R.string.error_5021)
        put(SERVER_ERROR_PHONE_ALREADY_USER, R.string.error_5022)

        put(SERVER_ERROR_INVALID_LINK, R.string.error_5025)



        // 회원가입
        put(INVALID_EMAIL_SIZE, R.string.error_10101)
        put(INVALID_EMAIL_FORM, R.string.error_10102)
        put(INVALID_EMAIL_OVERLAP, R.string.error_10103)
        put(INVALID_EMAIL_ETC, R.string.error_10104)
        put(INVALID_PASSWORD_SIZE, R.string.error_10105)
        put(INVALID_PASSWORD_ETC, R.string.error_10106)
        put(INVALID_RE_PASSWORD_ETC, R.string.error_10107)
        put(INVALID_NAME_SIZE, R.string.error_10108)
        put(INVALID_NAME_OVERLAP, R.string.error_10109)
        put(INVALID_NAME_ETC, R.string.error_10110)
        put(INVALID_PHONE_LENGTH, R.string.error_10111)
        put(INVALID_PHONE_ALREADY_SEND, R.string.error_10112)
        put(INVALID_PHONE_SEND_MAX_SIZE, R.string.error_10113)
        put(INVALID_PHONE_ETC, R.string.error_10114)
        put(INVALID_RECOMMEND_EMPTY, R.string.error_10115)
        put(INVALID_RECOMMEND_ETC, R.string.error_10116)
        put(INVALID_TERNS, R.string.error_10117)

        // 환급신청
        put(INVALID_BANK_SIZE, R.string.error_10201)
        put(INVALID_BANK_SPECIFIC, R.string.error_10202)
        put(INVALID_BANK_ETC, R.string.error_10203)
        put(INVALID_ACCOUNT_SIZE, R.string.error_10204)
        put(INVALID_ACCOUNT_NOT_NUMBER, R.string.error_10205)
        put(INVALID_ACCOUNT_ETC, R.string.error_10206)


        put(INVALID_HOLDER_SIZE, R.string.error_10207)
        put(INVALID_HOLDER_SPECIFIC, R.string.error_10208)
        put(INVALID_HOLDER_ETC, R.string.error_10209)


        // 회원탈퇴
        put(INVALID_OUT_PASSWORD_NOT_EQUAL, R.string.error_10301)
        put(INVALID_OUT_PASSWORD_ETC, R.string.error_10302)


        // 비밀번호 변경
        put(INVALID_CHANGE_PASSWORD_NOT_EQUAL, R.string.error_10401)
        put(INVALID_CHANGE_PASSWORD_ETC, R.string.error_10402)
        put(INVALID_NEW_CHANGE_PASSWORD_SIZE, R.string.error_10403)
        put(INVALID_NEW_CHANGE_PASSWORD_EQUAL, R.string.error_10404)
        put(INVALID_NEW_CHANGE_PASSWORD_ETC, R.string.error_10405)
        put(INVALID_SEARCH_TEXT_EMPTY, R.string.error_10406)


        // 네트워크 에러
        put(SERVER_NETWORK_ERROR, R.string.error_10501)


        // 그 외 에러 메세지
        put(ETC_ERROR_CHECK_EMAIL, R.string.error_10701) // 이메일 중복확인 해주세여
        put(ETC_ERROR_CHECK_NICKNAME, R.string.error_10702) // 닉네임 중복확인 해주세여

        put(ETC_ERROR_CHECK_SERVICE, R.string.error_10703) // 서비스 이용약관 동의는 필수 입니다.
        put(ETC_ERROR_CHECK_PRIVATE, R.string.error_10704) // 개인정보 취급방침 동의는 필수 입니다.

        put(ETC_ERROR_RECO_NICKNAME, R.string.error_10705) // 본인을 추천할 수 없습니다.


        put(QNA_FILE_SIZE_LIMIT, R.string.error_10801)




    }
}