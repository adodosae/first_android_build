package com.hinos.rewardsample.util

object Encrypt
{
    private val AES_KEY = "c5b4758e1940b7960887d3155ee98758"

    fun encodeText(str: String?): String
    {
        return try {
            AES256Cipher.AES_Encode(str, AES_KEY)
        } catch (e: Exception) {
            ""
        }
    }

    fun decodeText(str: String?): String
    {
        return try {
            AES256Cipher.AES_Decode(str, AES_KEY)
        } catch (e: Exception) {
            ""
        }
    }
}