package com.hinos.rewardsample.util

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi

import com.hinos.rewardsample.system.JobHbService

object ComJob
{
    fun checkJobSchedule(context: Context)
    {
        if (Build.VERSION.SDK_INT < AppConfig.NEW_HEARTBEAT_VERSION)
            return

        if (isPending(context, AppConfig.JOB_ID))
            return

        val job = getJobInfo(context)

        val mJobService = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        
        mJobService.schedule(job)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun isPending(context: Context, serviceId: Int): Boolean {
        val scheduler = context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val pendingJobList = scheduler.allPendingJobs
        for (jobInfo in pendingJobList) {
            if (serviceId == jobInfo.id) {
                return true
            }
        }
        return false
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP) //21
    private fun getJobInfo(context: Context): JobInfo
    {
        val name = ComponentName(context, JobHbService::class.java)
        val nPeriodic = AppConfig.HEARTBEAT_JOBSCHEDULE_ITV.toLong() // run every hour
        val isPersistent = true
        val networkType = JobInfo.NETWORK_TYPE_ANY

        val jobInfo: JobInfo

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            jobInfo = JobInfo.Builder(AppConfig.JOB_ID, name)
                    .setRequiredNetworkType(networkType)
                    .setPersisted(isPersistent)
                    .setPeriodic(nPeriodic)
                    .setRequiredNetworkType(networkType)
                    .build()
        } else {
            jobInfo = JobInfo.Builder(AppConfig.JOB_ID, name)
                    .setRequiredNetworkType(networkType)
                    .setPeriodic(nPeriodic)
                    .setPersisted(isPersistent)
                    .build()
        }

        return jobInfo
    }

}
