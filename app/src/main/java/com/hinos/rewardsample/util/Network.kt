package com.hinos.rewardsample.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import com.hinos.rewardsample.data.remote.NetworkConnectivity
import javax.inject.Inject

/**
 * Created by AhmedEltaher
 */

class Network @Inject constructor(private val mContext: Context) : NetworkConnectivity {
    override fun isConnected(): Boolean {
        val connectivityManager = mContext.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager

        return if (Build.VERSION.SDK_INT >=
            Build.VERSION_CODES.M) {
            connectionCheckUnderM(connectivityManager)
        } else {
            connectionCheckUpperM(connectivityManager)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun connectionCheckUnderM(
        connectivityManager: ConnectivityManager
    ): Boolean {
        val network = connectivityManager.activeNetwork
        val connection = connectivityManager.getNetworkCapabilities(network)

        return connection != null && (
                connection.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                        connection.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))
    }

    private fun connectionCheckUpperM(
        connectivityManager: ConnectivityManager
    ): Boolean {
        val activeNetwork = connectivityManager.activeNetworkInfo

        if (activeNetwork != null) {
            return (activeNetwork.type == ConnectivityManager.TYPE_WIFI ||
                    activeNetwork.type == ConnectivityManager.TYPE_MOBILE)
        }
        return false
    }
}

