package com.hinos.rewardsample.util

import java.util.regex.Pattern

object ValueChecker {
    private val EMAIL_ADDRESS: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")"
    )

    private val SPECIFIC_CH : Pattern = Pattern.compile("[0-9|a-z|A-Z|ㄱ-ㅎ|ㅏ-ㅣ|가-힝]*")

    private val PHONE_NUMBER : Pattern = Pattern.compile(
        "^\\d{3}\\d{3,4}\\d{4}$"

    )

    private fun hasSpecific(text : String): Boolean {
        return !SPECIFIC_CH.matcher(text).matches()
    }



    fun isValidPassword(password : String) : Boolean {
        return password.trim().length in 4..16
    }
    fun isValidEmail(email: String): Boolean {
        if (!email.contains("@"))
            return false
        val split = email.split("@")
        return EMAIL_ADDRESS.matcher(email).matches() && split[0].isNotEmpty() && split[0].length <= 100
    }

    fun isValidPhoneNumLength(phoneNumber: String) : Boolean {
        return phoneNumber.length in 9..11
    }

    fun isValidPhoneNum(phoneNumber : String) : Boolean {
        return isValidPhoneNumLength(phoneNumber) && PHONE_NUMBER.matcher(phoneNumber).matches()
    }

    fun isValidNickName(nickName: String) : Boolean {
        return nickName.length in 3..16 && !hasSpecific(nickName)
    }

    fun isValidRecommend(recommend : String) : Boolean
    {
        return recommend.isEmpty() || recommend.length in 3..16 && !hasSpecific(recommend)
    }

    fun isValidProofKeyLength(proofKey : String) : Boolean {
        return proofKey.length == 6
    }


    fun isValidBank(bank : String) : Boolean {
        return bank.length in 2..10 && !hasSpecific(bank)
    }

    fun isValidAccount(account : String) : Boolean {
        return account.length in 10..25 && !hasSpecific(account)
    }

    fun isValidAccountName(accountName : String) : Boolean {
        return accountName.length in 2..10 && !hasSpecific(accountName)
    }
}