package com.hinos.rewardsample.util

import com.hinos.rewardsample.BuildConfig

object AppConfig
{
    const val APP_KEY = "FA010"
    const val ADS_TEMP_USER_KEY = "goodreward"
    const val BASE_URL = "https://devgiftshow.zdev.co.kr/"
    const val DEBUG : Boolean = true
    const val TESTKEY = "9FB14531BB1976F3C3C17BC29BA03A4F" // S10 5G

    const val JOB_ID = 105

    const val NOTIFICATION_ID = 1244

    const val URL_XW_ADS = "https://ads.neotem.net/wapi/login/"
    const val SCRIPT_REG_FCM: String = BASE_URL + "wapi/login/reg_fcm.php"


    const val TIME_SECOND   : Long = 1000
    const val TIME_MINUTE   : Long = 60 * TIME_SECOND
    const val TIME_HOUR     : Long = 3600 * TIME_SECOND
    const val TIME_DAY      : Long = 24 * TIME_HOUR
    const val TIME_WEEK     : Long = 7 * TIME_DAY
    const val RECOMMAND_ITV_DEV: Long = 1 * TIME_MINUTE
    const val RECOMMAND_ITV_REAL : Long = 5 * TIME_DAY


    val RECOMMAND_ITV : Long = if (BuildConfig.DEBUG) RECOMMAND_ITV_DEV else RECOMMAND_ITV_REAL

    val HEARTBEAT_JOBSCHEDULE_ITV = if (DEBUG) (5 * TIME_MINUTE) else (1 * TIME_HOUR)
    val HEARTBEAT_ITV = if (DEBUG) (5 * TIME_MINUTE).toInt() else (8 * TIME_HOUR).toInt()
    val HEARTBEAT_EITV = if (DEBUG) (1 * TIME_MINUTE).toInt() else (1 * TIME_HOUR).toInt()

    const val NEW_HEARTBEAT_VERSION = 21
}