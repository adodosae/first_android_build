package com.hinos.rewardsample.util

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BulletSpan
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.hinos.rewardsample.R
import com.scottyab.rootbeer.RootBeer
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.NumberFormat
import java.util.*
import kotlin.math.roundToInt


object Common
{
    val mHashInquiry : HashMap<Int, String> = HashMap<Int, String>().apply {
        put(1, "오류")
        put(2, "단순문의")
        put(3, "기타")
    }

    fun getBoardType(index : Int) : String
    {
        return when (index)
        {
            1 -> "ERR"
            2 -> "QNA"
            3 -> "REQ"
            else -> ""
        }
    }

    fun getBoardTypeString(type : String) : String
    {
        return when (type)
        {
            "ERR" -> "오류"
            "QNA" -> "단순문의"
            "REQ" -> "기타"
            else -> ""
        }
    }

    fun getAppSignatures(context: Context): List<String> {

        val appCodes = mutableListOf<String>()

        try {
            // Get all package signatures for the current package
            val packageName = context.packageName
            val packageManager = context.packageManager
            val signatures = packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES
            ).signatures

            // For each signature create a compatible hash
            for (signature in signatures) {
                val hash = getHash(packageName, signature.toCharsString())

                if (hash != null) {
                    appCodes.add(String.format("%s", hash))
                }

                Log.d("getAppSignatures", String.format("이 값을 SMS 뒤에 써서 보내주면 됩니다 : %s", hash))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.d("getAppSignatures", "Unable to find package to obtain hash. : $e")
        }

        return appCodes
    }

    fun getHash(packageName: String, signature: String): String? {
        val HASH_TYPE = "SHA-256"
        val NUM_HASHED_BYTES = 9
        val NUM_BASE64_CHAR = 11

        val appInfo = "$packageName $signature"

        try {
            val messageDigest = MessageDigest.getInstance(HASH_TYPE)

            // minSdkVersion이 19이상이면 체크 안해도 됨
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                messageDigest.update(appInfo.toByteArray(StandardCharsets.UTF_8))
            }

            // truncated into NUM_HASHED_BYTES
            val hashSignature = Arrays.copyOfRange(messageDigest.digest(), 0, NUM_HASHED_BYTES)

            // encode into Base64
            val base64Hash = Base64
                .encodeToString(hashSignature, Base64.NO_PADDING or Base64.NO_WRAP)
                .substring(0, NUM_BASE64_CHAR)

            Log.d("getHash", String.format("\nPackage : %s\nHash : %s", packageName, base64Hash))

            return base64Hash

        } catch (e: NoSuchAlgorithmException) {
            Log.d("getHash", "hash:NoSuchAlgorithm : $e")
        }

        return null
    }

    fun isProperUser(context: Context) : Boolean
    {
        if (isRootUser(context))
            return false
        if (isEmulatorUser())
            return false
        if (!isUsefulUSIM(context))
            return false
        if (getDevicePhoneNumber(context).isEmpty())
            return false
        return true
    }

    fun isRootUser(context: Context) : Boolean
    {
        val rootMgr = RootBeer(context)
        return rootMgr.isRooted
    }

    fun isEmulatorUser() : Boolean
    {
        fun checkEmulatorFiles() : Boolean {

            val GENY_FILES = arrayOf(
                    "/dev/socket/genyd",
                    "/dev/socket/baseband_genyd"
            )
            val PIPES = arrayOf(
                    "/dev/socket/qemud",
                    "/dev/qemu_pipe"
            )
            val X86_FILES = arrayOf(
                    "ueventd.android_x86.rc",
                    "x86.prop",
                    "ueventd.ttVM_x86.rc",
                    "init.ttVM_x86.rc",
                    "fstab.ttVM_x86",
                    "fstab.vbox86",
                    "init.vbox86.rc",
                    "ueventd.vbox86.rc"
            )
            val ANDY_FILES = arrayOf(
                    "fstab.andy",
                    "ueventd.andy.rc"
            )
            val NOX_FILES = arrayOf(
                    "fstab.nox",
                    "init.nox.rc",
                    "ueventd.nox.rc"
            )

            fun checkFiles(targets: Array<String>): Boolean {

                for (pipe in targets) {
                    val file = File(pipe)
                    if (file.exists()) {
                        return true
                    }
                }
                return false
            }

            return (checkFiles(GENY_FILES)
                    || checkFiles(ANDY_FILES)
                    || checkFiles(NOX_FILES)
                    || checkFiles(X86_FILES)
                    || checkFiles(PIPES))
        }
        val isEmulator = (Build.MANUFACTURER.contains("Genymotion")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.lowercase().contains("droid4x")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.HARDWARE == "goldfish"
                || Build.HARDWARE == "vbox86"
                || Build.HARDWARE.lowercase().contains("nox")
                || Build.FINGERPRINT.startsWith("generic")
                || Build.PRODUCT == "sdk"
                || Build.PRODUCT == "google_sdk"
                || Build.PRODUCT == "sdk_x86"
                || Build.PRODUCT == "vbox86p"
                || Build.PRODUCT.lowercase().contains("nox")
                || Build.BOARD.lowercase().contains("nox")
                || (Build.BRAND.startsWith("generic") &&    Build.DEVICE.startsWith("generic")))
        return isEmulator || checkEmulatorFiles()
    }

    fun isUsefulUSIM(context: Context): Boolean {
        val tm =
            context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager //gets the current TelephonyManager
        return tm.simState != TelephonyManager.SIM_STATE_ABSENT
    }

    fun openUrl(context: Context, strUrl: String): Boolean
    {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(strUrl))

            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        } catch (e: Exception) {
            return false
        }
        return true
    }

    fun openMarketUrl(context: Context, packageName: String = context.packageName)
    {
        val url = String.format("market://details?id%s", packageName)
        openUrl(context, url)
    }

    fun openSystemSharedActivity(
        context: Context,
        url: String,
        title: String,
        subTitle: String
    ) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.addCategory(Intent.CATEGORY_DEFAULT)
        intent.type = "text/plain"
//        intent.putExtra(Intent.EXTRA_TITLE, title)
//        intent.putExtra(Intent.EXTRA_SUBJECT, subTitle)
        intent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(Intent.createChooser(intent, context.resources.getString(R.string.share_chooser)))
    }

    fun getAppVersion(context: Context): String
    {
        return try {
            val packageInfo = context.packageManager
                .getPackageInfo(context.packageName, 0)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                packageInfo.longVersionCode.toString()
            } else {
                packageInfo.versionCode.toString()
            }
        } catch (e: PackageManager.NameNotFoundException) {
            // should never happen
            throw RuntimeException("Coult not get package name: $e")
        }
    }

    fun getCurrentTime() : Long {
        return System.currentTimeMillis()
    }



    fun getDevicePhoneNumber(context: Context) : String
    {
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_SMS) ==
            PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context,
                android.Manifest.permission.READ_PHONE_NUMBERS) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_PHONE_STATE) ==
            PackageManager.PERMISSION_GRANTED) {
            val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

            return tm.line1Number.replace("+82", "0")
        }
        return ""
    }

    fun getAndroidId(context: Context): String
    {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun getAndroidVersionCode(): String {
        return Build.VERSION.SDK_INT.toString()
    }

    fun getCountryCode(context: Context): String {
        val loc = context.resources.configuration.locale
        var str = loc.country
        str = str.uppercase(loc)
        return str
    }

    fun getLanguageCode(context: Context): String {
        val loc = context.resources.configuration.locale
        return loc.language.uppercase(loc)
    }

    fun getPhoneModel(): String {
        return Build.MODEL
    }

    fun convertTimeString(millions: Long) : String
    {
//        val millis: Long = millions % 1000
        val second: Long = millions / 1000 % 60
        val minute: Long = millions / (1000 * 60) % 60
//        val hour: Long = millions / (1000 * 60 * 60) % 24

        return String.format("%02d:%02d", minute, second)
    }

    fun recommendKAKAO(context: Context) : Boolean
    {
        var strText: String? = context.resources.getString(R.string.kakao_msg)

        strText += "\r\n"
        strText += "https://play.google.com/store/apps/details?id="
        strText += context.packageName

        try {
            val pm: PackageManager = context.packageManager
            val intent = pm.getLaunchIntentForPackage( "com.kakao.talk")?.apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, strText)
                type = "text/plain"
            }

            context.startActivity(intent)
        } catch (e: java.lang.Exception)
        {
            return false
        }
        return true
    }

    fun isExpired(lastTime: Long, expireTime: Long): Boolean
    {
        val nCurTime: Long = System.currentTimeMillis()
        val nElapse = nCurTime - lastTime
        return nElapse >= expireTime
    }

    fun toPointFormat(point: String, space : Boolean = true): String
    {
        val convertPoint = if (point.isEmpty()) 0 else point.toLong()
        return NumberFormat.getNumberInstance(Locale.US).format(convertPoint) + if (space) " "  else "" + "C"
    }

    fun toPointFormat(point: Int): String
    {
        return NumberFormat.getNumberInstance(Locale.US).format(point) + " C"
    }

    fun toCashFormat(point: String, suffix : String): String
    {
        val convertPoint = if (point.isEmpty()) 0 else point.toLong()
        return NumberFormat.getNumberInstance(Locale.US).format(convertPoint) + suffix
    }

    fun toListDotText(context: Context, description : String) : SpannableStringBuilder
    {
        val arr: List<String> = description.split("\n")

        val bulletGap = dp(context, 10).toInt()

        val ssb = SpannableStringBuilder()
        for (i in arr.indices) {
            val line = arr[i]
            val ss = SpannableString(line)
            ss.setSpan(BulletSpan(bulletGap), 0, line.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            ssb.append(ss)

            //avoid last "\n"
            if (i + 1 < arr.size) ssb.append("\n")
        }
        return ssb
    }

    fun dp(context: Context, dp : Int): Float = context.resources.displayMetrics.density * dp

    fun hideEmail(email : String) : String
    {
        val split = email.split("@")
        val prefix = split[0]
        val builder = StringBuilder(prefix)
        if (builder.length >= 5) {
            builder[2] = '*'
            builder[3] = '*'
            builder[4] = '*'
        }
        return builder.toString()+"@"+split[1]
    }

    fun getBitmapFromStorage(context: Context, strUrl: String?): Bitmap?
    {
        var bitmap: Bitmap? = null
        bitmap = try {
            if (Build.VERSION.SDK_INT < 28) {
                MediaStore.Images.Media.getBitmap(context.contentResolver, Uri.parse(strUrl))
            } else {
                val source = ImageDecoder.createSource(context.contentResolver, Uri.parse(strUrl))
                ImageDecoder.decodeBitmap(source)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            null
        }
        return bitmap
    }

    fun <T> isProperArray(ar: ArrayList<T>, position: Int): Boolean = position >= 0 && ar.size > position

    fun getRequestParam(strParam : String) : RequestBody
    {
        return RequestBody.create("text/plain".toMediaTypeOrNull(), strParam)
    }

    fun isLimitSize(f : File?, mByte : Int) : Boolean
    {
        // nSize = 1000 = 1MB
        f ?: return false
        return (f.length() / 1024) <= mByte * 1024
    }

    fun resizeXwBitmap(bitmap: Bitmap?, nSize : Int) : Bitmap? //높이, 너비 기준 최대 nSize로 리사이징
    {
        try
        {
            bitmap ?: return null

            var nWidth = bitmap.width
            var nHeight = bitmap.height

            if (nWidth <= nSize && nHeight <= nHeight)
            {
                return bitmap
            }

            val bBigWidth = nWidth >= nHeight
            val dPercent : Double
            val nInterval : Int
            if (bBigWidth)
            {
                nInterval = nWidth - nSize
                dPercent =  (nInterval.toDouble() / nWidth.toDouble()) * 100.0

                nWidth = nSize
                nHeight = nHeight - (nHeight * dPercent * 0.01).roundToInt()
            }
            else
            {
                nInterval = nHeight - nSize
                dPercent = (nInterval.toDouble() / nHeight.toDouble()) * 100.0

                nHeight = nSize
                nWidth = nWidth - (nWidth * dPercent * 0.01).roundToInt()
            }

            return getResizedBitmap(bitmap, nWidth ,nHeight)
        }catch (e : Exception)
        {
            return null
        }
    }

    fun getResizedBitmap(bm: Bitmap, newWidth: Int, newHeight: Int): Bitmap?
    {
        val width = bm.width
        val height = bm.height
        val scaleWidth = newWidth.toFloat() / width
        val scaleHeight = newHeight.toFloat() / height
        // CREATE A MATRIX FOR THE MANIPULATION
        val matrix = Matrix()
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight)

        // "RECREATE" THE NEW BITMAP
        val resizedBitmap = Bitmap.createBitmap(
            bm, 0, 0, width, height, matrix, false)
        bm.recycle()
        return resizedBitmap
    }

    fun getBitmapToFile(context : Context, bitmap: Bitmap, name: String) : File? // 비트맵을 갤러리 디렉토리에 저장하고 파일을 반환하는 함수
    {
        val imageFile = File(getGalleryFileDir(context), "$name.jpg")
        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os)
            os.flush()
            os.close()
        } catch (e: java.lang.Exception)
        {
            Log.e(context.packageName, "Error writing bitmap", e)
            return null
        }
        return imageFile
    }

    fun makeGalleryDir(context: Context)
    {
        val f = context.filesDir
        var strDir: String

        strDir = f.path
        strDir += "/" + "Gallery"

        val fDir = File(strDir)

        if (!fDir.isDirectory) {
            fDir.mkdir()
        }
    }

    fun deleteChildFileFromDir(strDirPath : String) // 해당디렉토리의 자식 파일들을 삭제하는 메서드
    {
        val dir = File(strDirPath)
        if (dir.isDirectory)
        {
            val childFileList = dir.listFiles()
            childFileList ?: return
            for (f in childFileList)
            {
                f.delete()
            }
        }
    }

    fun getGalleryFileDir(context: Context): String
    {
        val f = context.filesDir
        var strDir: String

        strDir = f.path
        strDir += "/" + "Gallery"
        f.mkdirs()

        return strDir
    }

    fun getRandomString(nLength: Int): String
    {
        val ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm"
        val rd = Random()
        val sb = StringBuilder(nLength)
        for (i in 0 until nLength)
            sb.append(ALLOWED_CHARACTERS[rd.nextInt(ALLOWED_CHARACTERS.length)])
        return sb.toString()
    }

}

